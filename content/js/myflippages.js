//$('.shape').shape();

function myflip() {
  // show loading image
  $('#loader_img1').show();

  // main image loaded ?
  $('.body').on('load', function(){
    // hide/remove the loading image
    $('#loader_img1').hide();
    $(this).shape('flip over').delay(100);
  });
  //$(this).shape('flip over').delay(100);
}

  var $container = jQuery('#masonry-grid');
  // initialize
  var $grid = $container.masonry({
    //columnWidth: 400,
     // gutter: 0,
     percentPosition: true,
    itemSelector: '.grid-item'
  });
var masonary = function(){
   $('#masonry-grid').masonry({
  // options
  itemSelector: '.grid-item',
   percentPosition: true
  //columnWidth: 200
 });

}
window.onload = function(){
  $('.main-slider .ui.grid').removeClass('invisible');
  $('.main-slider').removeClass('loading-ico');
    controlFlip() ;
}
window.onresize = function(){
  $('.main-slider .ui.grid').removeClass('invisible');
  $('.main-slider').removeClass('loading-ico');
    controlFlip() ;
}
function controlFlip(){
  //alert('s')
  // for individual flips
  var maxi = $('.main-slider .side.active:visible,.mobile-flip-slider .side.active:visible').length;
  timeperflip = 8000;
  flip = function(i){
    $('.shape:visible:eq('+i+')').shape({
      onChange:function(){
        if (++i == maxi) i =0;
        
        flip(i);
      },
      duration:timeperflip/maxi
    }).shape('flip over');
  }
  flip(0);
}

$(document).ready(function() { 

  // for complete slide transitions
 $('.progress-buttons .button').click(function(){
     var i = $(this).index();
     // alert(i)
     // $('.slider').show();
     // $('.slider').shape('set next side', '.sides .side:eq('+i+')')
     $('.sides .side.active').removeClass('active');
     $('.sides .side:nth-child('+(i+1)+')').addClass('active');
   $(this).addClass('active').siblings().removeClass('active');
     // $('.sides .side.active').removeClass('active');
     // // $('.slider').transition('flip over');
   });
  masonary();
});
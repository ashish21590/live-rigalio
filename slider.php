 <div class="main-slider-container screen-flip-slider">
            <div class=" main-slider loading-ico">
                <div class="ui grid invisible">
                    <div class="five column row ">
                        <div class="column">
                            <div class="ui people shape">
                                <div class="sides">
                                    <div class="active side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="content/slider/small/audemars_piguet_perpetual_calender.jpg" class='ui image' alt="slider_image"/>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><a href="product/audemars-piguet-royal-oak-perpetual-calendar/58/overview"><h4>Audemars Piguet Royal Oak Perpetual Calendar: The new invention of time.</h4> </a>
                             <hr>
                             <div> <img src="content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="content/slider/small/audemars_piguet_royal_oak_offshore_chronograph.jpg" class='ui image' alt="slider_image"/>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><a href="product/audemars-piguet-royal-oak-off-shore-chronograph/56/overview"><h4>Audemars Piguet Royal Oak Offshore Chronograph: Dynamisma and heritage from the Swiss maestro.</h4> </a>
                             <hr>
                             <div> <img src="content/images/icons/hovered-img-icon.png" alt="slider_image" /> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                   <?php /*?> <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/slider/small/bmw-7-series.jpg" class='ui image' alt="slider_image"/>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>The epitome of fine dining </h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div><?php */?>
                                </div>
                            </div>
                            <div class="ui people shape">
                                <div class="sides">
                                    <div class="active side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="content/slider/small/bmw-7-series.jpg" class='ui image'>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span> <a href="product/bmw-7-series-750-li-m-sport/34/overview"><h4>BMW 7 Series LI M Sport: Benchmarking luxury since 1916.</h4> </a>
                             <hr>
                             <div> <img src="content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="content/slider/small/burj-al-arab.jpg" class='ui image'/>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span> <a href="product/burj-al-arab-hotel/32/overview"><h4>The only 7 star hotel in the world.</h4> </a>
                             <hr>
                             <div> <img src="content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                   <?php /*?> <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/slider/small/gulfstream-g650-er.jpg" class='ui image' alt="slider_image"/>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>LVMH endorses sustainable luxury</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div><?php */?>
                                </div>
                            </div>
                        </div>
                        <div class=" column">
                            <div class="ui people shape">
                                <div class="sides">
                                    <div class="active side">
                                        <div class="content">
                                            <img src="content/slider/large/zuhair-murad-ss-16-collection.jpg" class='ui image' alt="slider_image"/>
                                            <div class="banner-hover-con">
                                                <div class="tab">
                                                    <div class="tab-in">
                             <span> <a href="product/zuhair_murad_ss_16_couture_collection/82/overview"><h4>Zuhair Murad SS16 couture: Sparkling, majestic, glamorous, a complete fairy tale.</h4></a>
                             <hr>
                             <div> <img src="content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                    </div>
                                                </div>
                                            </div> <!--/banner-hover-con -->
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <img src="content/slider/large/indian-motorcycle-sprinfield.jpg" class='ui image' alt="slider_image"/>
                                            <div class="banner-hover-con">
                                                <div class="tab">
                                                    <div class="tab-in">
                             <span> <a href="<?php echo base_url(); ?>product/indian_motorcycles_indian_springfield_thunder/63/overview"><h4>Indian Springfield Thunder: A Cruiser and a Bagger blended into one.</h4> </a>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                    </div>
                                                </div>
                                            </div> <!--/banner-hover-con -->
                                        </div>
                                    </div>
                                 <?php /*?>   <div class="side">
                                        <div class="content">
                                            <img src="<?php echo base_url(); ?>content/slider/large/macallan.jpg" class='ui image' alt="slider_image"/>
                                            <div class="banner-hover-con">
                                                <div class="tab">
                                                    <div class="tab-in">
                             <span><h4>A rare collection of exquisite tableware</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                    </div>
                                                </div>
                                            </div> <!--/banner-hover-con -->
                                        </div>
                                    </div><?php */?>
                                </div>
                            </div>
                        </div>

                        <div class="column">
                            <div class="ui people shape">
                                <div class="sides">
                                    <div class="active side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/slider/small/ducati_panigale.jpg" class='ui image' alt="slider_image"/>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span> <a href="<?php echo base_url(); ?>product/ducati-1198-panigale-r/31/overview"><h4>Track ready, start to finish.</h4> </a>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/slider/small/gulfstream-g650-er.jpg" class='ui image' alt="slider_image" />
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span>  <a href="<?php echo base_url(); ?>product/gulfstream-650-er/28/overview"><h4>Gulfstream 650 ER: The class leader in the skies.</h4> </a>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                   <?php /*?> <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/slider/small/samsung-88-inch.jpg" class='ui image'/>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>Escape monotony for a man made paradise</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div><?php */?>
                                </div>
                            </div>
                            <div class="ui people shape">
                                <div class="sides">
                                    <div class="active side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/slider/small/indian-motorcycle-chief-dark-horse.jpg" class='ui image' alt="slider_image"/>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span>  <a href="<?php echo base_url(); ?>product/indian-motorcycles-chief-dark-horse/62/overview"><h4>Indian Chief Dark Horse: a darker alter ego.</h4></a>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/slider/small/guerlain-lipstick.jpg" class='ui image' alt="slider_image"/>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span> <a href="<?php echo base_url(); ?>product/guerlain_kiss_kiss_gold_and_diamond_lipstick/90/overview"> <h4>Guerlain Kiss Kiss Gold and Diamond Lipstick.</h4></a>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                 <?php /*?>   <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/slider/small/vertu-bentley.jpg" class='ui image' alt="slider_image"/>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>A luxurious gastronomic journey</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div><?php */?>
                                </div>
                            </div>
                        </div>

                        <div class=" column hidden-sm4">
                            <div class="ui people shape">
                                <div class="sides">
                                    <div class="active side">
                                        <div class="content">
                                            <img src="<?php echo base_url(); ?>content/slider/large/montegrappa-ayrton-senna-edition.jpg" class='ui image' alt="slider_image"/>
                                            <div class="banner-hover-con">
                                                <div class="tab">
                                                    <div class="tab-in">
                             <span> <a href="<?php echo base_url(); ?>product/montegrappa_ayrton_senna_edition/119/overview"><h4>Montegrappa Tribute to Formula One Legend Ayrton Senna.</h4></a>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                    </div>
                                                </div>
                                            </div> <!--/banner-hover-con -->
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <img src="<?php echo base_url(); ?>content/slider/large/rolls-royce-phantom.jpg" class='ui image' alt="slider_image"/>
                                            <div class="banner-hover-con">
                                                <div class="tab">
                                                    <div class="tab-in">
                             <span><a href="<?php echo base_url(); ?>product/rolls_royce_phantom/18/overview"><h4>Rolls Royce Phantom: Birthed to be bespoke.</h4></a>
                             <hr>
                             <div><img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                    </div>
                                                </div>
                                            </div> <!--/banner-hover-con -->
                                        </div>
                                    </div>
                                 <?php /*?>   <div class="side">
                                        <div class="content">
                                            <img src="<?php echo base_url(); ?>content/images/banner/large/Artboard1copy5.jpg" class='ui image' alt="slider_image"/>
                                            <div class="banner-hover-con">
                                                <div class="tab">
                                                    <div class="tab-in">
                             <span><h4>Experience Michelin Star restaurants in the city of love</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                    </div>
                                                </div>
                                            </div> <!--/banner-hover-con -->
                                        </div>
                                    </div><?php */?>
                                </div>
                            </div>
                        </div>

                        <div class="column hidden-sm5">
                            <div class="ui people shape">
                                <div class="sides">
                                    <div class="active side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/slider/small/clive-christian-no-1-passant.jpg" class='ui image' alt="slider_image"/>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><a href="<?php echo base_url(); ?>product/clive_christian_no1_passant_guardant/111/overview"><h4>Clive Christian No1 Passant Guardant: The king of fragrances treasured in a magnificent bejewelled bottle.</h4></a>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/slider/small/subzero-pro-88.jpg" class='ui image' alt="slider_image"/>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span> <a href="<?php echo base_url(); ?>product/sub-zero_pro_48_with_glass_door/49/overview"><h4>Sub-Zero Pro 48: A tribute to nutrition conservation.</h4></a>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                   <?php /*?> <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/slider/small/Artboard1copy14.jpg" class='ui image' alt="slider_image"/>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>The finest wheels at Concorso d’Eleganza Villa d’Este 2016</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div><?php */?>
                                </div>
                            </div>
                            <div class="ui people shape">
                                <div class="sides">
                                    <div class="active side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/slider/small/taj-palace-mumbai.jpg" class='ui image' alt="slider_image"/>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span>  <a href="<?php echo base_url(); ?>product/taj_palace_mumbai/17/overview"><h4>Taj Palace Mumbai: The most iconic Taj.</h4> </a>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/slider/small/vertu-bentley.jpg" class='ui image' alt="slider_image"/>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><a href="<?php echo base_url(); ?>product/vertu_signature_for_bentley/47/overview"><h4>Vertu Signature for Bentley: The stylish cell-phone crafted after Bentley.</h4></a>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                   <?php /*?> <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/slider/small/Artboard1copy17.jpg" class='ui image' alt="slider_image"/>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>The Maharaja’s abode</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div><?php */?>
                                </div>
                            </div>
                        </div>

                    </div>

                    <ul class="progress-buttons">
                        <li class="button active"></li>
                        <li class="button"></li>
                        <li class="button"></li>
                    </ul>
                </div> <!--/ui grid flip slider -->


            </div>
        </div>
<?php class Files_Model extends CI_Model {
 
    public function insert_file($filename, $title,$visibility)
    {
        $data = array(
            'filename'      => $filename,
            'title'         => $title,
            'visibility'=> $visibility
        );
        $this->db->insert('files', $data);
        return $this->db->insert_id();
    }

    function get_image($file_id){

 					$this->db->select('filename');
                    $this->db->from('files');
                    $this->db->where('id',$file_id);
                    $query = $this->db->get();
                    return $query->result_array(); 

 
						}

}

?>
<?php

    class Geteditorial extends CI_Model
    {

        /**
         * Responsable for auto load the database
         * @return void
         */
        public function __construct()
        {
            $this->load->database();
        }



        public function all_editorial(){

            $this->db->select('*');
            $this->db->from('editorial_subcat');
            $this->db->where('editorial_subcat_isActive','1');
            $query = $this->db->get();
            return $query->result_array();

        }

        public function all_subcat_editorial($cat_id){

            $sql = "SELECT * FROM `editorial_detail`
            join curator_product on editorial_detail.cproduct_idfk=curator_product.cproduct_id
             join curator_detail on curator_detail.curator_id=editorial_detail.curator_idfk
             join editorial_subcat on editorial_subcat.editorial_subcat_id=editorial_detail.editorial_subcatidfk
             join category on curator_product.category_id=category.categoryid
              where editorial_detail.editorial_subcatidfk= $cat_id";
            //echo $sql;




            $result = $this->db->query($sql)->result_array();
            return $result;


        }

        public  function follow_curator($user_id,$curator_id)
        {

            $this->db->select('*');
            $this->db->from('following');
            $this->db->where('user_id', $user_id);
            $this->db->where('curator_id', $curator_id);
            $query = $this->db->get();
            $user_check =$query->num_rows();

            if ($user_check > 0) {

                echo "0"; //already following curator
            }else{

            $data = array(
                'user_id'       => $user_id,
                'curator_id'        => $curator_id

            );
            $this->db->insert('following', $data);
            $insert_id = $this->db->insert_id();
            return $insert_id;

        }

        }

        public function getcurator($curator_id)
        {
            $this->db->select('*');
            $this->db->from('curator_detail');
            $this->db->where('curator_detail.curator_id', $curator_id);
            $this->db->where('curator_detail.curator_isActive',"1");
            $query = $this->db->get();
            return $query->result_array();

        }
        public function curator_wise($curator_id)
        {
            $sql = "SELECT `editorial_detail`.`editorial_id`,`editorial_detail`.`curator_idfk`,
		`editorial_detail`.`editorial_subcatidfk`, `editorial_detail`.`cproduct_idfk`,
		`editorial_detail`.`editorial_name`, `editorial_detail`.`editorial_datetime`,`curator_detail`.`curator_id`,
		`curator_detail`.`curator_name`,
		`curator_product`.`cproduct_image`,`curator_product`.`cproduct_Name`,`curator_product`.`cproduct_Title`,
		`curator_product`.category_id,`curator_product`.`cproduct_id`,`curator_product`.cproductSmallDiscription,
		`category`.category_Name,`category`.`categoryId`
FROM `editorial_detail`
JOIN `curator_detail` ON `curator_detail`.`curator_id` = `editorial_detail`.`curator_idfk`
JOIN `curator_product` ON `curator_product`.`cproduct_id` = `editorial_detail`.`cproduct_idfk`
JOIN `category` ON `category`.`categoryId` = `curator_product`.`category_id`

WHERE `editorial_detail`.`curator_idfk` = $curator_id and `editorial_detail`.`editorial_isActive`=1";

            $result = $this->db->query($sql)->result_array();
            return $result;
        }

        public function get_editorial_info($editorialid)
        {
            $this->db->select('editorial_detail.`editorial_id`, editorial_detail.`editorial_subcatidfk`, editorial_detail.`curator_idfk`, editorial_detail.`cproduct_idfk`, editorial_detail.`editorial_banner_img`, editorial_detail.`editorial_name`, editorial_detail.`editorial_isActive`, editorial_detail.`editorial_datetime`, curator_detail.`curator_id`, curator_detail.`curator_name`, curator_detail.`curator_email`, curator_detail.`curator_intrest`, curator_detail.`curator_location`, curator_detail.`curator_picture`, curator_detail.`curator_detail`, curator_detail.`curator_isActive`, curator_detail.`curator_datetime`, editorial_subcat.`editorial_subcat_id`, editorial_subcat.`editorial_catname`, editorial_subcat.`editorial_subcat_name`');
            $this->db->from('editorial_detail');
            $this->db->join('curator_detail', 'curator_detail.curator_id = editorial_detail.curator_idfk');
            $this->db->join('editorial_subcat', 'editorial_subcat.editorial_subcat_id = editorial_detail.editorial_subcatidfk');
            // $this->db->group_by('category.category_Name');
            // $this->db->order_by('category.categoryId');
            $this->db->where('editorial_detail.editorial_id', $editorialid);
            $query = $this->db->get();
            return $query->result_array();
        }

        function editorial_wise_product($productid)
        {

            $sql = "SELECT `product`.`productId`, `product`.`product_Name`, product.`product_smallname`, `product`.`productSmallDiscription`, `category`.`category_color`, `product`.`product_image`, `product`.`created_on`, `brand`.`brandId`, `brand`.`brand_Name`,`brand`.`brand_image`, `sub_category`.`subCategoryId`, `sub_category`.`subCategory_Name`, `category`.`category_Name`, `category`.`category_icon`, `category`.`categoryId`
FROM `product`
JOIN `brand` ON `brand`.`brandId` = `product`.`brandId_Fk`
JOIN `sub_category` ON `sub_category`.`subCategoryId` = `product`.`subcategoryId_Fk`
JOIN `category` ON `category`.`categoryId` = `product`.`categoryId_Fk`
WHERE `product`.`productId` = $productid and `product`.`isActive`=1";
            $result = $this->db->query($sql)->result_array();
            return $result;
        }

        function curator_wise_product($curator_id)
        {
            $sql = "SELECT `editorial_detail`.`editorial_id`, `editorial_detail`.`editorial_subcatidfk`, `editorial_detail`.`curator_idfk`, `editorial_detail`.`product_idfk`,
`curator_detail`.`curator_id`,
`curator_detail`.`curator_name`,
`curator_detail`.`curator_email`,
`product`.`productId`,
`product`.`product_Name`, `product`.`productSmallDiscription`, `category`.`category_color`, `product`.`product_image`, `product`.`created_on`, `brand`.`brandId`, `brand`.`brand_Name`, `sub_category`.`subCategoryId`, `sub_category`.`subCategory_Name`, `category`.`category_Name`, `category`.`category_icon`, `category`.`categoryId`

FROM `editorial_detail`
JOIN `curator_detail` ON `curator_detail`.`curator_id` = `editorial_detail`.`curator_idfk`
JOIN `product` ON `product`.`productId` = `editorial_detail`.`product_idfk`
JOIN `brand` ON `brand`.`brandId` = `product`.`brandId_Fk`
JOIN `sub_category` ON `sub_category`.`subCategoryId` = `product`.`subcategoryId_Fk`
JOIN `category` ON `category`.`categoryId` = `product`.`categoryId_Fk`
WHERE `curator_detail`.`curator_id` = $curator_id and `product`.`isActive`=1";
//echo $sql;

            $result = $this->db->query($sql)->result_array();
            return $result;

        }

        function curator_product($curatorproductid)
        {
            $sql = "SELECT `curator_product`.`cproduct_id`, `curator_product`.`cproduct_Name`, curator_product.`cproduct_image`, `curator_product`.`cproduct_Title`, `curator_product`.`cproductSmallDiscription`, `curator_product`.`cproduct_link`, `curator_product`.`product_editorial_content_one`, `curator_product`.`product_editorial_content_two`, `curator_product`.`category_id`,`curator_product`.`cproduct_isActive`, `curator_product`.`cproduct_datetime`, `category`.`categoryId`, `category`.`category_Name`, `category`.`category_image`, `category`.`category_icon`, `category`.`category_color`
FROM `curator_product`
JOIN `category` ON `category`.`categoryId` = `curator_product`.`category_id`
WHERE `curator_product`.`cproduct_id` = $curatorproductid and `curator_product`.`cproduct_isActive`=1";
            $result = $this->db->query($sql)->result_array();
            return $result;
        }

        function curator_product_image($curatorproductid)
        {
            $this->db->select('*');
            $this->db->from('curator_product_image');
            $this->db->where('cproductId_Fk', $curatorproductid);
            $query = $this->db->get();
            return $query->result_array();
        }

        function curator_related_product($curator_id)
        {
            $this->db->select('cproduct_idfk');
            $this->db->from('editorial_detail');
            $this->db->where('curator_idfk', $curator_id);
            $query = $this->db->get();
            return $query->result_array();

        }

        function curator_all_product($curator_related_product)
        {
            $curator_product = [];
            foreach ($curator_related_product as $row) {
                foreach ($row as $v) {
                    $curator_product[] = $v;
                }
            }

            for ($i = 0; $i < count($curator_product); $i++) {
                $me[] = "'" . $curator_product[$i] . "'";
                $me12 = implode(',', $me);
            }
            //print_r($me12); exit;

            $sql = "SELECT `curator_product`.`cproduct_id`, `curator_product`.`cproduct_Name`, curator_product.`cproduct_image`, `curator_product`.`cproduct_Title`, `curator_product`.`cproductSmallDiscription`, `curator_product`.`cproduct_link`, `curator_product`.`product_editorial_content_one`, `curator_product`.`product_editorial_content_two`, `curator_product`.`category_id`,`curator_product`.`cproduct_isActive`, `curator_product`.`cproduct_datetime`, `category`.`categoryId`, `category`.`category_Name`, `editorial_detail`.`editorial_id`, `editorial_detail`.`editorial_subcatidfk`, `editorial_detail`.`curator_idfk`, `editorial_detail`.`cproduct_idfk`, `editorial_detail`.`editorial_banner_img`, `editorial_detail`.`editorial_name`, `editorial_detail`.`editorial_isActive`, `editorial_detail`.`editorial_datetime`
FROM `curator_product`
JOIN `category` ON `category`.`categoryId` = `curator_product`.`category_id`
JOIN `editorial_detail` ON `editorial_detail`.`cproduct_idfk` = `curator_product`.`cproduct_id`
WHERE `curator_product`.`cproduct_id` IN ($me12) and `curator_product`.`cproduct_isActive`=1";
//echo $sql;
            $result = $this->db->query($sql)->result_array();
            return $result;


        }

public function count_curatorfollower($curator_id)
    {
        $this->db->select('count(id) as no');
        $this->db->from('following');
        $this->db->where('curator_id', $curator_id);
        $query = $this->db->get();
        return $query->result_array();
    }
public function get_user_follow($userregistrationid)
    {
        $this->db->select('curator_id');
        $this->db->from('following');
        $this->db->where('user_id', $userregistrationid);
        $query = $this->db->get();
        //print_r($query);
        return $query->result_array();


    }
	function unfollow_curator($userid, $curator_id)
    {

        $this->db->where('user_id', $userid);
        $this->db->where('curator_id', $curator_id);
        $this->db->delete('following');
        return "0";


    }
	 function follow_segment($userid, $seg_id)
    {

        $this->db->select('*');
        $this->db->from('following');
        $this->db->where('user_id', $userid);
        $this->db->where('editorial_subcat_id', $seg_id);
        $query = $this->db->get();


        $rows = $query->num_rows();
        if ($rows > 0) {

            return "0";
        } else {
            $following = array('user_id' => $userid,
                'editorial_subcat_id' => $seg_id,
                'follow' => 1
            );
            $this->db->insert('following', $following);
            return $this->db->insert_id();
        }
    }
	 function unfollow_segment($userid, $seg_id)
    {

        $this->db->select('*');
        $this->db->from('following');
        $this->db->where('user_id', $userid);
        $this->db->where('editorial_subcat_id', $seg_id);
        $query = $this->db->get();


        $rows = $query->num_rows();
        if ($rows > 0) {

            $following = array('user_id' => $userid,
                'editorial_subcat_id' => $seg_id,
                'follow' => 1
            );

            $this->db->where('user_id', $userid);
            $this->db->where('editorial_subcat_id', $seg_id);
            $this->db->delete('following');
            return "0";


        }
    }

    }
        ?>

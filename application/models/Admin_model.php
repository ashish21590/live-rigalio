<?php

    class Admin_model extends CI_Model
    {

        /**
         * Responsable for auto load the database
         * @return void
         */
        public function __construct()
        {
            $this->load->database();
             $this->load->library("pagination");
        }

        function insert_model($brand_image,$cover_image){

            $brand_name = $this->input->post('brand_name');
            $establish = $this->input->post('establish');
            $brandDescription = $this->input->post('brandDescription');
            $small_description = $this->input->post('small_description');
            $official_link = $this->input->post('official_link');
            $brand_legacy=$this->input->post('brand_legacy');
            $facebook_username=$this->input->post('facebook_username');
            $twitter_username=$this->input->post('twitter_username');

if($cover_image){
            $data = array(
                'brand_Name'       => $brand_name,
                'Establish'     =>  $establish,
                'brandDescription'        => $brandDescription,
                //'lname' => $this->input->post('userlname'),
                'brandSmallDescription'          => $small_description,
                'official_link'        => $official_link,
                'brand_image'         => "uploads/brand/".$brand_image,
                'cover_image'   => "content/images/news-post-inner/".$cover_image,
                'twitter_userid'  => $twitter_username,
                'fb_userid' => $facebook_username,
                'legacy'=>$brand_legacy
            );

}
else{

$data = array(
                'brand_Name'       => $brand_name,
                'Establish'     =>  $establish,
                'brandDescription'        => $brandDescription,
                //'lname' => $this->input->post('userlname'),
                'brandSmallDescription'          => $small_description,
                'official_link'        => $official_link,
                'brand_image'         => "uploads/brand/".$brand_image,
               // 'cover_image'   => "content/".$cover_image,
                'twitter_userid'  => $twitter_username,
                'fb_userid' => $facebook_username,
                'legacy'=>$brand_legacy
            );

}
            $this->db->insert('brand', $data);
            $brand_insert_id = $this->db->insert_id();
            return $brand_insert_id;



        }

        function login($username, $password)
 {
   $this -> db -> select('*');
   $this -> db -> from('users');
   $this -> db -> where('username', $username);
   $this -> db -> where('password', $password);
   $this -> db -> limit(1);
   $query = $this -> db -> get();
   if($query -> num_rows() == 1)
   {
     return $query->result();
   }
   else
   {
     return false;
   }
 }



public function  list_brand()
    {   
        $this->db->select('*'); 
        $this->db->from('brand');
        $this->db->order_by('brandId','asc');   
        $get_categotylist_query = $this->db->get();     
        $get_categotylist = $get_categotylist_query->result_array();    
        return $get_categotylist;
        //print_r($get_categotylist);exit;
    }
    
    /*public function  list_brandnew()
    {   
        $this->db->select('brand.brandId, brand.brand_Name, brand.Establish, brand.brandDescription, brand.brandSmallDescription, brand.brandlongDescription, brand.title, brand.isActive, images.brandId_Fk, images.image_Name');  
        $this->db->from('brand');   
        $this->db->join('images','images.brandId_Fk=brand.brandId');
        $this->db->order_by('brandId','asc');   
        $get_categotylist_query = $this->db->get();     
        $get_categotylist = $get_categotylist_query->result_array();    
        return $get_categotylist;
        //echo $get_categotylist; exit;
    }*/

    public function get_brand($id)
    {   
        $this->db->select('*');
        $this->db->from('brand');
        //$this->db->join('images','images.brandId_Fk=brand.brandId');
        $this->db->where('brandId',$id);
        $get_category_query = $this->db->get();
        $get_category = $get_category_query->result_array();
        //print_r( $get_category) ;exit;
        return $get_category;
    }
    
    public function  list_category()
    {   
        $this->db->select('*'); 
        $this->db->from('category');    
        $this->db->order_by('categoryId','asc');    
        $get_categotylist_query = $this->db->get();     
        $get_categotylist = $get_categotylist_query->result_array();    
        return $get_categotylist;
        //print_r($get_categotylist);exit;
    }
        
    public function delete_brand($id)
    {
    
        $this->db->where('brandId',$id);    
        $this->db->delete('brand');
    }
    
    public function brand_active($id,$isActive)
    {
        
        if($isActive==0)
        {
            
        $data = array('isActive'=>1);       
        $this->db->where('brandId',$id);    
        $this->db->update('brand',$data);   
            
        }
        elseif($isActive==1)
        {           
        $data = array('isActive'=>0);       
        $this->db->where('brandId',$id);        
        $this->db->update('brand',$data);       
        }
    }
    
     public function get_brand_id($item)
    {

        $this->db->select('*');
        $this->db->from('brand');
        $this->db->where('brandId', $item);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }   
        
        public function update_brand($data)
        {
        $this->db->where('brandId',$id = $this->input->post('hide'));           
        $result = $this->db->update('brand', $data);    
        //return $result;
        //print_r($data);
        }

        public function product_insert($product_image)
    {
        //if($fileName!='' ){
        //$filename1 = explode(',',$fileName);
        //foreach($filename1 as $file){
        $data = array(
            'product_Name' => $this->input->post('productname'),
            'Price' => $this->input->post('productprice'),
            'model_no' => $this->input->post('productmodel'),
            'productSmallDiscription' => $this->input->post('productsmalldisc'),
            'productlongDiscription' => $this->input->post('productlongdisc'),
                'overview'                => $this->input->post('productoverview'),
            'product_Title' => $this->input->post('producttitle'),
            'gender' => $this->input->post('gender'),
            'categoryId_Fk' => $this->input->post('productcat'),
            'subcategoryId_Fk' => $this->input->post('productsubcat'),
            'brandId_Fk' => $this->input->post('productbrand'),
            'product_image'=>$product_image
            //'brand_image' => $file,
        );      
         $this->db->insert('product', $data);   
          $insert_id = $this->db->insert_id();
         return  $insert_id;
    //}
        //}
    }
    
     public function inserProductImage($fileName, $imageinsertid)
    {
    if($fileName!='' ){
    $filename1 = explode(',',$fileName);
    foreach($filename1 as $file){
    $data = array(
            'productId_Fk' => $imageinsertid,
            'image_Name' => $file,
        );      
         return $this->db->insert('images', $data); 
    }
        }
    }
        
    public function  list_product()
    {   
        $this->db->select('product.productId, product.product_Name, product.product_image, product.product_Title, product.Price, product.productSmallDiscription, product.productlongDiscription, product.model_no, product.gender, product.isActive, product.new, category.categoryId, category.category_Name, brand.brandId, brand.brand_Name, images.image_id, images.image_Name, images.productId_Fk ');    
        $this->db->from('product'); 
        $this->db->join('category','category.categoryId=product.categoryId_Fk');
        $this->db->join('brand','brand.brandId=product.brandId_Fk');
        $this->db->join('images','images.productId_Fk=product.productId','left');
        $this->db->group_by('product.product_Name');    
        $this->db->order_by('productId','desc'); 
        $get_categotylist_query = $this->db->get();     
        $get_categotylist = $get_categotylist_query->result_array();
        
        return $get_categotylist;
        //echo $get_categotylist; exit;
    }
    
    public function  list_productimage($id)
    {   
    $this->db->select('product.productId, product.product_Name, images.image_id, images.image_Name, images.productId_Fk '); 
        $this->db->from('product'); 
        $this->db->join('images','images.productId_Fk=product.productId','left');
        $this->db->where('productId_Fk',$id);
        $this->db->order_by('productId','asc'); 
        $get_categotylist_query = $this->db->get();     
        $get_productimagelist = $get_categotylist_query->result_array();
        return $get_productimagelist;
        //echo $get_categotylist; exit;
    }
    
    
    
    public function  list_subcategory()
    {   
        $this->db->select('*'); 
        $this->db->from('sub_category');    
        $this->db->order_by('subCategoryId','asc'); 
        $get_categotylist_query = $this->db->get();     
        $get_categotylist = $get_categotylist_query->result_array();    
        return $get_categotylist;
        //print_r($get_categotylist);exit;
    }
    
   
    
    public function  product_image($id)
    {   
        $this->db->select('product.productId, product.product_Name, images.image_id, images.image_Name, 
        images.productId_Fk '); 
        $this->db->from('product');
        $this->db->join('images','images.productId_Fk=product.productId');  
        $this->db->where('productId',$id);
        $this->db->order_by('image_id','asc');  
        $get_categotylist_query = $this->db->get();     
        $get_imagelist = $get_categotylist_query->result_array();   
        return $get_imagelist;
        //print_r($get_brandlist);exit;
    }
    
    public function get_product($id)
    {   
        $this->db->select('*');
        $this->db->from('product');
        $this->db->join('category','category.categoryId=product.categoryId_Fk');
        $this->db->join('brand','brand.brandId=product.brandId_Fk');
        $this->db->join('sub_category','sub_category.subCategoryId=product.subcategoryId_Fk');
        $this->db->where('productId',$id);
        $get_category_query = $this->db->get();
        $get_category = $get_category_query->result_array();
        //print_r( $get_category) ;exit;
        return $get_category;
    }   
    
    public function get_product_image($id)
    {   
        $this->db->select('*');
        $this->db->from('images');
        $this->db->where('image_id',$id);
        $get_category_query = $this->db->get();
        $get_category = $get_category_query->result_array();
        print_r( $get_category) ;
        return $get_category;
        //return $get_product_imagelist;
    }   
    
    public function delete_product($id)
    {
        $this->db->where('productId',$id);  
        $this->db->delete('product');
        
        $this->db->where('productId_Fk',$id);   
        $this->db->delete('images');
        
    }
    
    public function delete_product_image($id)
    {
        $this->db->where('image_id',$id);   
        $this->db->delete('images');
    }
    
    public function product_active($id,$isActive)
    {
        
        if($isActive==0)
        {
        $data = array('isActive'=>1);       
        $this->db->where('productId',$id);  
        $this->db->update('product',$data); 
        }
        elseif($isActive==1)
        {           
        $data = array('isActive'=>0);       
        $this->db->where('productId',$id);      
        $this->db->update('product',$data);     
        }
    }
    
     public function get_product_id($item)
    {

        $this->db->select('*');
        $this->db->from('product');
        $this->db->where('productId', $item);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }   
        
        public function update_product($data)
        {
            //print_r($data);
        $this->db->where('productId',$id = $this->input->post('hide'));         
        $result = $this->db->update('product', $data);  
        return $result;
    
        }
        
        public function update_productimage($data)
        {
                
        $this->db->where('image_id',$id = $this->input->post('hide'));          
        $result = $this->db->update('images', $data);   
        return $result;
    
        }
        
        
         public function subcategory($getcategory)
        {
            
        $this->db->select('*');
        $this->db->from('sub_category');
        $this->db->where('categoryId_Fk', $getcategory);
        $query = $this->db->get();
        return $query->result_array();
         
         }  


         //listing products

         public function  all_product()
    {   
        $this->db->select('*'); 
        $this->db->from('product');
        $this->db->order_by('productid','desc');
        $query = $this->db->get();     
        $result = $query->result_array();    
        return $result;
        //print_r($get_categotylist);exit;
    }
//listing specification name 
   
      public function  list_specification_name()
    {   
        $this->db->select('*'); 
        $this->db->from('specification_name');
        $query = $this->db->get();     
        $result = $query->result_array();    
        return $result;
        //print_r($get_categotylist);exit;
    }

    //listing specification type

    public function  list_specification_type()
    {   
        $this->db->select('*'); 
        $this->db->from('specification_type');
        $this->db->where('specifictionType_Name!=','');
        $query = $this->db->get();     
        $result = $query->result_array();    
        return $result;
        //print_r($get_categotylist);exit;
    }

    // adding product specifications

    public function addspecifications($data){

           $this->db->insert('specification', $data);
            $specification = $this->db->insert_id();
            return $specification;

    }

        //add specification type
        public function specificationType_insert()
        {
            $data = array(
                'specifictionType_Name' => $this->input->post('specificationtype'),
            );
            return $this->db->insert('specification_type', $data);
        }
        //add specification name
        public function specificationName_insert()
        {
            $data = array(
                'specification_Name' => $this->input->post('specificationname'),
            );
            return $this->db->insert('specification_name', $data);
        }

        public function list_specificationType()
        {
            $this->db->select('*');
            $this->db->from('specification_type');
            $this->db->order_by('specifictionTypeId', 'asc');
            $get_specifictionType = $this->db->get();
            $specifictionType = $get_specifictionType->result_array();
            return $specifictionType;
        }

        public function specifictionType_active($id, $isActive)
        {

            if ($isActive == 0) {

                $data = array('isActive' => 1);
                $this->db->where('specifictionTypeId', $id);
                $this->db->update('specification_type', $data);

            } elseif ($isActive == 1) {
                $data = array('isActive' => 0);
                $this->db->where('specifictionTypeId', $id);
                $this->db->update('specification_type', $data);
            }
        }

        public function brand_name()
        {
            $this->db->select('*');
            $this->db->from('brand');
            $query = $this->db->get();
            $result = $query->result_array();
            return $result;
            //print_r($get_categotylist);exit;
        }

        function brandnews_insert($brand_image, $cover_image)
        {

            $brandname = $this->input->post('brandname');
            $brandheadline = $this->input->post('brandheadline');
            $brandexcerptcont = $this->input->post('brandexcerptcont');
            $brandinnercont = $this->input->post('brandinnercont');
            $brandnewslink = $this->input->post('brandnewslink');
            $newstype = $this->input->post('newstype');
            $newsbuttonname = $this->input->post('newsbuttonname');


            $data = array(
                'brand_id'        => $brandname,
                'headline'        => $brandheadline,
                'news_small_desc' => $brandexcerptcont,
                //'lname' => $this->input->post('userlname'),
                'newslong_desc'   => $brandinnercont,
                'news_link'       => $brandnewslink,
                'type'            => $newstype,
                'button_name'     => $newsbuttonname,
                'news_img'        => "content/images/news-post/" . $brand_image,
                'banner_img'      => "content/images/news-post-inner/" . $cover_image
            );

            $this->db->insert('brand_news', $data);
            $brand_insert_id = $this->db->insert_id();
            return $brand_insert_id;
        }

        public function list_brandnews()
        {
            $this->db->select('*');
            $this->db->from('brand_news');
            $this->db->order_by('news_id', 'asc');
            $get_categotylist_query = $this->db->get();
            $get_categotylist = $get_categotylist_query->result_array();
            return $get_categotylist;
            //print_r($get_categotylist);exit;
        }
    public function p_count(){


        $this->db->select('*'); 
        $this->db->from('product');
        $query=$this->db->get();
        return $query->num_rows();


    }

    public function p_count_search($product_name){


        $this->db->select('*'); 
        $this->db->from('product');
         $this->db->where("product_Name LIKE '%$product_name%'");
        $query=$this->db->get();
        return $query->num_rows();

    }


    public function fetch_product($limit, $start) {
        
         $this->db->select('product.productId, product.product_Name, product.product_image, product.product_Title, product.Price, product.productSmallDiscription, product.productlongDiscription, product.model_no, product.gender, product.isActive, product.new, category.categoryId, category.category_Name, brand.brandId, brand.brand_Name, images.image_id, images.image_Name, images.productId_Fk ');    
        $this->db->from('product'); 
        $this->db->join('category','category.categoryId=product.categoryId_Fk');
        $this->db->join('brand','brand.brandId=product.brandId_Fk');
        $this->db->join('images','images.productId_Fk=product.productId','left');
        $this->db->group_by('product.product_Name');    
        $this->db->order_by('productId','desc'); 
            $this->db->limit($limit, $start);

        $get_categotylist_query = $this->db->get();     
        $get_categotylist = $get_categotylist_query->result_array();
        return $get_categotylist;
   }

   public function fetch_product_search($limit, $start,$product_name){

     $this->db->select('product.productId, product.product_Name, product.product_image, product.product_Title, product.Price, product.productSmallDiscription, product.productlongDiscription, product.model_no, product.gender, product.isActive, product.new, category.categoryId, category.category_Name, brand.brandId, brand.brand_Name, images.image_id, images.image_Name, images.productId_Fk ');    
        $this->db->from('product'); 
        $this->db->join('category','category.categoryId=product.categoryId_Fk');
        $this->db->join('brand','brand.brandId=product.brandId_Fk');
        $this->db->join('images','images.productId_Fk=product.productId','left');
        $this->db->where("product.product_Name LIKE '%$product_name%'");
        $this->db->group_by('product.product_Name');    
        $this->db->order_by('productId','desc'); 
            $this->db->limit($limit, $start);

        $get_categotylist_query = $this->db->get();     
        $get_categotylist = $get_categotylist_query->result_array();
        return $get_categotylist;

   }
	public function insert_highlight()
	{
		$data = array(
            'productId_Fk' => $this->input->post('productname'),
			'highlight' => $this->input->post('highlight')
        );	
		return $this->db->insert('product_highlight', $data);	
	}
	
	 public function  producthighlight()
    {   
        $this->db->select('*'); 
        $this->db->from('product_highlight');
		$this->db->order_by('highlightId','desc');
        $query = $this->db->get();     
        $result = $query->result_array();    
        return $result;
        //print_r($get_categotylist);exit;
    }
	 public function getprodinfo($hid)
    {

        $this->db->select('*');
		$this->db->select('product_highlight.`highlightId`, product_highlight.`productId_Fk`, product_highlight.`brandId_Fk`,
		product_highlight.categoryId_Fk, product_highlight.`highlight`, product_highlight.`isActive`, product.`productId`, product.`product_Name`');
        $this->db->from('product_highlight');
        $this->db->join('product', 'product.productId = product_highlight.productId_Fk');
        $this->db->where('highlightId', $hid);
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }	
	
	public function update_highlight($data)
		{
		 $pid=$this->input->post('hide'); 
		$this->db->where('highlightId',$pid);			
        $result = $this->db->update('product_highlight', $data);	
		return $result;
				//print_r($data);
		
		}
	public function delete_highlight($hid)
	{
	
		$this->db->where('highlightId',$hid);	
		$this->db->delete('product_highlight');
	
	}

    public function get_all_news($news_id){

         $this->db->select('*');
        $this->db->from('brand_news');
        $this->db->where('news_id', $news_id);
        $query = $this->db->get();
        return $query->result_array();

    }

    public function update_news($news_id){

            $news_id=$news_id;
            $brandname = $this->input->post('brandname');
            $brandheadline = $this->input->post('brandheadline');
            $brandexcerptcont = $this->input->post('brandexcerptcont');
            $brandinnercont = $this->input->post('brandinnercont');
            $brandnewslink = $this->input->post('brandnewslink');
            $newstype = $this->input->post('newstype');
            $newsbuttonname = $this->input->post('newsbuttonname');


            $data = array(
                'brand_id'        => $brandname,
                'headline'        => $brandheadline,
                'news_small_desc' => $brandexcerptcont,
                //'lname' => $this->input->post('userlname'),
                'newslong_desc'   => $brandinnercont,
                'news_link'       => $brandnewslink,
                'type'            => $newstype,
                'button_name'     => $newsbuttonname,
               // 'news_img'        => "content/images/news-post/" . $brand_image,
                //'banner_img'      => "content/images/news-post-inner/" . $cover_image
            );

            $this->db->where('news_id',$news_id);           
            $result = $this->db->update('brand_news', $data);    



    }





   

   public function update_news_1($cover_image,$news_id){
 $news_id=$news_id;
            $brandname = $this->input->post('brandname');
            $brandheadline = $this->input->post('brandheadline');
            $brandexcerptcont = $this->input->post('brandexcerptcont');
            $brandinnercont = $this->input->post('brandinnercont');
            $brandnewslink = $this->input->post('brandnewslink');
            $newstype = $this->input->post('newstype');
            $newsbuttonname = $this->input->post('newsbuttonname');


            $data = array(
                'brand_id'        => $brandname,
                'headline'        => $brandheadline,
                'news_small_desc' => $brandexcerptcont,
                //'lname' => $this->input->post('userlname'),
                'newslong_desc'   => $brandinnercont,
                'news_link'       => $brandnewslink,
                'type'            => $newstype,
                'button_name'     => $newsbuttonname,
                'news_img'        => "content/images/news-post/" .$cover_image
               // 'banner_img'      => "content/images/news-post/" . $cover_image
            );

            $this->db->where('news_id',$news_id);           
            $result = $this->db->update('brand_news', $data);    
   }

    public function update_news_2($brand_image,$news_id){
 $news_id=$news_id;
            $brandname = $this->input->post('brandname');
            $brandheadline = $this->input->post('brandheadline');
            $brandexcerptcont = $this->input->post('brandexcerptcont');
            $brandinnercont = $this->input->post('brandinnercont');
            $brandnewslink = $this->input->post('brandnewslink');
            $newstype = $this->input->post('newstype');
            $newsbuttonname = $this->input->post('newsbuttonname');


            $data = array(
                'brand_id'        => $brandname,
                'headline'        => $brandheadline,
                'news_small_desc' => $brandexcerptcont,
                //'lname' => $this->input->post('userlname'),
                'newslong_desc'   => $brandinnercont,
                'news_link'       => $brandnewslink,
                'type'            => $newstype,
                'button_name'     => $newsbuttonname,
                'banner_img'        => "content/images/news-post-inner/" . $brand_image
                //'banner_img'      => "content/images/news-post-inner/" . $cover_image
            );

            $this->db->where('news_id',$news_id);           
            $result = $this->db->update('brand_news', $data);    
   }

    public function update_news_3($brand_image,$cover_image,$news_id){
    $news_id=$news_id;
            $brandname = $this->input->post('brandname');
            $brandheadline = $this->input->post('brandheadline');
            $brandexcerptcont = $this->input->post('brandexcerptcont');
            $brandinnercont = $this->input->post('brandinnercont');
            $brandnewslink = $this->input->post('brandnewslink');
            $newstype = $this->input->post('newstype');
            $newsbuttonname = $this->input->post('newsbuttonname');


            $data = array(
                'brand_id'        => $brandname,
                'headline'        => $brandheadline,
                'news_small_desc' => $brandexcerptcont,
                //'lname' => $this->input->post('userlname'),
                'newslong_desc'   => $brandinnercont,
                'news_link'       => $brandnewslink,
                'type'            => $newstype,
                'button_name'     => $newsbuttonname,
                'news_img'        => "content/images/news-post/" . $brand_image,
                'banner_img'      => "content/images/news-post-inner/" . $cover_image
            );

            $this->db->where('news_id',$news_id);           
            $result = $this->db->update('brand_news', $data);    
   }



    }

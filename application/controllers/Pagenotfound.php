<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Pagenotfound extends CI_Controller {
    public function __construct() {
        parent::__construct();
         $this->load->library('session');  //Load the Session
    }

    public function index1() {
        $this->output->set_status_header('404'); 

       $this->session->unset_userdata('inviteuserid');
        $userregistrationid = $this->session->userdata('registrationid');

        $this->load->model('getdata');
        if($userregistrationid == '')
        {
            $data['products']=$this->getdata->getall();
            //print_r($data['products']);exit;
        }
        else{
            $hideproduct=$this->getdata->hideproduct($userregistrationid);
            //print_r($hideproduct);
            $data['products']=$this->getdata->removehideproduct($hideproduct);
            //print_r($data['products']); exit;
        }
        $data['avali_cat']=$this->getdata->availcat();
        $data['all_products_count']=$this->getdata->record_count();

        //print_r($data['avali_cat']); exit;
        $data['slider_image']=$this->getdata->slider('page', 'main');
        $data['seo']=$this->getdata->seo('page','home');
        $data['user_follow']=$this->getdata->get_user_follow($userregistrationid);// To get user follow details
        $data['user_crown']=$this->getdata->get_user_crown($userregistrationid);// To get user crown details
        $data['user_saveforlater'] = $this->getdata->get_saveforlater($userregistrationid);
        $data['userdata'] = $this->getdata->getuserdata($userregistrationid);
        //print_r($data);
        $data['all_cat']=$this->getdata->get_all_category();//for getting all the category in the head section
        $data['heading']="See whats new here !";
        $this->load->view('pages/head_main.php',$data);//passing category in the head section
        $this->load->view('pages/pagenotfound');//loading view
    }
}
?>
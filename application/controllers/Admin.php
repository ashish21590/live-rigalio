<?php
    class Admin extends CI_Controller
    {

        public function __construct()
        {
            parent::__construct();
//$this->output->enable_profiler(True );

            $this->load->helper('url_helper');
        $this->load->helper('form');
        $this->load->library('form_validation');
       $this->load->model('Admin_model');
        }


public function index(){
    $this->load->helper(array('form'));
     // $this->load->view('admin/header');      
   $this->load->view('admin/login_view');
}


      public function insertBrand(){

         // $this->load->view('admin/insertBrand');

          $this->load->view('admin/header');      
            $this->load->view('admin/sidebarmenu');       
            $this->load->view('admin/addbrand');      
            $this->load->view('admin/footer');        

    }

        public function ins_brand()
        {

           // $this->load->helper(array('form', 'url'));
                  

            $this->form_validation->set_rules('brand_name', 'Please Enter Brand Name', 'required');
           // $this->form_validation->set_rules('brand_image', 'Please Enter Brand Logo', 'required');


            if ($this->form_validation->run() == FALSE) {

           
              $data['msg']=validation_errors();

          $this->load->view('admin/header');      
            $this->load->view('admin/sidebarmenu');       
            $this->load->view('admin/addbrand',$data);      
            $this->load->view('admin/footer');  

            } else {

            

                $config = array();
                $config['upload_path'] = 'uploads/brand/';
                $config['allowed_types'] = '*';
                $config['max_size'] = '10000000';
                $config['max_width'] = '102400000';
                $config['max_height'] = '76800';
                $this->load->library('upload', $config, 'coverupload'); // Create custom object for cover upload
                $this->coverupload->initialize($config);
                $upload_cover = $this->coverupload->do_upload('brand_image');
               //print_r($upload_cover); 
                // Catalog upload
                $config = array();
                $config['upload_path'] = 'content/images/news-post-inner/';
                $config['allowed_types'] = '*';
                $config['max_size'] = '1000000';
                $this->load->library('upload', $config, 'catalogupload');  // Create custom object for catalog upload
                $this->catalogupload->initialize($config);
                $upload_catalog = $this->catalogupload->do_upload('cover_image');
//echo "Ashish"; exit;

                // Check uploads success

                // echo $config["upload_path"];
                // if ($upload_cover && $upload_catalog) {
                if ($upload_cover) {

                    // Both Upload Success

                    // Data of your cover file
                    $cover_data = $this->coverupload->data();
                    // print_r($cover_data);

                    // Data of your catalog file
                    $catlog_data = $this->catalogupload->data();
                  //  print_r($catlog_data);
                   

                    $this->load->model("Admin_model");
                    $check_insert = $this->Admin_model->insert_model($cover_data['file_name'], $catlog_data['file_name']);
                   // echo $check_insert; exit;

                    if ($check_insert > 0) {
                        $data['msg']="Brand Inserted successfully";

          $this->load->view('admin/header');      
            $this->load->view('admin/sidebarmenu');       
            $this->load->view('admin/addbrand',$data);      
            $this->load->view('admin/footer');       


                    } else {
                        echo "Please try again";
                    }


                } else {

                  //  echo "piyush"; exit;

                    // Error Occured in one of the uploads

                  //  echo 'Cover upload Error : ' . $this->coverupload->display_errors() . '<br/>'; 
                   // echo 'Catlog upload Error : ' . $this->catalogupload->display_errors() . '<br/>';

            $data['msg']=$this->coverupload->display_errors();

          $this->load->view('admin/header');      
            $this->load->view('admin/sidebarmenu');       
            $this->load->view('admin/addbrand',$data);      
            $this->load->view('admin/footer');       
                }
            }
        }

// verify login function

         function verifylogin()
 { 

    $this->load->model('Admin_model');
   $this->load->library('form_validation');
   $this->form_validation->set_rules('username', 'Username', 'trim|required');
   $this->form_validation->set_rules('password', 'Password', 'trim|required');
   if($this->form_validation->run() == FALSE)
   {
     $this->load->view('admin/login_view');
   }
   else
   {
      $username = $this->input->post('username');
      $password = $this->input->post('password');
     $result = $this->Admin_model->login($username, $password);
   
     $sess_array = array();
     foreach($result as $row)
     {
       $sess_array = array(
         'id' => $row->id,
         'username' => $row->username,
         'password' => $row->password
       );
     } 
    $this->session->set_userdata('logged_in', $sess_array);
      redirect('admin/home');
   }
 }


 public function home()
 {
     //echo $this->session->userdata('id'); 
      //echo $this->session->userdata('username'); 
   
   if($this->session->userdata('logged_in'))
   {
     $session_data = $this->session->userdata('logged_in');
     $data['username'] = $session_data['username'];
    // $this->load->view('home_view', $data);
            $this->load->view('admin/header');      
            $this->load->view('admin/sidebarmenu', $data);      
            $this->load->view('admin/Dashboard');       
            $this->load->view('admin/footer');
     //redirect('index.php/admin', $data);
   }
   else
   {
     //If no session, redirect to login page
     redirect('Admin');
   }
 }


public function managebrand()
        {
        
        $this->load->view('admin/header');        
        $this->load->view('admin/sidebarmenu');       
        $result = $this->Admin_model->list_brand();      
        $data['get_categotylist'] = $result;
        //$d['temp']=$this->productModel->list_product();
        $this->load->view('admin/managebrand',$data);     
        $this->load->view('admin/footer');
        //$d['rs']=$this->productModel->list_category();
        }

    public function deletebrand()
        {
        $id = $this->uri->segment(3);
        $this->Admin_model->delete_brand($id);
        redirect("Admin/managebrand");
        }
    
    public function isActivebrand()
        {
        
        $id = $this->uri->segment(3);
        $isActive = $this->uri->segment(4);
        //echo $isActive; exit;
        $this->Admin_model->brand_active($id,$isActive);
        redirect($_SERVER['HTTP_REFERER']);
        
        }       
        
    public function editBrand()
        {
        $id = $this->uri->segment(3);       
        //echo $id;     
        if($id){
        $this->load->view('admin/header');        
        $this->load->view('admin/sidebarmenu');       
        $result = $this->Admin_model->get_brand($id);        
        $data['get_category'] = $result;        
        $this->load->view('admin/editBrand', $data);      
        $this->load->view('admin/footer');
    }else{
         redirect("Admin/managebrand");
        }
    }
        
    public function updateBrand($item=null)
        {   
        
        $data['base_path']       = base_url();
        $config = array();
        $config['upload_path']   =   "uploads/brand/";
        $config['allowed_types'] =   "gif|jpg|jpeg|png";
        $config['max_size']      =   "500000";
        $config['max_width']     =   "11907";
        $config['max_height']    =   "11280";

        $this->load->library('upload',$config);
            
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');  
        
        $this->form_validation->set_rules('brandname', 'brand Name', 'required');       
        $this->form_validation->set_rules('brandyear', 'Establish year');
        $this->form_validation->set_rules('branddisc', 'Brand Discription');
        $this->form_validation->set_rules('brandsmalldisc', 'Brand Small Discription');
        $this->form_validation->set_rules('brandlongdisc', 'Brand Long Discription');
        $this->form_validation->set_rules('brandtitle', 'Brand Title');
        $this->form_validation->set_rules('gender', 'Brand Gender');    
        $this->form_validation->set_rules('userfile', 'Upload Image');  
        
     if ($this->form_validation->run() === FALSE)
        {
           // $data= $this->SliderModel->get_slider($item); 
            //$data['page_title'] = 'Update news';
            //$data['base_path'] = base_url();
            //$d['rs']=$this->productModel->list_category();
            $this->load->view('admin/header');        
            $this->load->view('admin/sidebarmenu');       
            $this->load->view('admin/editnews', $data);      
            $this->load->view('admin/footer');    
        }   
         else
        {           
            $fullFilePath = null;
            if (!$this->upload->do_upload())
            {
             $data['base_path'] = base_url();
             $data = array(
            'brand_Name' => $this->input->post('brandname'),
            'Establish' => $this->input->post('brandyear'),
            'brandDescription' => $this->input->post('branddisc'),
            'legacy' => $this->input->post('legacy'),
            'brandSmallDescription' => $this->input->post('brandsmalldisc'),
            'title' => $this->input->post('brandtitle'),
            //'brand_image' => $file,
        );          
                $this->Admin_model->update_brand($data);             
            }
            else {
            
            $filedetails = $this->upload->data();
           /* $fullFilePath = base_url()."application/views/uploads/".$filedetails['file_name'];*/
            $fullFilePath = "uploads/brand/".$filedetails['file_name'];
            $data = array( 
             'brand_Name' => $this->input->post('brandname'),
            'Establish' => $this->input->post('brandyear'),
            'brandDescription' => $this->input->post('branddisc'),
            'legacy' => $this->input->post('legacy'),
            'brandSmallDescription' => $this->input->post('brandsmalldisc'),
            'title' => $this->input->post('brandtitle'),
            'brand_image' => $fullFilePath,              
        );
                $saveddata = $this->Admin_model->update_brand($data);
                
         }      
                
            $this->load->view('admin/header');        
            $this->load->view('admin/sidebarmenu');       
            $this->load->view('admin/managebrand');       
            $this->load->view('admin/footer');        
        }       
    }

    public function product(){

            $this->load->view('admin/header');        
            $this->load->view('admin/sidebarmenu');       
            $this->load->view('admin/addproduct');        
            $this->load->view('admin/footer');        
    }


public function insertproduct(){
        
        $data['base_path'] = base_url();
        $config['upload_path']   =   "uploads/product/";
        $config['allowed_types'] =   "gif|jpg|jpeg|png";
        $config['max_size']      =   "500000";
        $config['max_width']     =   "11907";
        $config['max_height']    =   "11280";

        $this->load->library('upload',$config);     
            
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');      
        $this->form_validation->set_rules('productname', 'Product Name', 'required');       
        $this->form_validation->set_rules('productprice', 'Product Price');
        $this->form_validation->set_rules('productmodel', 'Product Model');
        $this->form_validation->set_rules('productsmalldisc', 'Product Small Discription');
        $this->form_validation->set_rules('productlongdisc', 'Product Long Discription');
            $this->form_validation->set_rules('productoverview', 'Product Overview');
        $this->form_validation->set_rules('producttitle', 'Product Title');
        $this->form_validation->set_rules('gender', 'Gender');
        $this->form_validation->set_rules('productcat', 'Product Category');
        $this->form_validation->set_rules('productsubcat', 'Product Sub Category'); 
        $this->form_validation->set_rules('productbrand', 'Product Brand'); 
        $this->form_validation->set_rules('userfile', 'required');
        
        $d['rs']=$this->Admin_model->list_category();
        $d['subcategory']=$this->Admin_model->list_subcategory();
        $brand['result']=$this->Admin_model->list_brand();
        
        if ($this->form_validation->run() == FALSE) 
        {
        
        //$d['rs']=$this->productModel->list_category();
        
        //$brand['result']=$this->productModel->list_brand();
            
        $this->load->view('admin/header');        
        $this->load->view('admin/sidebarmenu',$d);        
        $this->load->view('admin/addproduct',$brand);     
        $this->load->view('admin/footer');        
        //print_r($brand['result']);
        } 
        else
         {
             $files = $_FILES;
             $cpt = count($_FILES['userfile']['name']);
        
         for($i=0; $i<$cpt; $i++)
          {
           $_FILES['userfile']['name']= $files['userfile']['name'][$i];
           $_FILES['userfile']['type']= $files['userfile']['type'][$i];
           $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
           $_FILES['userfile']['error']= $files['userfile']['error'][$i];
           $_FILES['userfile']['size']= $files['userfile']['size'][$i];
           
         $data['base_path'] = base_url();
        $config['upload_path']   =   "uploads/product/";
        $config['allowed_types'] =   "gif|jpg|jpeg|png";
        $config['max_size']      =   "500000";
        $config['max_width']     =   "11907";
        $config['max_height']    =   "11280";
        
         $this->load->library('upload',$config);
        
             $fullFilePath = null;
            if (!$this->upload->do_upload())
            {
                $error = array('error' => $this->upload->display_errors());
               // print_r($error);
                $this->load->view('admin/header');        
                $this->load->view('admin/sidebarmenu');       
                $this->load->view('admin/addproduct');        
                $this->load->view('admin/footer');    
            }
            else
            {
                $filedetails = $this->upload->data();
               /* $fullFilePath = base_url()."application/views/uploads/".$filedetails['file_name'];*/
                //$fullFilePath = "uploads/product/".$filedetails['file_name'];
                //$saveddata = $this->productModel->product_insert($fullFilePath);
                 $fullFilePathnew = "uploads/product/".$filedetails['file_name'];
                $fullFilePath[] = $fullFilePathnew;
              //  print_r($saveddata);
                //echo "Data Saved Successfully";
            }
            $fileName = implode(',',$fullFilePath);
             $imageinsertid = $this->Admin_model->product_insert($fileName); //inserting data of the

           // $saveddata = $this->Admin_model->inserProductImage($fileName, $imageinsertid);
          }


            $this->load->view('admin/header');        
            $this->load->view('admin/sidebarmenu', $d);       
            $this->load->view('admin/addproduct', $brand);        
            $this->load->view('admin/footer');    
        }
    }
    
    public function manageProduct()
        {

        $config = array();
        $config["base_url"] = base_url() . "Admin/manageProduct";
        $config["total_rows"] = $this->Admin_model->p_count();
        $config["per_page"] = 20;
        $config["uri_segment"] = 3;

         $this->pagination->initialize($config);

        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["get_categotylist"] = $this->Admin_model->
            fetch_product($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        $this->load->view('admin/header');        
        $this->load->view('admin/sidebarmenu');       
        //$result = $this->Admin_model->list_product();      
        //$data['get_categotylist'] = $result;
        //$d['temp']=$this->productModel->list_product();
        $this->load->view('admin/manageproduct',$data);       
        $this->load->view('admin/footer');
        //$d['rs']=$this->productModel->list_category();
        }
        
    public function listProductimage()
        {
        $id = $this->uri->segment(3);
        $this->load->view('header');        
        $this->load->view('sidebarmenu');       
        $result = $this->Admin_model->list_productimage($id);      
        $data['get_productimagelist'] = $result;
        //$d['temp']=$this->productModel->list_product();
        $this->load->view('deleteproductimage',$data);      
        $this->load->view('footer');
        //$d['rs']=$this->productModel->list_category();
        }

    public function deleteProduct()
        {
        $id = $this->uri->segment(3);
        $this->Admin_model->delete_product($id);
        redirect($_SERVER['HTTP_REFERER']);
        }
        
    public function deleteProductimage()
        {
        $id = $this->uri->segment(3);
        $this->Admin_model->delete_product_image($id);
        redirect($_SERVER['HTTP_REFERER']);
        
        }
    
    public function isActiveProduct()
        {
        $id = $this->uri->segment(3);
        $isActive = $this->uri->segment(4);
        $this->Admin_model->product_active($id,$isActive);
        redirect($_SERVER['HTTP_REFERER']);
        }       
        
    public function editProduct()
        {
        
        $id = $this->uri->segment(3);       
        $this->load->view('admin/header');        
        $result = $this->Admin_model->get_product($id);
        $data['get_category'] = $result;
        $d['rs']=$this->Admin_model->list_category();
        $d['subcategory']=$this->Admin_model->list_subcategory();
        $d['result']=$this->Admin_model->list_brand();
        $d['image']=$this->Admin_model->product_image($id);
        $this->load->view('admin/sidebarmenu', $d);       
        $this->load->view('admin/editProduct', $data);        
        $this->load->view('admin/footer');
        //print_r($brand);
        }
    
    public function editProductimage()
        {
        $id = $this->uri->segment(3);       
        $this->load->view('admin/header');        
        $result = $this->Admin_model->get_product_image($id);
        $data['get_category'] = $result;
        //print_r($data); 
        $this->load->view('admin/sidebarmenu');       
        $this->load->view('admin/editProductimage', $data);       
        $this->load->view('admin/footer');
        //print_r($brand);
        }   
    
    public function updateProduct($item=null)
        {
        $data['base_path']       = base_url();
        $config['upload_path']   =   "uploads/product/";
        $config['allowed_types'] =   "gif|jpg|jpeg|png";
        $config['max_size']      =   "500000";
        $config['max_width']     =   "11907";
        $config['max_height']    =   "11280";

        $this->load->library('upload',$config);
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');  
        $this->form_validation->set_rules('productname', 'Product Name', 'required|min_length[3]|max_length[15]');      
        $this->form_validation->set_rules('productprice', 'Product Price');
        $this->form_validation->set_rules('productmodel', 'Product Model');
        $this->form_validation->set_rules('productcolor', 'Product Color');
        $this->form_validation->set_rules('productsmalldisc', 'Product Small Discription');
        $this->form_validation->set_rules('productlongdisc', 'Product long Discription');
        $this->form_validation->set_rules('producttitle', 'Product Title'); 
        $this->form_validation->set_rules('gender', 'gender');  
        $this->form_validation->set_rules('productcat', 'Product Category');
        $this->form_validation->set_rules('productbrand', 'Product Brand');         
        $this->form_validation->set_rules('userfile', 'Upload Image');  
        
     if ($this->form_validation->run() === FALSE)
        {
            $this->load->view('admin/header');        
            $this->load->view('admin/sidebarmenu');       
            $this->load->view('admin/editProduct', $data);        
            $this->load->view('admin/footer');    
        }   
         else
        {           
            $fullFilePath = null;
            if (!$this->upload->do_upload())
            {
        
              //  $error = array('error' => $this->upload->display_errors());
                // print_r($error);
             $data['base_path'] = base_url();
            $data = array(
            'product_name' => $this->input->post('productname'),
            'Price' => $this->input->post('productprice'),
            'model_no' => $this->input->post('productmodel'),
            'productSmallDiscription' => $this->input->post('productsmalldisc'),
            'productlongDiscription' => $this->input->post('productlongdisc'),
            'product_Title' => $this->input->post('producttitle'),
            'gender' => $this->input->post('gender'),
            'categoryId_Fk' => $this->input->post('productcat'),
            'brandId_Fk' => $this->input->post('productbrand'),             
        );          
        
                $this->Admin_model->update_product($data);             
                
            }
        else {
                $filedetails = $this->upload->data();
                $fullFilePath = "uploads/productimage/".$filedetails['file_name'];
                $data = array( 
            
            'product_name' => $this->input->post('productname'),
            'Price' => $this->input->post('productprice'),
            'model_no' => $this->input->post('productmodel'),
            'productSmallDiscription' => $this->input->post('productsmalldisc'),
            'productlongDiscription' => $this->input->post('productlongdisc'),
            'product_Title' => $this->input->post('producttitle'),
            'gender' => $this->input->post('gender'),
            'categoryId_Fk' => $this->input->post('productcat'),
            'brandId_Fk' => $this->input->post('productbrand'),
            'product_image' => $fullFilePath,                 
        );
        
                $saveddata = $this->Admin_model->update_product($data);
                //print_r($saveddata);
            }
            
            redirect('Admin/manageProduct');        
                
            //$this->load->view('header');      
            //$this->load->view('sidebarmenu');     
            //$this->load->view('Dashboard.php');       
            //$this->load->view('footer');      
        }       
    }   
    
    public function updateProductimage($item=null)
        {   
         
        $data['base_path']       = base_url();
        $config['upload_path']   =   "uploads/product/";
        $config['allowed_types'] =   "gif|jpg|jpeg|png";
        $config['max_size']      =   "500000";
        $config['max_width']     =   "11907";
        $config['max_height']    =   "11280";

        $this->load->library('upload', $config);
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $this->form_validation->set_rules('userfile', 'required');
        //print_r($me); 
    /*  
     if ($this->form_validation->run() == FALSE)
        {
            echo "Ashish"; 
           // $data= $this->SliderModel->get_slider($item); 
            //$data['page_title'] = 'Update news';
            //$data['base_path'] = base_url();
            $this->load->view('header');        
            $this->load->view('sidebarmenu');       
            $this->load->view('editProductimage');      
            $this->load->view('footer');    
        }
         else
        {           
        echo "Ashish1"; exit;*/   
                $fullFilePath = null;
            if (!$this->upload->do_upload())
            {
                echo "Ashish2"; 
              //  $error = array('error' => $this->upload->display_errors());
                // print_r($error);
             $data['base_path'] = base_url();
             echo  $this->input->post('imgbrandid'); exit;
             $data = array(
            'brandId_Fk' => $this->input->post('imgbrandid'),
            'productId_Fk' => $this->input->post('imgprodid'), 
        );          
                $this->Admin_model->update_productimage($data);
            }
            else
             {
                 echo "Ashish3";
                $filedetails = $this->upload->data();
                $fullFilePath = "uploads/product/".$filedetails['file_name'];
                $data = array(
            'brandId_Fk' => $this->input->post('imgbrandid'),
            'productId_Fk' => $this->input->post('imgprodid'), 
            'image_Name' => $fullFilePath                 
             );
            //print_r($data);
                $saveddata = $this->Admin_model->update_productimage($data);
            }
            
            redirect('index.php/product/manageProduct');    
       // }
    }
    
    public function getsubcategory()
        {
            //echo $_POST['categoryid'];
            if($this->input->post('categoryid'))
            {
            $category=$this->input->post('categoryid');
            $getcategory=$this->input->post('categoryid');
            }
            $data['get_subcategory'] = $this->Admin_model->subcategory($getcategory);
            $this->load->view('admin/getsubcategory', $data);     
         }


        public function addspecificationtype()
        {

            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            $this->form_validation->set_rules('specificationtype', 'specification type', 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->load->view('header');
                $this->load->view('sidebarmenu');
                $this->load->view('addspecificationtype');
                $this->load->view('footer');
            } else {
                $saveddata = $this->Admin_model->specificationType_insert();
                //redirect('index.php/specification/managespecificationtype');
            }
        }

       /* public function managespecificationtype()
        {

            $this->load->view('header');
            $this->load->view('sidebarmenu');
            $result = $this->specificationModel->list_specificationType();
            $data['specifictionType'] = $result;
            $this->load->view('managespecificationtype', $data);
            $this->load->view('footer');
        }
*/

    public function deletespecifictionType()
        {
        $id = $this->uri->segment(3);
        $this->specificationModel->delete_specifictionType($id);
        redirect($_SERVER['HTTP_REFERER']);
        }
    
    public function isActivespecifictionType()
        {

            $id = $this->uri->segment(3);
            $isActive = $this->uri->segment(4);
            $this->Admin_model->specifictionType_active($id, $isActive);
            redirect($_SERVER['HTTP_REFERER']);
        }

        public function editspecifictionType()
        {
        $id = $this->uri->segment(3);       
        $this->load->view('header');        
        $this->load->view('sidebarmenu');       
        $result = $this->specificationModel->get_specifictionType($id);     
        $data['get_category'] = $result;        
        $this->load->view('editspecifictionType', $data);       
        $this->load->view('footer');
        }
        
    public function updatespecifictionType($item=null)
        {   
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');  
        $this->form_validation->set_rules('specificationname', 'specification name', 'required');       
     if ($this->form_validation->run() == FALSE)
        {
            redirect('index.php/specification/editspecifictionType');
        }   
         else
        {           
             $data = array(
            'specifictionType_Name' => $this->input->post('specificationname'),
             );         
                $saveddata = $this->specificationModel->update_specifictionType($data);
         }      
            redirect('index.php/specification/managespecificationtype');    
    }
    
/*--------------------------------specification type end-------------------------------------*/

/*--------------------------------specification name start---------------------------*/

        public function addspecificationname()
        {

            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            $this->form_validation->set_rules('specificationname', 'specification name', 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->load->view('admin/header');
                $this->load->view('admin/sidebarmenu');
                $this->load->view('admin/addspecificationname');
                $this->load->view('admin/footer');
            } else {
                $saveddata = $this->Admin_model->specificationName_insert();
                // redirect('index.php/specification/managespecificationname');
            }
    }
    
    public function managespecificationname()
        {
        
        $this->load->view('header');        
        $this->load->view('sidebarmenu');       
        $result = $this->specificationModel->list_specificationName();      
        $data['specifictionType'] = $result;
        $this->load->view('managespecificationname',$data);     
        $this->load->view('footer');
        }
    
    public function deletespecifictionName()
        {
        $id = $this->uri->segment(3);
        $this->specificationModel->delete_specifictionName($id);
        redirect($_SERVER['HTTP_REFERER']);
        }
    
    public function isActivespecifictionName()
        {
        
        $id = $this->uri->segment(3);
        $isActive = $this->uri->segment(4);
        $this->specificationModel->specifictionName_active($id,$isActive);
        redirect($_SERVER['HTTP_REFERER']);
        }
        
    public function editspecifictionName()
        {
        $id = $this->uri->segment(3);       
        $this->load->view('header');        
        $this->load->view('sidebarmenu');       
        $result = $this->specificationModel->get_specifictionName($id);     
        $data['get_category'] = $result;        
        $this->load->view('editspecifictionName', $data);       
        $this->load->view('footer');
        }   
    
    public function updatespecifictionName($item=null)
        {   
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');  
        $this->form_validation->set_rules('specificationname', 'specification name', 'required');       
     if ($this->form_validation->run() == FALSE)
        {
            redirect('index.php/specification/editspecifictionName');
        }   
         else
        {           
             $data = array(
            'specification_Name' => $this->input->post('specificationname'),
             );         
                $saveddata = $this->specificationModel->update_specifictionName($data);
         }      
            redirect('index.php/specification/managespecificationname');    
    }


     function specification($product_id){

        $data['products']=$this->Admin_model->all_product();
        $data['specification_name']=$this->Admin_model->list_specification_name();
        $data['specification_type']=$this->Admin_model->list_specification_type();

        $this->load->view('admin/header');        
       // $this->load->view('admin/sidebarmenu');       
        $this->load->view('admin/addspecification',$data);      
       // $this->load->view('admin/footer');        

    }


    public function insertproductspecification(){

         $p_id= $this->uri->segment(3); 
         $specificationNameId =$this->input->post('specificationNameId');
         $specification_type=  $this->input->post('specification_type');
         $specification_desc= $this->input->post('specification_desc');


          //foreach($filename1 as $file){
        $data = array(
            'specification_Desc' => $specification_desc,
            'productId_Fk' => $p_id,
            'specificationTypeId_Fk' =>   $specification_type,
            'specificationNameId_Fk' => $specificationNameId
           
           
            //'brand_image' => $file,
        ); 

      $msg=$this->Admin_model->addspecifications($data);

      if($msg>0){
                 $data['msg']="Inserted successfully";

                 redirect("/Admin/specification/".$p_id."");
      }
      else{
     $data['msg']="Please Try again"; 
     redirect("/Admin/specification/'".$p_id."'");
      }
        
    }

        public function managespecificationtype()
        {

            $this->load->view('admin/header');
            $this->load->view('admin/sidebarmenu');
            $result = $this->Admin_model->list_specificationType();
            $data['specifictionType'] = $result;
            $this->load->view('admin/managespecificationtype', $data);
            $this->load->view('admin/footer');
        }

        public function insertbrandnews()
        {
            $data['brand_name'] = $this->Admin_model->brand_name();
            // $this->load->view('admin/insertBrand');
            $this->load->view('admin/header');
            //  $this->load->view('admin/sidebarmenu');       
            $this->load->view('admin/addbrandnews', $data);
            // $this->load->view('admin/footer');        

        }

        public function ins_brandnews()
        {
            $data['brand_name'] = $this->Admin_model->brand_name();

            // $this->load->helper(array('form', 'url'));
            $this->form_validation->set_rules('brandname', 'Please Enter Brand Name', 'required');
            // $this->form_validation->set_rules('brand_image', 'Please Enter Brand Logo', 'required');


            if ($this->form_validation->run() == FALSE) {
                $data['msg'] = validation_errors();
                $this->load->view('admin/header');
                //$this->load->view('admin/sidebarmenu');       
                $this->load->view('admin/addbrandnews', $data);
                $this->load->view('admin/footer');

            } else {
                $config = array();
                $config['upload_path'] = 'content/images/news-post/';
                $config['allowed_types'] = '*';
                $config['max_size'] = '10000000';
                $config['max_width'] = '102400000';
                $config['max_height'] = '76800';
                $this->load->library('upload', $config, 'coverupload'); // Create custom object for cover upload
                $this->coverupload->initialize($config);
                $upload_cover = $this->coverupload->do_upload('news_blockimage');
                //print_r($upload_cover); 
                // Catalog upload
                $config = array();
                $config['upload_path'] = 'content/images/news-post-inner/';
                $config['allowed_types'] = '*';
                $config['max_size'] = '1000000';
                $this->load->library('upload', $config, 'catalogupload');  // Create custom object for catalog upload
                $this->catalogupload->initialize($config);
                $upload_catalog = $this->catalogupload->do_upload('news_bannerimage');
                // Check uploads success
                // echo $config["upload_path"];
                // if ($upload_cover && $upload_catalog) {
                if ($upload_cover) {

                    // Both Upload Success

                    // Data of your cover file
                    $cover_data = $this->coverupload->data();
                    // print_r($cover_data);

                    // Data of your catalog file
                    $catlog_data = $this->catalogupload->data();
                    //  print_r($catlog_data);
                    $this->load->model("Admin_model");
                    $check_insert = $this->Admin_model->brandnews_insert($cover_data['file_name'], $catlog_data['file_name']);
                    // echo $check_insert; exit;
                    if ($check_insert > 0) {
                        $data['msg'] = "Brand Inserted successfully";

                        $this->load->view('admin/header');
                        //$this->load->view('admin/sidebarmenu');       
                        $this->load->view('admin/addbrandnews', $data);
                        $this->load->view('admin/footer');


                    } else {
                        echo "Please try again";
                    }


                } else {
                    $data['msg'] = $this->coverupload->display_errors();

                    $this->load->view('admin/header');
                    // $this->load->view('admin/sidebarmenu');       
                    $this->load->view('admin/addbrandnews', $data);
                    $this->load->view('admin/footer');
                }
            }
        }

        public function managebrandnews()
        {
            $this->load->view('admin/header');
            $this->load->view('admin/sidebarmenu');
            $list_brandnews = $this->Admin_model->list_brandnews();
            $data['list_brandnews'] = $list_brandnews;
            //$d['temp']=$this->productModel->list_product();
            $this->load->view('admin/managebrandnews', $data);
            $this->load->view('admin/footer');
            //$d['rs']=$this->productModel->list_category();
        }
		
	public function insertcategory(){
		
		$data['base_path'] = base_url();
        $config['upload_path']   =   "uploads/category/";
        $config['allowed_types'] =   "gif|jpg|jpeg|png";
        $config['max_size']      =   "500000";
        $config['max_width']     =   "11907";
        $config['max_height']    =   "11280";

        $this->load->library('upload',$config);		
			
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');		
		$this->form_validation->set_rules('catname', 'Category Name', 'required');		
		$this->form_validation->set_rules('cattitle', 'Category Title');	
		$this->form_validation->set_rules('catdisc', 'Category discription');
		$this->form_validation->set_rules('catkeyword', 'Category keyword');	
		$this->form_validation->set_rules('userfile', 'Upload Image');
		
		if ($this->form_validation->run() == FALSE) 
		{
		$this->load->view('admin/header');		
		$this->load->view('admin/sidebarmenu');		
		$this->load->view('admin/addcategory');		
		$this->load->view('admin/footer');		
		} 
		else
		 {
			 $fullFilePath = null;
            if (!$this->upload->do_upload())
            {
				$error = array('error' => $this->upload->display_errors());
                print_r($error);
				$this->load->view('admin/header');		
				$this->load->view('admin/sidebarmenu');		
				$this->load->view('admin/addcategory');		
				$this->load->view('admin/footer');	
			}
            else
            {
				$filedetails = $this->upload->data();
               /* $fullFilePath = base_url()."application/views/uploads/".$filedetails['file_name'];*/
			    $fullFilePath = "uploads/category/".$filedetails['file_name'];
                $saveddata = $this->categoryModel->category_insert($fullFilePath);
              //  print_r($saveddata);
                //echo "Data Saved Successfully";
            }
			$this->load->view('admin/header');		
			$this->load->view('admin/sidebarmenu');		
			$this->load->view('admin/addcategory.php');		
			$this->load->view('admin/footer');	
        }
    }
	
		public function search_product()
        {


       $product_name=$this->input->post('product_name');

      $config = array();
        $config["base_url"] = base_url() . "Admin/manageProduct";
        $config["total_rows"] = $this->Admin_model->p_count_search($product_name);
        $config["per_page"] = 20;
        $config["uri_segment"] = 3;

         $this->pagination->initialize($config);

        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["get_categotylist"] = $this->Admin_model->
            fetch_product_search($config["per_page"], $page,$product_name);
        $data["links"] = $this->pagination->create_links();
        $this->load->view('admin/header');        
        $this->load->view('admin/sidebarmenu');       
        //$result = $this->Admin_model->list_product();      
        //$data['get_categotylist'] = $result;
        //$d['temp']=$this->productModel->list_product();
        $this->load->view('admin/manageproduct',$data);       
        $this->load->view('admin/footer');


        }

	public function insertProductHighlights()
	{
		$data['all_product']=$this->Admin_model->all_product();
        $this->load->view('admin/header');      
        $this->load->view('admin/addprodhighlight', $data);      
    }
   
	public function insertHighlights()
	{
		$data['all_product']=$this->Admin_model->insert_highlight();
		 redirect("/Admin/insertProductHighlights/");
      
    }
	
	public function manageProductHighlights()
	{
		$data['product_highlight']=$this->Admin_model->producthighlight();
        $this->load->view('admin/header');
		$this->load->view('admin/sidebarmenu');		
        $this->load->view('admin/manageprodhighlight', $data);  
		$this->load->view('admin/footer');    
    }
	
	public function editProductHighlights()
	{
		
		$hid = $this->uri->segment(3);
		//echo $hid ; exit;
		$data['product_info']=$this->Admin_model->getprodinfo($hid);
		//print_r($data['product_info']); exit;
        $this->load->view('admin/header');
		$this->load->view('admin/sidebarmenu');		
        $this->load->view('admin/editprodhighlight', $data);  
		$this->load->view('admin/footer');    
    }
	
	public function updateProductHighlights()
	{
		$data = array(
            'highlight' => $this->input->post('highlight')
       		 );
	    $data['updateproduct_highlight']=$this->Admin_model->update_highlight($data);
		 redirect("/Admin/manageProductHighlights/");
          
    }
	
	public function deleteProductHighlights()
		{
		$hid = $this->uri->segment(3);
		$this->Admin_model->delete_highlight($hid);
		redirect($_SERVER['HTTP_REFERER']);
		}

public function insertProductslider()
	{
		$data['all_product']=$this->Admin_model->all_product();
        $this->load->view('admin/header');      
        $this->load->view('admin/addprodslider', $data);      
    }
	
	public function insertslider()
	{
		$data['all_product']=$this->Admin_model->insert_highlight();
		 redirect("/Admin/insertProductHighlights/");
      
    }

    public function editbrandnews($news_id){

        //print_r($data['product_info']); exit;

        $news_id=$this->uri->segment(3);
        $data['news']=$this->Admin_model->get_all_news($news_id);
        $data['brand_name'] = $this->Admin_model->brand_name();

        $this->load->view('admin/header');
       // $this->load->view('admin/sidebarmenu');     
        $this->load->view('admin/editnews', $data);  
        $this->load->view('admin/footer');    

    }

    public function update_news($news_id){

        $news_id=$this->uri->segment(3);
        if(!$news_id){

        }
    else{


 $config = array();

 // uploading for the block image 
                $config['upload_path'] = 'content/images/news-post/';
                $config['allowed_types'] = '*';
                $config['max_size'] = '10000000';
                $config['max_width'] = '102400000';
                $config['max_height'] = '76800';
                $this->load->library('upload', $config, 'coverupload'); // Create custom object for cover upload
                $this->coverupload->initialize($config);
                $upload_cover = $this->coverupload->do_upload('news_blockimage');
             //   print_r($upload_cover);  

             //   echo "....................";
                // Catalog upload

                //uploading for the banner image
                $config = array();
                $config['upload_path'] = 'content/images/news-post-inner/';
                $config['allowed_types'] = '*';
                $config['max_size'] = '1000000';
                $this->load->library('upload', $config, 'catalogupload');  // Create custom object for catalog upload
                $this->catalogupload->initialize($config);
                $upload_catalog = $this->catalogupload->do_upload('news_bannerimage');

                $catalogupload = $this->catalogupload->data();
                 $coverupload = $this->coverupload->data();


//echo $catalogupload['file_name'];

//echo ".........";
echo  $coverupload['file_name']; 
                //print_r($upload_catalog ); exit;
                // Check uploads success
                // echo $config["upload_path"];
                // if ($upload_cover && $upload_catalog) {
            if ($coverupload['file_name'] && $catalogupload['file_name']=="") {
                    $cover_data = $this->coverupload->data();
                    // Data of your catalog file
                  
                    //  print_r($catlog_data);
                    $this->load->model("Admin_model");
                    $check_insert = $this->Admin_model->update_news_1($cover_data['file_name'],$news_id);
                  //  $data['news']=$this->Admin_model->get_all_news($news_id);
                redirect($_SERVER['HTTP_REFERER']);

}
else if($catalogupload['file_name'] && $coverupload['file_name']==""){

    // echo "news_bannerimage"; 

     $catalogupload = $this->catalogupload->data();
               //  print_r($cover_data); exit;

                    // Data of your catalog file
                  //  $catlog_data = $this->catalogupload->data();
                    //  print_r($catlog_data);
                    $this->load->model("Admin_model");
                    $check_insert = $this->Admin_model->update_news_2($catalogupload['file_name'],$news_id);
                    //$data['news']=$this->Admin_model->get_all_news($news_id);
      redirect($_SERVER['HTTP_REFERER']);


}
else if( $catalogupload['file_name'] && $coverupload['file_name']) {


   // echo "ashish"; exit;



    // echo "both"; exit;
               $cover_data = $this->coverupload->data();
                    // print_r($cover_data);

                    // Data of your catalog file
                    $catlog_data = $this->catalogupload->data();
                    //  print_r($catlog_data);
                    $this->load->model("Admin_model");
                    $check_insert = $this->Admin_model->update_news_3($cover_data['file_name'],$catlog_data['file_name'],$news_id);


       redirect($_SERVER['HTTP_REFERER']);

       

    }
    else{
      //  echo "u n"; exit;
         $check=$this->Admin_model->update_news($news_id);
         redirect($_SERVER['HTTP_REFERER']);

    }





    }

}
	
	

    }



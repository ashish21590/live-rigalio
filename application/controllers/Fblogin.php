<?php 
/* -----------------------------------------------------------------------------------------
   IdiotMinds - http://idiotminds.com
   -----------------------------------------------------------------------------------------
*/
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//include the facebook.php from libraries directory
require_once APPPATH.'libraries/facebook/facebook.php';

class Fblogin extends CI_Controller {

   public function __construct(){
	    parent::__construct();
	    $this->load->library('session');  //Load the Session 
		$this->config->load('facebook'); //Load the facebook.php file which is located in config directory
    }
	public function index()
	{
	  $this->load->view('pages/fblogin.php'); //load the main.php file for view
	}
	
	function logout(){
		$base_url=$this->config->item('base_url'); //Read the baseurl from the config.php file
		$this->session->sess_destroy();  //session destroy
	redirect($base_url);  //redirect to the home page
		
	}
	function fblogin(){
		$base_url=$this->config->item('base_url'); //Read the baseurl from the config.php file
		//get the Facebook appId and app secret from facebook.php which located in config directory for the creating the object for Facebook class
    	$facebook = new Facebook(array(
		'appId'		=>  $this->config->item('appID'), 
		'secret'	=> $this->config->item('appSecret'),
		));
		
		$user = $facebook->getUser(); // Get the facebook user id 
		//print_r($user); exit;
		if($user){
			
			try{
				$user_profile = $facebook->api('/me?fields=id,name,email,picture{url},age_range');  //Get the facebook user profile data
				//print_r($user_profile); exit;
				$params = array('next' => $base_url);
				
				$ses_user=array('User'=>$user_profile,
				'logout' =>$facebook->getLogoutUrl($params)   //generating the logout url for facebook 
				);
		        $this->session->set_userdata($ses_user);
				//header('Location: '.$base_url.'fblogin/logout');
			}catch(FacebookApiException $e){
				error_log($e);
				$user = NULL;
			}		
		}	
		//print_r($user_profile); 
		//$first_names = array_column($user_profile, 'picture{url}');
		//print_r($first_names); exit;
                $inviteuserid = $this->session->userdata('inviteuserid');
		 // print_r($inviteuserid); exit;
		  $this->load->model('getdata');
		  $data=$this->getdata->checkuser($user_profile['email']);
               // print_r(count($data)); exit;
		  if(count($data)>0){
		  //echo $registrationid=$data[0]['registrationid'];
		 // $this->session->set_userdata('registrationid',$registrationid);
		  redirect('fblogin/closewindow');
		  }
		  else{
		  $myvalue=array(
		  'firstname' => $user_profile['name'],
		  'email'=>$user_profile['email'],
		  'social_id'=>$user_profile['id'],
		  'userinviteid'=>$inviteuserid,
		  'auth_provider'=>'facebook'
		   );
			//print_r($myvalue); exit;
			$data['user_id']=$this->getdata->insert_fbdetail($myvalue);
			redirect('fblogin/closewindow');

}
				
	}
	
	function closewindow()
	{
	$this->load->view('pages/fbloginonload.php'); 
	}
	
}

/* End of file fbci.php */
/* Location: ./application/controllers/fbci.php */
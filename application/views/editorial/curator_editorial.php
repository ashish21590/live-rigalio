<script src="<?php echo base_url(); ?>content/js/hover.js"></script>
<?php foreach($curator_detail as $details){?>
<div class="curator-banner col-lg-12 nopadding">
<span class="brand-bg-banner" style="background:url(<?php echo base_url(); ?><?php echo $details['banner_img'];?>);"> </span>

   
    <div class="col-lg-10 col-sm-11 col-xs-12 brand-divison nopadding">
        <div class="curatorpic">
            <span style="background:url(<?php echo base_url(); ?><?php echo $details['curator_picture'];?>) no-repeat top center;"> </span>
        </div>
    </div>
</div>


<div class="curator_tagline">
    <div class="col-lg-10 col-md-10 col-sm-11 col-xs-12 curator-logo-sec">
        <div class="col-lg-6 col-sm-12 nopadding">
            <span class="curator-intro"><?php echo $details['curator_name'];?></span>
        </div>
        <div class="col-lg-6 col-sm-12 nopadding inner-up">
            <span class="followers"><img src="<?php echo base_url();?>content/images/icons/followers-icon.png">
            <i id="getcount1"><?php foreach($this->Geteditorial->count_curatorfollower($details['curator_id']) as $count_no){
					if($count_no['no'] != ''){
					 echo $count_no['no']; 
					}
					else
					{
						echo '0';
					}
					 }?> </i>
             </span>
       <span class="curator-follow" id="<?php echo $details['curator_id'];?>">
        <?php
															$me = [];
															foreach($user_follow as $user_foll){
																
																	$me[] = $user_foll['curator_id'];
															}
															$flag=1;
															for($i=0;$i<count($me);$i++){
															if($details['curator_id'] ==$me[$i]){
																$flag=0;
																break;
															}
															}
															if($flag==0)
															{?>
                  <button class="follow-curator-btn1 following1cur" data-text="Follow curator"><i class="fa fa-check"></i></button>
     						 <?php }
							else { ?>
                             <button class="follow-curator-btn1 follow1cur" data-text="Follow curator"><i class="fa fa-check"></i></button>
     				<?php } ?>
      </span>
        </div>

    </div>
</div> <!--/brand_pg_tagline -->
<!--category-pg-con -->
<div class="category-pg-con editorial-pd brand-pg-con curated-abt-con col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
    <div class="container-fluid nomargin nopadding">
        <div class="row">
            <div class="upbrand-bg col-lg-12 nopadding">
                <div class="brand-tab-panel col-lg-10 col-md-10 col-sm-11 col-xs-12">
                    <ul  class="nav nav-pills">
                        <li><a href="<?php echo  base_url(); ?>editorial/curator/<?php echo $details['curator_id']; ?>/about-curator">About</a></li>
                        <li class="active"><a  href="<?php echo  base_url(); ?>editorial/curator/<?php echo $details['curator_id']; ?>/editorial-tab">Editorial</a></li>
                    </ul>
                </div> <!--/category-tab-panel -->
            </div>


            <div id="myTabContent1" class="tab-content">
                
		
                <div class="editorial-block tab-pane fade <?php if($this->uri->segment(4, 0)=="editorial-tab"){ echo "active in"; } ?>"" id="editorial-tab">
                    <div class="col-lg-10 col-md-10 col-sm-11 col-xs-12 whats_new_content">
						<div class="grid" id="masonry-grid">
                            <?php
                                //  $sr = 0;
                                foreach ($editorial_detail as $details1) {
                                    ?>
                                    <div class="wall-column grid-item">
                                        <div class="wall-item">
                                            <div class="category cat_editorial">
                                                <div class="category_img" data-url="<?php echo base_url(); ?>editorial/artical/<?php echo $details1['editorial_id']; ?>">
                                                    <img src="<?php echo base_url();?><?php echo $details1['cproduct_image']; ?>" class="img-responsive">
						     <div class="editorial-icon"> <img src="<?php echo base_url(); ?>content/images/icons/editorial-icon-block.png" class="img-responsive"> </div>
                                                    <div class="hover-content-cat" data-url="<?php echo base_url(); ?>editorial/artical/<?php echo $details1['editorial_id']; ?>">
                                                        <div class="tab">
                                                            <div class="tab-in" id="<?php echo $curator_detail[0]['curator_id']; ?>">
                                                                <a href="javascript:void(0)" class="myan follow-brand-sectn" id="<?php echo $curator_detail[0]['curator_id']; ?>">
                                                                 <?php
															$me = [];
															foreach($user_follow as $user_foll){
																
																	$me[] = $user_foll['curator_id'];
															}
															$flag=1;
															for($i=0;$i<count($me);$i++){
															if($curator_detail[0]['curator_id'] ==$me[$i]){
																$flag=0;
																break;
															}
															}
															if($flag==0)
															{?>
                  <button class="follow-curator-btn1 following1cur" data-text="Follow curator"><i class="fa fa-check"></i></button>
     						 <?php }
							else { ?>
                             <button class="follow-curator-btn1 follow1cur" data-text="Follow curator"><i class="fa fa-check"></i></button>
     				<?php } ?>
                    </a>

                                                                                                                               
                                                           </div>  <!--/tab-in -->
                                                        </div> <!--/tab -->
                                                    </div> <!--/hover-content-cat -->
                                                </div>
                                                <div class="category_content curated-con">
                                                    <div class="cur-cat"><span><?php echo $details1['category_Name']; ?></span></div>
                                                    <a href="<?php echo base_url();?>Editorial/artical/<?php echo $details1['editorial_id']; ?>">
                                                        <h3><?php echo $details1['cproduct_Title']; ?></h3></a>
                                                    <h2><?php echo $details1['cproductSmallDiscription']; ?></h2>
                                                </div> <!--/category_content-->
                                            
                                                <div class="cur-infor">
                                    <a href="<?php echo  base_url(); ?>editorial/curator/<?php echo $curator_detail[0]['curator_id']; ?>/about-curator"><span class="pf-img" style="background:url(<?php echo base_url(); ?><?php echo $curator_detail[0]['curator_picture']; ?>);"> </span>
                                        <span class="cur-name"><?php echo $curator_detail[0]['curator_name']; ?></span></a> <a href="<?php echo base_url(); ?>editorial/artical/<?php echo $details1['editorial_id']; ?>"><span class="read-more">Read More </span></a>
                                </div>
                                                <div class="category_options">
                                                    <table>
                                                        <tbody><tr>
                                                            <td class="date-status cat_editorial">
                                                                <p><?php
                                                                        $now = time(); $startDate=$details1['editorial_datetime']; // or your date as well
                                                                        $your_date = strtotime($startDate);
                                                                        $datediff = $now - $your_date;
                                                                        echo floor(($datediff/(60*60*24))+1); ?> days ago</p>
                                                            </td>
                                           
                                             <?php
                                                            $me = [];
                                                            foreach($user_crown as $user_foll){
                                                                
                                                                    $me[] = $user_foll['cproduct_id'];
                                                            }
                                                            $flag=1;
                                                            for($i=0;$i<count($me);$i++){
                                                            if($details1['cproduct_id'] ==$me[$i]){
                                                                $flag=0;
                                                                break;
                                                            }
                                                            } 
                                                            if($flag==0)
                                                            {?>                                       
                                                   <td class="crown-sectn11 cproduct-crown active" id="<?php echo $details1['cproduct_id']; ?>"> <span id="cproductcrowncount"><?php foreach ($this->getdata->cproduct_count_crown($details1['cproduct_id']) as $count_no) {
                                echo $count_no['no'];
                            } ?></span> <span class="icomoon icon-crown"> </span></td>
                            <?php } 
                                                           else { ?> 
                                                           
                                                             <td class="crown-sectn11 cproduct-crown" id="<?php echo $details1['cproduct_id']; ?>"> <span id="cproductcrowncount"><?php foreach ($this->getdata->cproduct_count_crown($details1['cproduct_id']) as $count_no) {
                                echo $count_no['no'];
                            } ?></span> <span class="icomoon icon-crown"> </span></td>
                            <?php }  ?> 

                                                     <td class="comment-sectn">
                                            <a href="<?php echo base_url(); ?>editorial/artical/<?php echo $details1['editorial_id']; ?>"><span class="text"><?php foreach($this->getdata->count_comment_cproduct($details1['cproduct_id']) as $count_no){ echo $count_no['no'];  }?></span> <span class="icomoon icon-chat"> </span></a></td>
                                                            <td class="fwd-icon-sectn cat_editorial" id="fwd-id1"><span> </span> <span class="icomoon icon-sharing"></span></td>
                                                        </tr>
                                                        </tbody></table>

                                                    <div class="fwd-social-icons sec7" style="display:none;">
                                                        <ul class="cat-follow-icons">
                                                            <li>
                                                                <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo base_url(); ?>editorial/artical/<?php echo $details1['editorial_id']; ?>" class="fb"></a>
                                                            </li>
                                                            <li>
                                                                <a target="_blank" href="https://twitter.com/home?status=<?php echo base_url(); ?>editorial/artical/<?php echo $details1['editorial_id']; ?>" class="twitter"></a>
                                                            </li>
                                                            <li class="show-more"><a class="more" href="javascript:void(0)"></a>
                                                            </li>
                                                        </ul>
                                                        <div class="hidden-more-icons" style="display:none;">
                                                            <ul class="follow-icons-more">
                                                                <li>

                                                                    <a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo base_url(); ?>editorial/artical/<?php echo $details1['editorial_id']; ?>&title=Rigalio&summary=Rigalio%20content&source=<?php echo base_url(); ?>editorial/artical/<?php echo $details1['editorial_id']; ?>" class="linkdin"></a>
                                                                </li>
                                                                <li>
                                                                    <a target="_blank" href="https://plus.google.com/share?url=<?php echo base_url(); ?>editorial/artical/<?php echo $details1['editorial_id']; ?>" class="gplus"></a>

                                                                </li>
                                                                <li>
                                                                    <a target="_blank" href="https://pinterest.com/pin/create/button/?url=Testing&media=<?php echo base_url(); ?>editorial/artical/<?php echo $details1['editorial_id']; ?>&description=Testing" class="pini"></a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div> <!--/inner forward icons social -->
                                                </div> <!--/category_options-->

                                            </div> <!--/category-->
                                        </div> <!--/wall-item -->
                                    </div>  <!--/wall-column -->
                                    <?php
                                    
                                }
                              
                            ?>
                        </div>

                      

                    </div> <!--/whats_new_content -->
                </div> <!-- brand products tab content ends -->

            </div> <!--/tab-content -->
            <div class="curator-share-sectn col-lg-8 col-md-8 col-sm-11 col-xs-12">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
                    <div class="product-inner-social  curator-inner-social col-lg-6 col-md-5 col-sm-12 col-xs-12 nopadding">
                        <ul class="follow-icons">
                            <span>Share This </span>
                            <li><a class="facebook-follow" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo base_url();?>editorial/curator/<?php echo $curator_detail[0]['curator_id']; ?>/about-curator" target="_blank"></a></li>
                            <li><a class="twitter-follow" href="https://twitter.com/home?status=<?php echo base_url();?>editorial/curator/<?php echo $curator_detail[0]['curator_id']; ?>/about-curator" target="_blank"></a></li>
                            <?php /*?><li><a class="instagram-follow" href="" target="_blank"></a></li><?php */?>
                            <li><a class="linkedin-follow" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo base_url();?>editorial/curator/<?php echo $curator_detail[0]['curator_id']; ?>/about-curator&title=Rigalio&summary=Rigalio%20content&source=<?php echo base_url();?>editorial/curator/<?php echo $curator_detail[0]['curator_id']; ?>/about-curator" target="_blank"></a></li>
                            <li><a class="rss-follow" href="https://plus.google.com/share?url=<?php echo base_url();?>editorial/curator/<?php echo $curator_detail[0]['curator_id']; ?>/about-curator" target="_blank"></a></li>
                        </ul>
                        <?php } ?>
                    </div>
                </div>
            </div> <!-- /product-share-sectn -->


        </div>
    </div>
</div>
<!--barnd pg-con ends -->

<footer>
    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 footer-inner">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <p>All rights reserved. All content belongs to respective owners </p>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <ul>
                 <li><a href="<?php echo base_url();?>">Rigalio Team </a> </li>
                <li><a href="<?php echo base_url();?>main/contactus">Contact & Legal Notice </a> </li>
                <li><a href="<?php echo base_url();?>main/privacy">Privacy </a> </li>
            </ul>
        </div>
    </div>
</footer>
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->


</body>

<script type="text/javascript">
	$(document).ready(function (e) {
        $('#masonry-grid').masonry({
            itemSelector: '.grid-item',
            percentPosition: true
        });
		   });
	
	</script>
<script type="text/javascript">
    onsload();
</script>



</html>

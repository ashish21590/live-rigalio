<script src="<?php echo base_url(); ?>content/js/hover.js"></script>
<?php foreach($curator_detail as $details){?>
<div class="curator-banner col-lg-12 nopadding">
<span class="brand-bg-banner" style="background:url(<?php echo base_url(); ?><?php echo $details['banner_img'];?>);"> </span>

   
    <div class="col-lg-10 col-sm-11 col-xs-12 brand-divison nopadding">
        <div class="curatorpic">
            <span style="background:url(<?php echo base_url(); ?><?php echo $details['curator_picture'];?>) no-repeat top center;"> </span>
        </div>
    </div>
</div>


<div class="curator_tagline">
    <div class="col-lg-10 col-md-10 col-sm-11 col-xs-12 curator-logo-sec">
        <div class="col-lg-6 col-sm-12 nopadding">
            <span class="curator-intro"><?php echo $details['curator_name'];?></span>
        </div>
        <div class="col-lg-6 col-sm-12 nopadding inner-up">
            <span class="followers"><img src="<?php echo base_url();?>content/images/icons/followers-icon.png">
            <i id="getcount1"><?php foreach($this->Geteditorial->count_curatorfollower($details['curator_id']) as $count_no){
					if($count_no['no'] != ''){
					 echo $count_no['no']; 
					}
					else
					{
						echo '0';
					}
					 }?> </i>
             </span>
       <span class="curator-follow" id="<?php echo $details['curator_id'];?>">
        <?php
															$me = [];
															foreach($user_follow as $user_foll){
																
																	$me[] = $user_foll['curator_id'];
															}
															$flag=1;
															for($i=0;$i<count($me);$i++){
															if($details['curator_id'] ==$me[$i]){
																$flag=0;
																break;
															}
															}
															if($flag==0)
															{?>
                  <button class="follow-curator-btn1 following1cur" data-text="Follow curator"><i class="fa fa-check"></i></button>
     						 <?php }
							else { ?>
                             <button class="follow-curator-btn1 follow1cur" data-text="Follow curator"><i class="fa fa-check"></i></button>
     				<?php } ?>
      </span>
        </div>

    </div>
</div> <!--/brand_pg_tagline -->
<!--category-pg-con -->
<div class="category-pg-con editorial-pd brand-pg-con curated-abt-con col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
    <div class="container-fluid nomargin nopadding">
        <div class="row">
            <div class="upbrand-bg col-lg-12 nopadding">
                <div class="brand-tab-panel col-lg-10 col-md-10 col-sm-11 col-xs-12">
                    <ul  class="nav nav-pills">
                        <li class="active"><a href="<?php echo  base_url(); ?>editorial/curator/<?php echo $details['curator_id']; ?>/about-curator">About</a></li>
                        <li><a href="<?php echo  base_url(); ?>editorial/curator/<?php echo $details['curator_id']; ?>/editorial-tab">Editorial</a></li>
                    </ul>
                </div> <!--/category-tab-panel -->
            </div>


            <div id="myTabContent1" class="tab-content">
                <div class="about-cur-con tab-pane fade <?php if($this->uri->segment(4, 0)=="about-curator"){ echo "active in"; } ?>" id="about-curator">
                    <div class="col-lg-6 col-md-6 col-sm-8 col-xs-12 content_headline2">
                        <h2>About Curator</h2>
                        <hr>
                    </div>
                    <?php echo $details['curator_detail'];?>
                </div>

            
            </div> <!--/tab-content -->
            <div class="curator-share-sectn col-lg-8 col-md-8 col-sm-11 col-xs-12">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
                    <div class="product-inner-social  curator-inner-social col-lg-6 col-md-5 col-sm-12 col-xs-12 nopadding">
                        <ul class="follow-icons">
                            <span>Share This </span>
                            <li><a class="facebook-follow" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo base_url();?>editorial/curator/<?php echo $curator_detail[0]['curator_id']; ?>/about-curator" target="_blank"></a></li>
                            <li><a class="twitter-follow" href="https://twitter.com/home?status=<?php echo base_url();?>editorial/curator/<?php echo $curator_detail[0]['curator_id']; ?>/about-curator" target="_blank"></a></li>
                            <?php /*?><li><a class="instagram-follow" href="" target="_blank"></a></li><?php */?>
                            <li><a class="linkedin-follow" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo base_url();?>editorial/curator/<?php echo $curator_detail[0]['curator_id']; ?>/about-curator&title=Rigalio&summary=Rigalio%20content&source=<?php echo base_url();?>editorial/curator/<?php echo $curator_detail[0]['curator_id']; ?>/about-curator" target="_blank"></a></li>
                            <li><a class="rss-follow" href="https://plus.google.com/share?url=<?php echo base_url();?>editorial/curator/<?php echo $curator_detail[0]['curator_id']; ?>/about-curator" target="_blank"></a></li>
                        </ul>
                        <?php } ?>
                    </div>
                </div>
            </div> <!-- /product-share-sectn -->


        </div>
    </div>
</div>
<!--barnd pg-con ends -->

<footer>
    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 footer-inner">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <p>All rights reserved. All content belongs to respective owners </p>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <ul>
                 <li><a href="<?php echo base_url();?>">Rigalio Team </a> </li>
                <li><a href="<?php echo base_url();?>main/contactus">Contact & Legal Notice </a> </li>
                <li><a href="<?php echo base_url();?>main/privacy">Privacy </a> </li>
            </ul>
        </div>
    </div>
</footer>
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->


</body>

<script type="text/javascript">
	$(document).ready(function (e) {
        $('#masonry-grid').masonry({
            itemSelector: '.grid-item',
            percentPosition: true
        });
		   });
	
	</script>
<script type="text/javascript">
    onsload();
</script>



</html>

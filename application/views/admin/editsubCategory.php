<?php
foreach($get_category as $category_data)
{
}
?>
<script src="http://tinymce.cachefly.net/4.0/tinymce.min.js"></script>
<script type="text/javascript">
tinymce.init({
    selector: "textarea",
    plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
});
</script> 

<section id="main-content">
  <section class="wrapper">
    <h3><i class="fa fa-angle-right"></i> Form Components</h3>
    
    <!-- BASIC FORM ELELEMNTS -->
    <div class="row mt">
      <div class="col-lg-12">
         <div><!--<div class="form-panel">-->
          <h4 class="mb"><i class="fa fa-angle-right"></i>Update Sub Category</h4>
           <h2><?php echo validation_errors(); ?></h2>
          
          <?php /*?><?php
		  $id=$this->uri->segment();	
		   echo form_open_multipart('index.php/Slider/updateSlider/'".$id."'');?><?php */?>
           
           <?php echo form_open_multipart("index.php/subcategory/updatesubCategory")?>
          
           <input type="hidden" name="hide" value="<?php echo $this->uri->segment(3)?>"/>          
          
            <div class="form-group">
              <label class="col-sm-2 col-sm-2 control-label">Sub Category Name :</label>
              <div class="col-sm-10">
                <input type="text" name="subcatname" class="form-control" value="<?php echo $category_data['subCategory_Name'];?>">
              </div>
            </div>            
            
            <div class="form-group">
              <label class="col-sm-2 col-sm-2 control-label">Sub Category Title :</label>
              <div class="col-sm-10">
                <input type="text" name="subcattitle" class="form-control" value="<?php echo $category_data['title'];?>">
              </div>
            </div> 
            
            <div class="form-group">
              <label class="col-sm-2 col-sm-2 control-label">Sub Category Discription :</label>
              
              <div class="col-sm-10">
                <textarea name="subcatdisc" class="form-control" value=""><?php echo $category_data['description'];?> </textarea><br /><br />
              </div>
              
            </div>   
            
            <div class="form-group">
              <label class="col-sm-2 col-sm-2 control-label">Sub Category Keyword :</label>
              <div class="col-sm-10">
                <input type="text" name="subcatkeyword" class="form-control" value="<?php echo $category_data['keywords'];?>"><br /><br />
              </div>
            </div>    
            
            <div class="form-group">
              <label class="col-sm-2 col-sm-2 control-label">Category :</label>
              <div class="col-sm-10">                
                <select name="cat" class="form-control" > 
                 <option >-------Select Category-------</option>  
                 <?php
				// print_r($get_categotylist );
				foreach($rs as $categotylist_data){
					//$this->output->enable_profiler(TRUE);
					?>
			<option value="<?php echo $categotylist_data['categoryId'];?>" <?php if($categotylist_data['categoryId']==$category_data['categoryId_Fk']){ echo "selected";}?>><?php echo $categotylist_data['category_Name'];?></option>
				<?php }?>                   							
				</select> <br />         
              </div>
              </div>      
            
            <div class="form-group">           
              <label class="col-sm-2 col-sm-2 control-label" for="userfile">Upload New Sub Category Image :</label>               
              <div class="col-sm-10">
              <img src="<?php echo base_url().$category_data['subCategory_Image'];?>" width="15%" height="10%" /><br />
              <input type="hidden" name="up" value="<?php echo $category_data['subCategory_Image']; ?>"/>
                <input type="file" name="userfile" class="form-control" value="<?php echo $category_data['subCategory_Image']; ?>">
              </div>
            </div>                    
            <button name="submit" class="btn btn-theme" type="submit">Submit</button>  
          </form>
        </div>
      </div>
      <!-- col-lg-12--> 
    </div>
    <!-- /row --> 
    
  </section>
  <! --/wrapper --> 
</section>

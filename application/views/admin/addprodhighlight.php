<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/jquery-customselect.css"/>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/jquery-customselect.js"></script>
<script src="http://tinymce.cachefly.net/4.0/tinymce.min.js"></script>
<script type="text/javascript">
tinymce.init({
    selector: "textarea",
    plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
});
</script>
<!--sign up-pg con -->
<!-- (Optional) Latest compiled and minified JavaScript translation files -->
<script type="text/javascript">
   
$(function() {
$("#standard").customselect();
});
$(function() {
$("#productname").customselect();
});
    </script>
<section id="main-content" style="margin-left:0px">
  <section class="wrapper">
    <h3><i class="fa fa-angle-right"></i> Product Highlights</h3>
    <!-- BASIC FORM ELELEMNTS -->
    <div class="row mt">
      <div class="col-lg-12">
         <div><!--<div class="form-panel">-->
          <h4 class="mb"><i class="fa fa-angle-right"></i>Insert Product Highlights</h4>
           <h4 class="mb"><a href="<?php echo base_url(); ?>Admin/home">Back</a></h4>
          <h2><?php echo validation_errors(); ?></h2>         
        <?php
         echo form_open_multipart('Admin/insertHighlights/');?>
         <div class="form-group">
              <label class="col-sm-2 col-sm-2 control-label">Product Name :</label>
              <div class="col-sm-10">               
                 <select name="productname" class="form-control custom-select" id="productname" > 
                 <option>-------Select Product Name -------</option>                        
                <?php
        // print_r($get_categotylist );
        foreach($all_product as $productname){
        echo '<option value="'.$productname['productId'].'">'.$productname['product_Name'].'</option>';
        }
        ?>                                  
        </select> <br />   
              </div>
            </div>
     <div class="form-group">
              <label class="col-sm-2 col-sm-2 control-label">Product Highlights :</label>
              <div class="col-sm-10">
              <textarea name="highlight" id="highlight" class="form-control">
                           <div class="hightlight-inner-con col-lg-9 col-md-9 col-sm-10 col-xs-12">
                 <div class="highlight-sectn1 col-lg-6 col-md-6 col-sm-12 col-xs-12">
                     <div class="highlight-img col-lg-5 col-md-5 col-sm-6 col-xs-6 nopadding">
                        <span><img src="http://rigalio.com/uploads/highlight/FERRARI488-GTB-highlight-1.jpg" class="img-responsive"></span> <hr class="left">
                     </div> <!-- /highlight-img -->
                     <div class="hightlight-text col-lg-6 col-md-6 col-sm-6 col-xs-6 nopadding">
                         <h5></h5>
                         <p>The 488 GTB features very sculptural flanks which are the key to its character. Its large signature air intake scallop is a nod to the original 308 GTB and is divided into two sections by a splitter.</p>
                     </div> <!-- /hightlight-text -->
                 </div> <!-- /highlight-sectn1 -->
                 <div class="highlight-sectn1 col-lg-6 col-md-6 col-sm-12 col-xs-12">
                     <div class="hightlight-text col-lg-6 col-md-6 col-sm-6 col-xs-6 nopadding">
                         <h5></h5>
                         <p>Improved algorithms in the manettino-operated stability control system. Even the styling is better, by virtue of being more aggressive. </p>
                     </div> <!-- /hightlight-text -->
                     <div class="highlight-img col-lg-5 col-md-5 col-sm-6 col-xs-6 nopadding">
                        <span><img src="http://rigalio.com/uploads/highlight/FERRARI488-GTB-highlight-2.jpg" class="img-responsive pull-right"> </span> <hr class="right">
                     </div> <!-- /highlight-img -->
                 </div> <!-- /highlight-sectn1 -->
             </div> <!--/ hightlight-inner-con -->
             <div class="hightlight-inner-con col-lg-8 col-md-8 col-sm-10 col-xs-12">
                 <div class="highlight-sectn2 col-lg-6 col-md-6 col-sm-12 col-xs-12">
                     <div class="hightlight-text col-lg-6 col-md-6 col-sm-6 col-xs-6 nopadding">
                         <h5> </h5>
                         <p>Perhaps even more significantly, 560lb ft of torque, all available from under 3,000rpm. It’ll officially do 0–62mph in 3.0secs, and hit 124mph barely five seconds later. </p>
                     </div> <!-- /hightlight-text -->
                     <div class="highlight-img col-lg-6 col-md-6 col-sm-6 col-xs-6 nopadding">
                        <span><img src="http://rigalio.com/uploads/highlight/FERRARI488-GTB-highlight-3.jpg" class="img-responsive pull-right"></span> <hr class="left">
                     </div> <!-- /highlight-img -->
                 </div> <!-- /highlight-sectn1 -->
                 <div class="highlight-sectn2 col-lg-6 col-md-6 col-sm-12 col-xs-12">
                     <div class="highlight-img col-lg-6 col-md-6 col-sm-6 col-xs-6 nopadding">
                        <span><img src="http://rigalio.com/uploads/highlight/FERRARI488-GTB-highlight-4.jpg" class="img-responsive"></span> <hr class="right">
                     </div> <!-- /highlight-img -->
                     <div class="hightlight-text col-lg-6 col-md-6 col-sm-6 col-xs-6 nopadding">
                         <h5></h5>
                         <p>The software carried forward from the hugely successful 458’s seven speed dual-clutch box has been revised for faster shifts, adding to a surge in relentless torque figures. </p>
                     </div> <!-- /hightlight-text -->
                     
                 </div> <!-- /highlight-sectn1 -->
             </div> <!--/ hightlight-inner-con -->
              
               </textarea><br /><br />
              </div>
            </div>
            <button name="submit" class="btn btn-theme" type="submit">Submit</button>
            </form>
        </div>
      </div>
      <!-- col-lg-12--> 
    
    </div>
    <!-- /row --> 
  </section>
  <! --/wrapper --> 
</section>
<!-- (Optional) Latest compiled and minified JavaScript translation files -->
<script type="text/javascript">
    $(function() {
    $( "#skills" ).autocomplete({
        source: '<?php echo base_url(); ?>main/getcity',
    minLength: 2
    });
});
    </script>

<script type="text/javascript">

function subcategory(id){
  	//alert(id);

	var data = {
					"categoryid": id
				   
					};
				$.ajax({
			url: 'getsubcategory',
			 type: "post",
			data: data,
			success: function(data) {
				//alert(data);
				$("#mysub_cat").html(data);
		 //alert0(data);
		}
		})
  }

  </script>

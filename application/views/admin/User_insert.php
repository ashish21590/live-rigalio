<section id="main-content">
  <section class="wrapper">
    <h3><i class="fa fa-angle-right"></i> Form Components</h3>
    
    <!-- BASIC FORM ELELEMNTS -->
    <div class="row mt">
      <div class="col-lg-12">
        <div class="form-panel">
          <h4 class="mb"><i class="fa fa-angle-right"></i> Form Elements</h4>
          <form class="form-horizontal style-form" method="post">
            <div class="form-group">
              <label class="col-sm-2 col-sm-2 control-label">Name</label>
              <div class="col-sm-10">
                <input type="text" name="txtName" class="form-control">
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 col-sm-2 control-label">Email</label>
              <div class="col-sm-10">
                <input type="text" name="txtEmail" class="form-control">
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 col-sm-2 control-label">Phone No</label>
              <div class="col-sm-10">
                <input type="text" name="txtPhone" class="form-control">
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 col-sm-2 control-label">Adress</label>
              <div class="col-sm-10">
                <input type="text" name="txtAdress" class="form-control">
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 col-sm-2 control-label">City</label>
              <div class="col-sm-10">
                <input type="text" name="txtCity" class="form-control">
              </div>
            </div>
            <button name="submit" class="btn btn-theme" type="submit">Submit</button>
          </form>
        </div>
      </div>
      <!-- col-lg-12--> 
    </div>
    <!-- /row --> 
    
  </section>
  <! --/wrapper --> 
</section>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
 
	<head>
		
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		
		<title><?php  //echo $site_name;?> | Admin</title>
		
		<!--                       CSS                       -->
	  
		<!-- Reset Stylesheet -->
		<link rel="stylesheet" href="resources/css/reset.css" type="text/css" media="screen" />
	  
		<!-- Main Stylesheet -->
		<link rel="stylesheet" href="resources/css/style.css" type="text/css" media="screen" />
		
		<!-- Invalid Stylesheet. This makes stuff look pretty. Remove it if you want the CSS completely valid -->
		<link rel="stylesheet" href="resources/css/invalid.css" type="text/css" media="screen" />	
		
		<!-- jQuery -->
		<script type="text/javascript" src="resources/scripts/jquery-1.3.2.min.js"></script>
		
		<!-- jQuery Configuration -->
		<script type="text/javascript" src="resources/scripts/simpla.jquery.configuration.js"></script>
		
		<!-- Facebox jQuery Plugin -->
		<script type="text/javascript" src="resources/scripts/facebox.js"></script>
		
		<!-- jQuery WYSIWYG Plugin -->
		<script type="text/javascript" src="resources/scripts/jquery.wysiwyg.js"></script>
<script>
function validate()
{
	if(document.form1.username.value=="")
	{
		alert('Please Enter Username');
		document.form1.username.focus();
		return false;
	}
	if(document.form1.password.value=="")
	{
		alert('Please Enter Password');
		document.form1.password.focus();
		return false;
	}
return true;
}
</script>			
		<!-- Internet Explorer .png-fix -->
		
		<!--[if IE 6]>
			<script type="text/javascript" src="resources/scripts/DD_belatedPNG_0.0.7a.js"></script>
			<script type="text/javascript">
				DD_belatedPNG.fix('.png_bg, img, li');
			</script>
		<![endif]-->
		
	</head>
  
	<body id="login">
		
		<div id="login-wrapper" class="png_bg">
			<div id="login-top">
			
				<h1>Simpla Admin</h1>
				<!-- Logo (221px width) -->
				<img id="logo" src="resources/images/logo.png" alt="Simpla Admin logo" />
			</div> <!-- End #logn-top -->
			
			<div id="login-content">
				
			<form action="" method="POST" name="form1" onSubmit="return validate(this.form);">
				<?php if($msg){?>
					<div class="notification information png_bg">
						<div>
							<?php if($msg){echo $msg;}else{echo "Please fill username and password for login.";} ?>
						</div>
					</div>
					<?php }?>
					<p><label>Username</label>
						<input name="username" class="text-input" type="text" value="" />
					</p>
					<div class="clear"></div>
					<p><label>Password</label>
						<input name="password" class="text-input" type="password" value="" />
					</p>
					<div class="clear"></div>
					<p id="remember-password"><a href="" class="forgot-pwd"></a></p>
					<div class="clear"></div>
					<p><input class="button" name="submit" type="submit" value="Sign In" /></p>
					
				</form>
			</div> <!-- End #login-content -->
			
		</div> <!-- End #login-wrapper -->
		
  </body>
  
</html>
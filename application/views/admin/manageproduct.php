<section id="main-content">
  <section class="wrapper">
    <h3><i class="fa fa-angle-right"></i>Manage Product</h3>
    <div class="row mt">
      <div class="col-lg-12">
        <div class="content-panel">
        <form name="search product" action="search_product" method="post">
        <input type="text" name="product_name" value="">
        <input type="submit" name="submit" value="submit">
        </form>
          <!--<h4><i class="fa fa-angle-right"></i> Responsive Table</h4>-->
          <section id="unseen">
              <table class="table table-bordered table-striped table-condensed">
              <thead>
                <tr>
                  <th>S. No.</th>
                  <th>Product Name</th>
                    <th>Add Specifications</th>
                  <th>Product Category</th>
                
                  <th>Product Brand</th>
                  <th>Product Title</th>
                  <th>Product Image</th>
                  <th>Product Price</th>
                  <th>Model No.</th>
                  <th>Product Small Discription</th>
                  <th>Product Long Discription</th>
               

                  <th>Manage</th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody>
              <?php
			  $sno = 1;
			  foreach($get_categotylist as $categotylist_data){
				  //$this->output->enable_profiler(TRUE);
				  //$this->output->enable_profiler(TRUE);
			  ?>
              
                <tr>
                  <td><?php echo $sno;?></td>
                  <td><?php echo $categotylist_data['product_Name'];?></td>
                  <td> <a href="<?php echo base_url(); ?>Admin/specification/<?php echo $categotylist_data['productId']; ?>">Add</a></td>
                  <td><?php echo $categotylist_data['category_Name'];?></td>
                  <td><?php echo $categotylist_data['brand_Name'];?></td>
                  <td><?php echo $categotylist_data['product_Title'];?></td>
                  <td>
             <a href="<?php echo base_url();?>Admin/listProductimage/<?php echo $categotylist_data['productId']; ?>">
                  <img src="<?php echo base_url().$categotylist_data['image_Name'];?>"/></a></td>
                   <td><?php echo $categotylist_data['Price'];?></td>
                    <td><?php echo $categotylist_data['model_no'];?></td>
                   <td><?php echo $categotylist_data['productSmallDiscription'];?></td>
                   <td><?php echo $categotylist_data['productlongDiscription'];?></td>
                  
                  <td><a href="<?php echo base_url();?>Admin/editProduct/<?php echo $categotylist_data['productId']; ?>"><img src="<?php echo base_url();?>assets/img/icons/pencil_48.png" width="18"></a>&nbsp;|&nbsp;<a onClick="confirm('Are you sure!');" href="<?php echo base_url();?>Admin/deleteProduct/<?php echo $categotylist_data['productId']; ?>"><img src="<?php echo base_url();?>assets/img/icons/cross.png"</a></td>
                  
                  <td><a href="<?php echo base_url();?>Admin/isActiveProduct/<?php echo $categotylist_data['productId']; ?>/<?php echo $categotylist_data['isActive']; ?>"><?php if($categotylist_data['isActive']==1){?><img src="<?php echo base_url();?>assets/img/icons/active.png" width="18"><?php }else{?><img src="<?php echo base_url();?>assets/img/icons/deactivate.png" width="18"><?php }?></a></td>
                </tr>
              <?php $sno++;
			  }?>


              </tbody>
            </table>
          </section>
          <p><?php echo $links; ?></p>
        </div>
      </div>
    </div>
  </section>
</section>


<script src="http://tinymce.cachefly.net/4.0/tinymce.min.js"></script>
<script type="text/javascript">
tinymce.init({
    selector: "textarea",
    plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
});
</script> 


<section id="main-content">
  <section class="wrapper">
    <h3><i class="fa fa-angle-right"></i> Product Component</h3>
    <!-- BASIC FORM ELELEMNTS -->
    <div class="row mt">
      <div class="col-lg-12">
         <div><!--<div class="form-panel">-->
          <h4 class="mb"><i class="fa fa-angle-right"></i>Insert Product</h4>
          <h2><?php echo validation_errors(); ?></h2>         
          
        <?php echo form_open_multipart('Admin/insertproduct');?>
         
<?php /*?> <?php echo form_open('index.php/Slider/insertSlider');?><?php */?>
             
              <div class="form-group">
              <label class="col-sm-2 col-sm-2 control-label">Product Name :</label>
              <div class="col-sm-10">
                <input type="text" name="productname" class="form-control"><br /><br />
              </div>
            </div>
        <!--    <div class="form-group">
              <label class="col-sm-2 col-sm-2 control-label">Product Price :</label>
              <div class="col-sm-10">
                <input type="text" name="productprice" class="form-control"><br /><br />
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 col-sm-2 control-label">Product Model :</label>
              <div class="col-sm-10">
                <input type="text" name="productmodel" class="form-control"><br /><br />
              </div>
            </div>-->
             <div class="form-group">
              <label class="col-sm-2 col-sm-2 control-label">Product Title :</label>
              <div class="col-sm-10">
                <input type="text" name="producttitle" class="form-control"><br /><br />
              </div>
            </div>   
            <div class="form-group">
              <label class="col-sm-2 col-sm-2 control-label">Product Small Discription :</label>
              <div class="col-sm-10">
                <textarea name="productsmalldisc" class="form-control"> </textarea><br /><br />
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 col-sm-2 control-label">Product Overview content 1 :</label>
              <div class="col-sm-10">
              <textarea name="productlongdisc" class="form-control"> </textarea><br /><br />
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 col-sm-2 control-label">Product Overview content 2 :</label>
              <div class="col-sm-10">
              <textarea name="productoverview" class="form-control"> </textarea><br /><br />
              </div>
            </div>   
            <div class="form-group">
              <label class="col-sm-2 col-sm-2 control-label">Gender :</label>
              <div class="col-sm-10">                
                <select name="gender" class="form-control">
                <option value="Select">-------Select-------</option>                
				<option value="0">MEN</option>
				<option value="1">WOMEN</option>				
				</select> <br />         
              </div>
              <div class="form-group">
              <label class="col-sm-2 col-sm-2 control-label">Product Category :</label>
              <div class="col-sm-10">               
                 <select name="productcat" class="form-control" id="productcat" onChange="subcategory(this.value);" > 
                 <option>-------Select Category-------</option>                         
                 <?php
				// print_r($get_categotylist );
				foreach($rs as $categotylist_data){
			echo '<option value="'.$categotylist_data['categoryId'].'">'.$categotylist_data['category_Name'].'</option>';
				}
				?>                     							
				</select> <br />   
              </div>
            </div>    
            
            <div class="form-group">
              <label class="col-sm-2 col-sm-2 control-label">Product Sub Category :</label>
              <div class="col-sm-10">               
                 <select name="productsubcat" class="form-control" id="mysub_cat"> 
                 <option>-------Select Sub Category-------</option>                         
                 <?php /*?><?php
				// print_r($get_categotylist );
				foreach($subcategory as $subcategotylist_data){
			echo '<option value="'.$subcategotylist_data['subCategoryId'].'">'.$subcategotylist_data['subCategory_Name'].'</option>';
				}
				?><?php */?>                     							
				</select> <br />   
              </div>
            </div>    
             
            <div class="form-group">
              <label class="col-sm-2 col-sm-2 control-label">Product Brand :</label>
              <div class="col-sm-10">               
                 <select name="productbrand" class="form-control" > 
                 <option>-------Select Brand-------</option>                        
                <?php
				// print_r($get_categotylist );
				foreach($result as $brandlist_data){
				echo '<option value="'.$brandlist_data['brandId'].'">'.$brandlist_data['brand_Name'].'</option>';
				}
				?>                     							
				</select> <br />   
              </div>
            </div>  


           <div class="form-group">
                   <label class="col-sm-2 col-sm-2 control-label" for="userfile">Product Image :</label>
              <div class="col-sm-10">
              	<?php /*?><?php echo form_open_multipart('index.php/Slider/insertSlider');?><?php */?>
                <input type="file" name="userfile[]" multiple  class="form-control"><br /><br />
              </div>
            </div>
           <!-- <div class="form-group">
                   <label class="col-sm-2 col-sm-2 control-label" for="userfile">Product Slider Thumbnail Image :</label>
              <div class="col-sm-10">
                <?php /*?><?php echo form_open_multipart('index.php/Slider/insertSlider');?><?php */?>
                <input type="file" name="thumb_image[]" multiple  class="form-control"><br /><br />
              </div>
            </div>


            <div class="form-group">
                   <label class="col-sm-2 col-sm-2 control-label" for="userfile">Product Block Image :</label>
              <div class="col-sm-10">
                <?php /*?><?php echo form_open_multipart('index.php/Slider/insertSlider');?><?php */?>
                <input type="file" name="block_image[]" multiple  class="form-control"><br /><br />
              </div>
            </div>
-->
           <button name="submit" class="btn btn-theme" type="submit">Submit</button>
            </form>
        </div>
      </div>
      <!-- col-lg-12--> 
    </div>
    <!-- /row --> 
  </section>
  <! --/wrapper --> 
</section>

 <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js" type="text/javascript"></script>
 <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.6/jquery-ui.min.js" type="text/javascript"></script>
<script type="text/javascript">

function subcategory(id){
  	//alert(id);

	var data = {
                            "categoryid": id
                           
                            };
                        $.ajax({
                    url: 'getsubcategory',
                     type: "post",
                    data: data,
                    success: function(data) {
                    	//alert(data);
                    	$("#mysub_cat").html(data);
                 //alert0(data);
                }
                })

  }
  </script>

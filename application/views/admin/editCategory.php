<?php
foreach($get_category as $category_data)
{
}
?>
<script src="http://tinymce.cachefly.net/4.0/tinymce.min.js"></script>
<script type="text/javascript">
tinymce.init({
    selector: "textarea",
    plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
});
</script> 

<section id="main-content">
  <section class="wrapper">
    <h3><i class="fa fa-angle-right"></i> Form Components</h3>
    
    <!-- BASIC FORM ELELEMNTS -->
    <div class="row mt">
      <div class="col-lg-12">
         <div><!--<div class="form-panel">-->
          <h4 class="mb"><i class="fa fa-angle-right"></i>Insert Product</h4>
           <h2><?php echo validation_errors(); ?></h2>
          
          <?php /*?><?php
		  $id=$this->uri->segment();	
		   echo form_open_multipart('index.php/Slider/updateSlider/'".$id."'');?><?php */?>
           
           <?php echo form_open_multipart("index.php/category/updateCategory")?>
          
           <input type="hidden" name="hide" value="<?php echo $this->uri->segment(3)?>"/>          
          
            <div class="form-group">
              <label class="col-sm-2 col-sm-2 control-label">Category Name :</label>
              <div class="col-sm-10">
                <input type="text" name="catname" class="form-control" value="<?php echo $category_data['category_Name'];?>">
              </div>
            </div>            
            
            <div class="form-group">
              <label class="col-sm-2 col-sm-2 control-label">Category Title :</label>
              <div class="col-sm-10">
                <input type="text" name="cattitle" class="form-control" value="<?php echo $category_data['title'];?>">
              </div>
            </div> 
            
            <div class="form-group">
              <label class="col-sm-2 col-sm-2 control-label">Category Discription :</label>
              <div class="col-sm-10">
                <textarea name="catdisc" class="form-control"><?php echo $category_data['Description'];?> </textarea><br /><br />
              </div>
            </div>   
            
            <div class="form-group">
              <label class="col-sm-2 col-sm-2 control-label">Category Keyword :</label>
              <div class="col-sm-10">
                <input type="text" name="catkeyword" class="form-control" value="<?php echo $category_data['Keywords'];?>"><br /><br />
              </div>
            </div>       
            
            <div class="form-group">           
              <label class="col-sm-2 col-sm-2 control-label" for="userfile">Upload New Category Image :</label>               
              <div class="col-sm-10">
              <img src="<?php echo base_url().$category_data['category_image'];?>" width="15%" height="10%" /><br />
              <input type="hidden" name="up" value="<?php echo $category_data['category_image']; ?>"/>
                <input type="file" name="userfile" class="form-control" value="<?php echo $category_data['category_image']; ?>">
              </div>
            </div>                    
            <button name="submit" class="btn btn-theme" type="submit">Submit</button>  
          </form>
        </div>
      </div>
      <!-- col-lg-12--> 
    </div>
    <!-- /row --> 
    
  </section>
  <! --/wrapper --> 
</section>

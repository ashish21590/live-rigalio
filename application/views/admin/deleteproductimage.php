<?php
  foreach($get_productimagelist as $productimagelist_data)
  {
  }
 ?>
<section id="main-content">
  <section class="wrapper">
    <h3 align="center"><?php echo $productimagelist_data['product_Name']; ?></h3>
    <div class="row mt">
      <div class="col-lg-12">
        <div class="content-panel">
          <!--<h4><i class="fa fa-angle-right"></i> Responsive Table</h4>-->
          <section id="unseen">
              <table class="table table-bordered table-striped table-condensed">
              <thead>
                <tr>
                  <th>S. No.</th>
                  <th>Product Image</th>
                  <th>Manage</th>
                  <th>Delete</th>
                </tr>
              </thead>
              <tbody>
              <?php
			  $sno = 1;
			  foreach($get_productimagelist as $productimagelist_data){
				  //$this->output->enable_profiler(TRUE);
				  //$this->output->enable_profiler(TRUE);
			  ?>
                <tr>
                  <td><?php echo $sno;?></td>
                  <td><img src="<?php echo base_url().$productimagelist_data['image_Name'];?>"/></td>
                                   
                  <td><a href="<?php echo base_url();?>index.php/product/editProductimage/<?php echo $productimagelist_data['image_id']; ?>"><img src="<?php echo base_url();?>assets/img/icons/pencil_48.png" width="18"></a></td>
                  
                  <td><a onClick="confirm('Are you sure!');" href="<?php echo base_url();?>index.php/product/deleteProductimage/<?php echo $productimagelist_data['image_id']; ?>"><img src="<?php echo base_url();?>assets/img/icons/cross.png"</a></td>
                  
                </tr>
              <?php $sno++;
			  }?>
              </tbody>
            </table>
          </section>
        </div>
      </div>
    </div>
  </section>
</section>

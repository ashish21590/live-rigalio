<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Insert News</title>
</head>

<body>
<div id="insert">
<h1>Insert News</h1>
<?php echo form_open_multipart('brand/insertbrand');?>
<!--<form method="post" action="index.php/brand/insertbrand" enctype="multipart/form-data">-->
<label for="news_img">News Block Image</label>
<input type='file' name='userfile' id='userfile' size="30" />
</br>
<label for="news_banner_img">News Banner Image</label>
<input type='file' name='userfile1' id='userfile1' size="30"/>
</br>
<label for="type">News Type</label>
<input type="text" name="news_type" placeholder="Launches/Trending/News" value=""/>
</br>
<label for="buttn_name">Button Name</label>
<input type="text" name="button_name" placeholder="Read More" value=""/>
</br>
<label for="link">News Link</label>
<input type="text" name="news_link" value=""/>
</br>
<label for="headline">Headline</label>
<textarea name="headline" rows="4" cols="50" value=""></textarea>
</br>
<label for="small_desc">Small Description</label>
<textarea name="small" rows="4" cols="50" value=""></textarea>
</br>
<label for="long_desc">Long Description</label>
<textarea name="long" rows="4" cols="50" value=""></textarea>
</br>
<label for="brand">Brand name</label>
<select name="brand_name">
<option value="" disabled="disabled" selected="selected">Please select a name</option>
<?php 
foreach ($brand_name as $brand_name) {  
?>
    <option value="<?php echo $brand_name['brand_Name'];?>"><?php echo $brand_name['brand_Name'];?></option>
    <?php } ?>
</select>
</br>
<input type="submit" name="Submit" value="Insert"/>
<?php echo "</form>"?>
<a href="<?php base_url();?>brand/shownews/1"><input type="submit"name="Submit" value="show"/></a>
</div>
</body>
</html>
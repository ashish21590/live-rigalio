<aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">
              
              	  <p class="centered"><a href="javascript:void(0)"><img src="<?php echo base_url();?>assets/img/ui-sam.jpg" class="img-circle" width="60"></a></p>
              	  <h5 class="centered">Welcome Admin</h5>
              	  	
                  <li class="mt">
                      <a href="#Dashboard.php">
                          <i class="fa fa-dashboard"></i>
                          <span>Dashboard</span>
                      </a>
                  </li>
                  
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-desktop"></i>
                          <span>Slider</span>
                      </a>
                      <ul class="sub">
                          <li><a href="<?php echo base_url();?>index.php/Slider/insertSlider">Add Slider</a></li>
                          <li><a href="<?php echo base_url();?>index.php/Slider/manageSlider">Manage Slider</a></li>
                      </ul>
                  </li>
                  

                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-desktop"></i>
                          <span>Categorys</span>
                      </a>
                      <ul class="sub">
                          <li><a href="<?php echo base_url();?>Admin/insertcategory">Add Category</a></li>
                          <li><a href="<?php echo base_url();?>Admin/manageCategory">Manage Categorys</a></li>
                      </ul>
                  </li>
                  
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-desktop"></i>
                          <span>Sub Categorys</span>
                      </a>
                      <ul class="sub">
                          <li><a href="<?php echo base_url();?>index.php/subcategory/insertsubcategory">Add Subcategory</a></li>
                          <li><a href="<?php echo base_url();?>index.php/subcategory/managesubCategory">Manage Subcategorys</a></li>
                      </ul>
                  </li>
                  
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-desktop"></i>
                          <span>Brands</span>
                      </a>
                      <ul class="sub">
                          <li><a href="<?php echo base_url();?>Admin/insertbrand">Add Brands</a></li>
                          <li><a href="<?php echo base_url();?>Admin/managebrand">Manage Brands</a></li>
                      </ul>
                  </li>
                  
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-desktop"></i>
                          <span>Brand News</span>
                      </a>
                      <ul class="sub">
                          <li><a href="<?php echo base_url();?>Admin/insertbrandnews">Add Brand News</a></li>
                          <li><a href="<?php echo base_url();?>Admin/managebrandnews">Manage Brand News</a></li>
                      </ul>
                  </li>
                                   
                   

                  <!--<li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-cogs"></i>
                          <span>Components</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="calendar.html">Calendar</a></li>
                          <li><a  href="gallery.html">Gallery</a></li>
                          <li><a  href="todo_list.html">Todo List</a></li>
                      </ul>
                  </li>-->
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-book"></i>
                          <span>Products</span>
                      </a>
                      <ul class="sub">
                          <li><a href="<?php echo base_url();?>Admin/insertProduct">Add Product</a></li>
                          <li><a href="<?php echo base_url();?>Admin/manageProduct">Manage Products</a></li>
                      </ul>
                  </li>
                  
                   <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-book"></i>
                          <span>Products Highlights</span>
                      </a>
                      <ul class="sub">
                          <li><a href="<?php echo base_url();?>Admin/insertProductHighlights">Add Product Highlights</a></li>
                          <li><a href="<?php echo base_url();?>Admin/manageProductHighlights">Manage Products Highlights</a></li>
                      </ul>
                  </li>
                  
                   <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-book"></i>
                          <span>Products Slider Image</span>
                      </a>
                      <ul class="sub">
                          <li><a href="<?php echo base_url();?>Admin/insertProductslider">Add Slider Image</a></li>
                          <li><a href="<?php echo base_url();?>Admin/manageProductslider">Manage Slider Image</a></li>
                      </ul>
                  </li>
                  
                   <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-book"></i>
                          <span>Specification</span>
                      </a>
                      <ul class="sub">
    <?php /*?> <li><a href="<?php echo base_url();?>index.php/specification/addspecificationtype">Add Specification Type</a></li><?php */?>
    <li><a href="<?php echo base_url();?>Admin/managespecificationtype">Manage Specification Type</a></li>
                     
    <?php /*?> <li><a href="<?php echo base_url();?>index.php/specification/addspecificationname">Add Specification Name</a></li><?php */?>
    <li><a href="<?php echo base_url();?>Admin/managespecificationname">Manage Specification Name</a></li>
   <?php /*?>                  
    <li><a href="<?php echo base_url();?>index.php/specification/addspecification">Add Specification</a></li><?php */?>
    <li><a href="<?php echo base_url();?>Admin/managespecification">Manage Specification</a></li>
                      </ul>
                  </li>
                  
                   <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-book"></i>
                          <span>Feature</span>
                      </a>
                      <ul class="sub">
                          <li><a href="<?php echo base_url();?>index.php/product/insertProduct">Add Feature</a></li>
                          <li><a href="<?php echo base_url();?>index.php/product/manageProduct">Manage Feature</a></li>
                      </ul>
                  </li>
                  
                <!--  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-tasks"></i>
                          <span>Forms</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="form_component.html">Form Components</a></li>
                      </ul>
                  </li>
                  <li class="sub-menu">
                      <a class="active" href="javascript:;" >
                          <i class="fa fa-th"></i>
                          <span>Data Tables</span>
                      </a>
                      <ul class="sub">
                          <li class="active"><a  href="basic_table.html">Basic Table</a></li>
                          <li><a  href="responsive_table.html">Responsive Table</a></li>
                      </ul>
                  </li>
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class=" fa fa-bar-chart-o"></i>
                          <span>Charts</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="morris.html">Morris</a></li>
                          <li><a  href="chartjs.html">Chartjs</a></li>
                      </ul>
                  </li>-->

              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      
             
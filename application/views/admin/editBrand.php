<?php
foreach($get_category as $category_data)
{
}
?>
<script src="http://tinymce.cachefly.net/4.0/tinymce.min.js"></script>
<script type="text/javascript">
tinymce.init({
    selector: "textarea",
    plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
});
</script> 

<section id="main-content">
  <section class="wrapper">
    <h3><i class="fa fa-angle-right"></i> Edit Brand</h3>
    
    <!-- BASIC FORM ELELEMNTS -->
    <div class="row mt">
      <div class="col-lg-12">
         <div><!--<div class="form-panel">-->
          <h4 class="mb"><i class="fa fa-angle-right"></i>Edit Brand</h4>
           <h2><?php echo validation_errors(); ?></h2>
          
          <?php /*?><?php
		  $id=$this->uri->segment();	
		   echo form_open_multipart('index.php/Slider/updateSlider/'".$id."'');?><?php */?>
           
           <?php echo form_open_multipart("Admin/updatebrand"); ?>
          
           <input type="hidden" name="hide" value="<?php echo $this->uri->segment(3)?>"/> 
          
             <div class="form-group">
              <label class="col-sm-2 col-sm-2 control-label">Brand Name :</label>
              <div class="col-sm-10">
                <input type="text" name="brandname" class="form-control" value="<?php echo $category_data['brand_Name'];?>">
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 col-sm-2 control-label">Established year :</label>
              <div class="col-sm-10">
                <input type="text" name="brandyear" class="form-control" value="<?php echo $category_data['Establish'];?>">
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 col-sm-2 control-label">Brand Discription :</label>
              <div class="col-sm-10">
                <input type="text" name="branddisc" class="form-control" value="<?php echo $category_data['brandDescription'];?>">
              </div>
              
            </div>
            <div class="form-group">
              <label class="col-sm-2 col-sm-2 control-label">Brand Small Discription :</label>
              <div class="col-sm-10">
                <textarea name="brandsmalldisc" class="form-control"><?php echo $category_data['brandSmallDescription'];?> </textarea><br /><br />
              </div>
            </div>
         


  <div class="form-group">
              <label class="col-sm-2 col-sm-2 control-label">Brand Legacy :</label>
              <div class="col-sm-10">
     <textarea name="brand_legacy"  class="form-control"><?php echo $category_data['legacy'];?> </textarea> <br /><br />
              </div>
            </div>   


            <div class="form-group">
              <label class="col-sm-2 col-sm-2 control-label">Brand Title :</label>
              <div class="col-sm-10">
              <input type="text" name="brandtitle" class="form-control" value="<?php echo $category_data['title'];?>">
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-2 col-sm-2 control-label">Official Link :</label>
              <div class="col-sm-10">
                <input type="text" name="official_link" class="form-control" value="<?php echo $category_data['official_link'];?>"><br /><br />
              </div>
            </div>   


 <div class="form-group">
              <label class="col-sm-2 col-sm-2 control-label">Twitter Username :</label>
              <div class="col-sm-10">
                <input type="text" name="twitter_username" class="form-control" value="<?php echo $category_data['official_link'];?>"><br /><br />
              </div>
            </div>   


            <div class="form-group">
              <label class="col-sm-2 col-sm-2 control-label">Facebook Username :</label>
              <div class="col-sm-10">
                <input type="text" name="facebook_username" class="form-control"><br /><br />
              </div>
            </div>   


              
            <!--<div class="form-group">
              <label class="col-sm-2 col-sm-2 control-label">Gender :</label>
              <div class="col-sm-10">                
                <select name="gender" class="form-control">
                <option>-------Select-------</option>                
				<option value="0">MEN</option>
				<option value="1">WOMEN</option>				
				</select> <br />         
              </div>
              <div class="form-group">
              <label class="col-sm-2 col-sm-2 control-label">Product Category :</label>
              <div class="col-sm-10">               
                 <select name="productcat" class="form-control">                         
                <?php /*?> <?php
				// print_r($get_categotylist );
				foreach($rs as $categotylist_data){
				echo '<option value="'.$categotylist_data['id'].'">'.$categotylist_data['category'].'</option>';
				}
				?> <?php */?>                    							
				</select> <br />   
              </div>
            </div>         --> 
            
            <div class="form-group">           
              <label class="col-sm-2 col-sm-2 control-label" for="userfile">Upload New Image :</label>               
              <div class="col-sm-10">
              <img src="<?php echo base_url().$category_data['brand_image'];?>" width="15%" height="10%" /><br />
              <input type="hidden" name="up" value="<?php echo $category_data['brand_image']; ?>"/>
                <input type="file" name="userfile"  class="form-control" value="<?php echo $category_data['brand_image']; ?>">
              </div>
            </div>                    
            <button name="submit" class="btn btn-theme" type="submit">Submit</button>  
          </form>
        </div>
      </div>
      <!-- col-lg-12--> 
    </div>
    <!-- /row --> 
    
  </section>
  <! --/wrapper --> 
</section>

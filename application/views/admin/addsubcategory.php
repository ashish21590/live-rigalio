<script src="http://tinymce.cachefly.net/4.0/tinymce.min.js"></script>
<script type="text/javascript">
tinymce.init({
    selector: "textarea",
    plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
});
</script> 
<section id="main-content">
  <section class="wrapper">
    <h3 align="center">Add Subcategory</h3>
    <!-- BASIC FORM ELELEMNTS -->
    <div class="row mt">
      <div class="col-lg-12">
         <div><!--<div class="form-panel">-->
          <h4 class="mb"><i class="fa fa-angle-right"></i>Insert Subcategory</h4>
          <h2><?php echo validation_errors(); ?></h2>         
        <?php echo form_open_multipart('index.php/subcategory/insertsubcategory');?>
         <?php /*?> <?php echo form_open('index.php/Slider/insertSlider');?><?php */?>
              <div class="form-group">
              <label class="col-sm-2 col-sm-2 control-label">Subcategory Name :</label>
              <div class="col-sm-10">
                <input type="text" name="subcatname" class="form-control"><br /><br />
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 col-sm-2 control-label">Subcategory Title :</label>
              <div class="col-sm-10">
                <input type="text" name="subcattitle" class="form-control"><br /><br />
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 col-sm-2 control-label">Subcategory Discription :</label>
              <div class="col-sm-10">
                <textarea name="subcatdisc" class="form-control"> </textarea><br /><br />
              </div>
            </div>
             <div class="form-group">
              <label class="col-sm-2 col-sm-2 control-label">Subcategory Keywords :</label>
              <div class="col-sm-10">
                <input type="text" name="subcatkeyword" class="form-control"><br /><br />
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 col-sm-2 control-label">Category :</label>
              <div class="col-sm-10">               
                 <select name="cat" class="form-control" > 
                 <option>-------Select Category-------</option>                         
                 <?php
				// print_r($get_categotylist );
				foreach($category as $categotylist_data){
			echo '<option value="'.$categotylist_data['categoryId'].'">'.$categotylist_data['category_Name'].'</option>';
				}
				?>                     							
				</select> <br />   
              </div>
            </div>     
           <div class="form-group">
                   <label class="col-sm-2 col-sm-2 control-label" for="userfile">Subcategory Image :</label>
              <div class="col-sm-10">
              	<?php /*?><?php echo form_open_multipart('index.php/Slider/insertSlider');?><?php */?>
                <input type="file" name="userfile"   class="form-control"><br /><br />
              </div>
            </div>
           <button name="submit" class="btn btn-theme" type="submit">Submit</button>
            </form>
        </div>
      </div>
      <!-- col-lg-12--> 
    </div>
    <!-- /row --> 
  </section>
  <! --/wrapper --> 
</section>
>
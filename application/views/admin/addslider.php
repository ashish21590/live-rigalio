<section id="main-content">
  <section class="wrapper">
    <h3><i class="fa fa-angle-right"></i> Slider Component</h3>
    <!-- BASIC FORM ELELEMNTS -->
    <div class="row mt">
      <div class="col-lg-12">
         <div><!--<div class="form-panel">-->
          <h4 class="mb"><i class="fa fa-angle-right"></i>Insert Slider</h4>
          <h2><?php echo validation_errors(); ?></h2>         
          
        <?php echo form_open_multipart('index.php/slider/insertslider');?>
         
<?php /*?> <?php echo form_open('index.php/Slider/insertSlider');?><?php */?>
              <div class="form-group">
              <label class="col-sm-2 col-sm-2 control-label">Slider Text :</label>
              <div class="col-sm-10">
                <input type="text" name="slidertxt" class="form-control"><br /><br />
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 col-sm-2 control-label">Slider URL :</label>
              <div class="col-sm-10">
                <input type="text" name="sliderurl" class="form-control"><?php echo form_error('sliderurl'); ?><br /><br />
              </div>
            </div>
            <div class="form-group">
                   <label class="col-sm-2 col-sm-2 control-label" for="userfile">Upload Image :</label>
              <div class="col-sm-10">
              	<?php /*?><?php echo form_open_multipart('index.php/Slider/insertSlider');?><?php */?>
                <input type="file" name="userfile"   class="form-control"><?php echo form_error('sliderimage'); ?><br /><br />
              </div>
            </div>
           <button name="submit" class="btn btn-theme" type="submit">Submit</button>
            </form>
        </div>
      </div>
      <!-- col-lg-12--> 
    </div>
    <!-- /row --> 
  </section>
  <! --/wrapper --> 
</section>

<section id="main-content">
  <section class="wrapper">
    <h3><i class="fa fa-angle-right"></i>Manage Brand News</h3>
    <div class="row mt">
      <div class="col-lg-12">
        <div class="content-panel">
          <!--<h4><i class="fa fa-angle-right"></i> Responsive Table</h4>-->
          <section id="unseen">
              <table class="table table-bordered table-striped table-condensed">
              <thead>
                <tr>
                  <th>S. No.</th>
                  <th>Brand Name</th>
                  <th>News Headline</th>
                  <th>News Excert Content</th>
                 <!-- <th>News Inner Content</th>-->
                  <!--<th>News Link</th>-->
                  <th>News Block Image</th>
                  <th>News Cover Image</th>
                  <th>Manage</th>
                </tr>
              </thead>
              <tbody>
              <?php
			  $sno = 1;
			  foreach($list_brandnews as $list_brandnews){
			  ?>
                <tr>
                  <td>                  
                  <?php echo $sno;?></td>
                  <td><?php echo $list_brandnews['brand_id'];?></td>
                  <td><?php echo $list_brandnews['headline'];?></td>
                  <td><?php echo $list_brandnews['news_small_desc'];?></td>
                <?php /*?>  <td><?php echo $list_brandnews['newslong_desc'];?></td><?php */?>
                 <?php /*?> <td><?php echo $list_brandnews['news_link'];?></td><?php */?>
                  <td><img src="<?php echo base_url().$list_brandnews['news_img'];?>"/></td>
                  <td><img src="<?php echo base_url().$list_brandnews['banner_img'];?>"/></td>
                
                  <td><a href="<?php echo base_url();?>Admin/editbrandnews/<?php echo $list_brandnews['news_id']; ?>"><img src="<?php echo base_url();?>assets/img/icons/pencil_48.png" width="18"></a>&nbsp;|&nbsp;
                  <a onClick="confirm('Are you sure!');" href="<?php echo base_url();?>Admin/deleteBrand/<?php echo $list_brandnews['news_id']; ?>"><img src="<?php echo base_url();?>assets/img/icons/cross.png"</a></td>
                </tr>
              <?php $sno++;
			  }?>
              </tbody>
            </table>
          </section>
        </div>
      </div>
    </div>
  </section>
</section>

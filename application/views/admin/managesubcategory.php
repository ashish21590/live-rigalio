<section id="main-content">
  <section class="wrapper">
    <h3><i class="fa fa-angle-right"></i>Manage Category</h3>
    <div class="row mt">
      <div class="col-lg-12">
        <div class="content-panel">
          <!--<h4><i class="fa fa-angle-right"></i> Responsive Table</h4>-->
          <section id="unseen">
              <table class="table table-bordered table-striped table-condensed">
              <thead>
                <tr>
                  <th>S. No.</th>
                  <th>Subcategory Name</th>
                  <th>Subcategory Title</th>
                  <th>Subcategory Image</th>
                  <th>Subcategory Discription</th>
                  <th>Subcategory Keyword</th>
                  <th>Manage</th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody>
              <?php
			  $sno = 1;
			  foreach($get_categotylist as $categotylist_data){
			  ?>
                <tr>
                  <td><?php echo $sno; ?></td>
                  <td><?php echo $categotylist_data['subCategory_Name'];?></td>
                  <td><?php echo $categotylist_data['title'];?></td>
                  <td><img src="<?php echo base_url().$categotylist_data['subCategory_Image'];?>"/></td>
                   <td><?php echo $categotylist_data['description'];?></td>
                   <td><?php echo $categotylist_data['keywords'];?></td>
                  
                  <td><a href="<?php echo base_url();?>index.php/subcategory/editsubCategory/<?php echo $categotylist_data['subCategoryId']; ?>"><img src="<?php echo base_url();?>assets/img/icons/pencil_48.png" width="18"></a>&nbsp;|&nbsp;<a onClick="confirm('Are you sure!');" href="<?php echo base_url();?>index.php/subcategory/deletesubCategory/<?php echo $categotylist_data['subCategoryId']; ?>"><img src="<?php echo base_url();?>assets/img/icons/cross.png"</a></td>
                  
                  <td><a href="<?php echo base_url();?>index.php/subcategory/isActivesubCategory/<?php echo $categotylist_data['subCategoryId']; ?>/<?php echo $categotylist_data['isActive']; ?>"><?php if($categotylist_data['isActive']==1){?><img src="<?php echo base_url();?>assets/img/icons/active.png" width="18"><?php }else{?><img src="<?php echo base_url();?>assets/img/icons/deactivate.png" width="18"><?php }?></a></td>
                </tr>
              <?php $sno++;}?>
              </tbody>
            </table>
          </section>
        </div>
      </div>
    </div>
  </section>
</section>

<?php
foreach($get_category as $category_data)
{
}
?>
<script src="http://tinymce.cachefly.net/4.0/tinymce.min.js"></script>
<script type="text/javascript">
tinymce.init({
    selector: "textarea",
    plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
});
</script> 

<section id="main-content">
  <section class="wrapper">
    <h3><i class="fa fa-angle-right"></i> Product Components</h3>
    
    <!-- BASIC FORM ELELEMNTS -->
    <div class="row mt">
      <div class="col-lg-12">
         <div><!--<div class="form-panel">-->
          <h4 class="mb"><i class="fa fa-angle-right"></i>Insert Product</h4>
           <h2><?php echo validation_errors(); ?></h2>
          
          <?php /*?><?php
		  $id=$this->uri->segment();	
		   echo form_open_multipart('index.php/Slider/updateSlider/'".$id."'');?><?php */?>
           
           <?php echo form_open_multipart("Admin/updateProduct")?>
          
           <input type="hidden" name="hide" value="<?php echo $this->uri->segment(3)?>"/>          
          
             <div class="form-group">
              <label class="col-sm-2 col-sm-2 control-label">Product Name :</label>
              <div class="col-sm-10">
                <input type="text" name="productname" class="form-control" value="<?php echo $category_data['product_Name'];?>"><br /><br />
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 col-sm-2 control-label">Product Price :</label>
              <div class="col-sm-10">
                <input type="text" name="productprice" class="form-control" value="<?php echo $category_data['Price'];?>"><br /><br />
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 col-sm-2 control-label">Product Model :</label>
              <div class="col-sm-10">
                <input type="text" name="productmodel" class="form-control" value="<?php echo $category_data['model_no'];?>"><br /><br />
              </div>
            </div>


            <div class="form-group">
              <label class="col-sm-2 col-sm-2 control-label">Product Small Discription:</label>
              <div class="col-sm-10">
                <textarea name="productsmalldisc" class="form-control"><?php echo $category_data['productSmallDiscription'];?> </textarea><br /><br />
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 col-sm-2 control-label">Product Long Discription:</label>
              <div class="col-sm-10">
              <textarea name="productlongdisc" class="form-control"><?php echo $category_data['productlongDiscription'];?> </textarea><br /><br />
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 col-sm-2 control-label">Product Title :</label>
              <div class="col-sm-10">
                <input type="text" name="producttitle" class="form-control" value="<?php echo $category_data['product_Title'];?>"><br /><br />
              </div>
            </div> 
            <div class="form-group">
              <label class="col-sm-2 col-sm-2 control-label">Gender :</label>
              <div class="col-sm-10">                
                <select name="gender" class="form-control">
                <option>-------Select-------</option>                
				<option value="0" <?php if($category_data['gender']==0){ echo 'selected="selected"';} ?>>MEN</option>
				<option value="1" <?php if($category_data['gender']==1){ echo 'selected="selected"';} ?>>WOMEN</option>				
				</select> <br />         
              </div>
              </div>
              <div class="form-group">
              <label class="col-sm-2 col-sm-2 control-label">Product Category :</label>
              <div class="col-sm-10">                
                <select name="productcat" class="form-control" > 
                 <option >-------Select Category-------</option>  
                 <?php
				// print_r($get_categotylist );
				foreach($rs as $categotylist_data){
					//$this->output->enable_profiler(TRUE);
					?>
			<option value="<?php echo $categotylist_data['categoryId'];?>" <?php if($categotylist_data['categoryId']==$category_data['categoryId']){ echo "selected";}?>><?php echo $categotylist_data['category_Name'];?></option>
				<?php }?>                   							
				</select> <br />         
              </div>
              </div>
              
              <div class="form-group">
              <label class="col-sm-2 col-sm-2 control-label">Product Sub Category :</label>
              <div class="col-sm-10">                
                <select name="productsubcat" class="form-control" > 
                 <option >-------Select Sub Category-------</option>  
                 <?php
				// print_r($get_categotylist );
				foreach($subcategory as $subcategotylist_data){
					//$this->output->enable_profiler(TRUE);
					?>
			<option value="<?php echo $subcategotylist_data['subCategoryId'];?>" <?php if($subcategotylist_data['subCategoryId']==$category_data['subCategoryId']){ echo "selected";}?>><?php echo $subcategotylist_data['subCategory_Name'];?></option>
				<?php }?>                   							
				</select> <br />         
              </div>
              </div>
              
              <div class="form-group">
              <label class="col-sm-2 col-sm-2 control-label">Product Brand :</label>
              <div class="col-sm-10">                
                <select name="productbrand" class="form-control" > 
                 <option>-------Select Brand-------</option> 
                <?php
				 //print_r($get_brandlist );
				foreach($result as $brandlist_data){
					//$this->output->enable_profiler(TRUE);
					?>
			<option value="<?php echo $brandlist_data['brandId'];?>" <?php if($brandlist_data['brandId']==$category_data['brandId']){ echo "selected";}?>><?php echo $brandlist_data['brand_Name'];?></option>
				<?php }?>                    							
				</select> <br />         
              </div>
              </div>
              <!--<div class="form-group">
              <label class="col-sm-2 col-sm-2 control-label">Product Category :</label>
              <div class="col-sm-10">               
                 <select name="productcat" class="form-control">                         
                <?php /*?> <?php
				// print_r($get_categotylist );
				foreach($rs as $categotylist_data){
				echo '<option value="'.$categotylist_data['id'].'">'.$categotylist_data['category'].'</option>';
				}
				?> <?php */?>                							
				</select> <br />   
              </div>
            </div>         --> 
            <div class="form-group">           
              <label class="col-sm-2 col-sm-2 control-label" for="userfile">Upload New Image :</label> 
              <?php
				// print_r($get_categotylist );
				//print_r($image);
				if(!$image){ ?>
					<!--echo '<input type="file" name="userfile" class="form-control" value="">';-->
                     <div class="col-sm-10">
                    <input type="file" name="userfile" class="form-control" value="">
                     </div>
					<?php }
				else{
				foreach($image as $productimage_list){
					//$this->output->enable_profiler(TRUE);
					?>              
              <div class="col-sm-10">
              <img src="<?php echo base_url().$productimage_list['image_Name'];?>" width="15%" height="10%" /><br />
              <input type="hidden" name="up" value="<?php echo $productimage_list['image_Name']; ?>"/>
            <input type="file" name="userfile" class="form-control" value="<?php echo $productimage_list['image_Name']; ?>">
                <?php } }?>
                
              </div>
              
            </div>                    
            <button name="submit" class="btn btn-theme" type="submit">Submit</button>  
          </form>
        </div>
      </div>
      <!-- col-lg-12--> 
    </div>
    <!-- /row --> 
    
  </section>
  <! --/wrapper --> 
</section>

<section id="main-content">
  <section class="wrapper">
    <h3><i class="fa fa-angle-right"></i>Manage Brand News</h3>
    <div class="row mt">
      <div class="col-lg-12">
        <div class="content-panel">
          <!--<h4><i class="fa fa-angle-right"></i> Responsive Table</h4>-->
          <section id="unseen">
              <table class="table table-bordered table-striped table-condensed">
              <thead>
                <tr>
                  <th>S. No.</th>
                  <th>product Name</th>
                  <th>product Highlight</th>
                  <th>Manage</th>
                </tr>
              </thead>
              <tbody>
              <?php
			  $sno = 1;
			  foreach($product_highlight as $product_highlight){
			  ?>
                <tr>
                  <td>                  
                  <?php echo $sno;?></td>
                  <td><?php echo $product_highlight['productId_Fk'];?></td>
                  <td><?php echo $product_highlight['highlight'];?></td>
                  <td><a href="<?php echo base_url();?>Admin/editProductHighlights/<?php echo $product_highlight['highlightId']; ?>"><img src="<?php echo base_url();?>assets/img/icons/pencil_48.png" width="18"></a>&nbsp;|&nbsp;
                  <a onClick="confirm('Are you sure!');" href="<?php echo base_url();?>Admin/deleteProductHighlights/<?php echo $product_highlight['highlightId']; ?>"><img src="<?php echo base_url();?>assets/img/icons/cross.png"</a></td>
                </tr>
              <?php $sno++;
			  }?>
              </tbody>
            </table>
          </section>
        </div>
      </div>
    </div>
  </section>
</section>

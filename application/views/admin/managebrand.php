
<section id="main-content">
  <section class="wrapper">
    <h3><i class="fa fa-angle-right"></i>Manage Brand</h3>
    <div class="row mt">
      <div class="col-lg-12">
        <div class="content-panel">
          <!--<h4><i class="fa fa-angle-right"></i> Responsive Table</h4>-->
          <section id="unseen">
              <table class="table table-bordered table-striped table-condensed">
              <thead>
                <tr>
                  <th>S. No.</th>
                  <th>Brand Name</th>
                  <th>Established year</th>
                  <th>Brand Image</th>
                  <th>Brand Discription</th>
                  <th>Brand Small Discription</th>
                  <th>Brand Long Discription</th>
                  <th>Brand Title</th>
                  <th>Manage</th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody>
              <?php
			  $sno = 1;
			  foreach($get_categotylist as $categotylist_data){
			  ?>
                <tr>
                  <td>                  
                  <?php echo $sno;?></td>
                  <td><?php echo $categotylist_data['brand_Name'];?></td>
                  <td><?php echo $categotylist_data['Establish'];?></td>
                  <td><img src="<?php echo base_url().$categotylist_data['brand_image'];?>"/></td>
                  <td><?php echo $categotylist_data['brandDescription'];?></td>
                  <td><?php echo $categotylist_data['brandSmallDescription'];?></td>
                  <td><?php echo $categotylist_data['brandlongDescription'];?></td>
                  <td><?php echo $categotylist_data['title'];?></td>
                
                  <td><a href="<?php echo base_url();?>Admin/editBrand/<?php echo $categotylist_data['brandId']; ?>"><img src="<?php echo base_url();?>assets/img/icons/pencil_48.png" width="18"></a>&nbsp;|&nbsp;
                  <a onClick="confirm('Are you sure!');" href="<?php echo base_url();?>Admin/deleteBrand/<?php echo $categotylist_data['brandId']; ?>"><img src="<?php echo base_url();?>assets/img/icons/cross.png"</a></td>
                  
                  <td><a href="<?php echo base_url();?>Admin/isActiveBrand/<?php echo $categotylist_data['brandId']; ?>/<?php echo $categotylist_data['isActive']; ?>"><?php if($categotylist_data['isActive']==1){?><img src="<?php echo base_url();?>assets/img/icons/active.png" width="18"><?php }else{?><img src="<?php echo base_url();?>assets/img/icons/deactivate.png" width="18"><?php }?></a></td>
                </tr>
              <?php $sno++;
			  }?>
              </tbody>
            </table>
          </section>
        </div>
      </div>
    </div>
  </section>
</section>

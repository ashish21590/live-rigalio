<section id="main-content">
  <section class="wrapper">
    <h3><i class="fa fa-angle-right"></i>Manage specification type</h3>
    <div class="row mt">
      <div class="col-lg-12">
        <div class="content-panel">
          <!--<h4><i class="fa fa-angle-right"></i> Responsive Table</h4>-->
          <section id="unseen">
              <table class="table table-bordered table-striped table-condensed">
              <thead>
                <tr>
                  <th>S. No.</th>
                  <th>specification type Name</th>
                  <th>Manage</th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody>
              <?php
			  $sno = 1;
			  foreach($specifictionType as $specifictionType_data){
			  ?>
                <tr>
                  <td>                  
                  <?php echo $sno;?></td>
                  <td><?php echo $specifictionType_data['specifictionType_Name'];?></td>
                  
                  <td><a href="<?php echo base_url();?>index.php/specification/editspecifictionType/<?php echo $specifictionType_data['specifictionTypeId']; ?>"><img src="<?php echo base_url();?>assets/img/icons/pencil_48.png" width="18"></a>&nbsp;|&nbsp;
                  <a onClick="confirm('Are you sure!');" href="<?php echo base_url();?>index.php/specification/deletespecifictionType/<?php echo $specifictionType_data['specifictionTypeId']; ?>"><img src="<?php echo base_url();?>assets/img/icons/cross.png"</a></td>
                  
                  <td><a href="<?php echo base_url();?>index.php/specification/isActivespecifictionType/<?php echo $specifictionType_data['specifictionTypeId']; ?>/<?php echo $specifictionType_data['isActive']; ?>"><?php if($specifictionType_data['isActive']==1){?><img src="<?php echo base_url();?>assets/img/icons/active.png" width="18"><?php }else{?><img src="<?php echo base_url();?>assets/img/icons/deactivate.png" width="18"><?php }?></a></td>
                </tr>
              <?php $sno++;
			  }?>
              </tbody>
            </table>
          </section>
        </div>
      </div>
    </div>
  </section>
</section>

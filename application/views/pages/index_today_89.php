

    <!-- Carousel
    ================================================== -->
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding mg-top">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="main-slider-container hidden-xs">  
          <div id="myCarousel" class="carousel main-slider slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
              <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
              <li data-target="#myCarousel" data-slide-to="1"></li>
              <li data-target="#myCarousel" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner" role="listbox">
              <div class="item active">
                <div class="row">
                  <div class="slider-sections sectn1">
                   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images banner-img1-sectn1 nopadding"> <img src="content/images/banner/banner-img5.jpg" class="img-responsive">
                    <div class="banner-hover-con">
                     <span><h4>Add a pop of yellow to black and gray </h4>
                       <hr>
                       <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                     </div> <!--/banner-hover-con -->
                   </div>
                   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images banner-img2-sectn1 nopadding"> <img src="content/images/banner/banner-img7.jpg" class="img-responsive"> 
                    <div class="banner-hover-con">
                     <span><h4>Add a pop of yellow to black and gray </h4>
                       <hr>
                       <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                     </div> <!--/banner-hover-con -->
                   </div>
                 </div>
                 <div class="slider-sections sectn2">
                   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images banner-img1-sectn2 nopadding"> <img src="content/images/banner/banner-img4.jpg" class="img-responsive"> 
                    <div class="banner-hover-con">
                     <span><h4>Add a pop of yellow to black and gray </h4>
                       <hr>
                       <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                     </div> <!--/banner-hover-con -->
                   </div>
                 </div>
                 <div class="slider-sections sectn3">
                   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images banner-img1-sectn3 nopadding"> <img src="content/images/banner/banner-img5.jpg" class="img-responsive"> 
                    <div class="banner-hover-con">
                     <span><h4>Add a pop of yellow to black and gray </h4>
                       <hr>
                       <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                     </div> <!--/banner-hover-con -->
                   </div>
                   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images nopadding banner-img2-sectn3"> <img src="content/images/banner/banner-img7.jpg" class="img-responsive"> 
                    <div class="banner-hover-con">
                     <span><h4>Add a pop of yellow to black and gray </h4>
                       <hr>
                       <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                     </div> <!--/banner-hover-con -->
                   </div>
                 </div>
                 <div class="slider-sections sectn4">
                   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images nopadding banner-img1-sectn4"> <img src="content/images/banner/banner-img4.jpg" class="img-responsive"> 
                     <div class="banner-hover-con">
                       <span><h4>Add a pop of yellow to black and gray </h4>
                         <hr>
                         <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                       </div> <!--/banner-hover-con -->
                     </div>
                   </div>
                   <div class="slider-sections sectn5 hidden-sm">
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images nopadding banner-img1-sectn5"> <img src="content/images/banner/banner-img5.jpg" class="img-responsive"> 
                      <div class="banner-hover-con">
                       <span><h4>Add a pop of yellow to black and gray </h4>
                         <hr>
                         <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                       </div> <!--/banner-hover-con -->
                     </div>
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images nopadding banner-img2-sectn5"> <img src="content/images/banner/banner-img7.jpg" class="img-responsive"> 
                      <div class="banner-hover-con">
                       <span><h4>Add a pop of yellow to black and gray </h4>
                         <hr>
                         <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                       </div> <!--/banner-hover-con -->
                     </div>
                   </div>
                 </div>
               </div> <!--/slide1 -->
               <div class="item">
                 <div class="row">
                  <div class="slider-sections sectn1">
                   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images banner-img1-sectn1 nopadding"> <img src="content/images/banner/banner-img5.jpg" class="img-responsive">
                    <div class="banner-hover-con">
                     <span><h4>Add a pop of yellow to black and gray </h4>
                       <hr>
                       <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                     </div> <!--/banner-hover-con -->
                   </div>
                   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images banner-img2-sectn1 nopadding"> <img src="content/images/banner/banner-img7.jpg" class="img-responsive"> 
                    <div class="banner-hover-con">
                     <span><h4>Add a pop of yellow to black and gray </h4>
                       <hr>
                       <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                     </div> <!--/banner-hover-con -->
                   </div>
                 </div>
                 <div class="slider-sections sectn2">
                   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images banner-img1-sectn2 nopadding"> <img src="content/images/banner/banner-img4.jpg" class="img-responsive"> 
                    <div class="banner-hover-con">
                     <span><h4>Add a pop of yellow to black and gray </h4>
                       <hr>
                       <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                     </div> <!--/banner-hover-con -->
                   </div>
                 </div>
                 <div class="slider-sections sectn3">
                   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images banner-img1-sectn3 nopadding"> <img src="content/images/banner/banner-img5.jpg" class="img-responsive"> 
                    <div class="banner-hover-con">
                     <span><h4>Add a pop of yellow to black and gray </h4>
                       <hr>
                       <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                     </div> <!--/banner-hover-con -->
                   </div>
                   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images nopadding banner-img2-sectn3"> <img src="content/images/banner/banner-img7.jpg" class="img-responsive"> 
                    <div class="banner-hover-con">
                     <span><h4>Add a pop of yellow to black and gray </h4>
                       <hr>
                       <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                     </div> <!--/banner-hover-con -->
                   </div>
                 </div>
                 <div class="slider-sections sectn4">
                   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images nopadding banner-img1-sectn4"> <img src="content/images/banner/banner-img4.jpg" class="img-responsive"> 
                     <div class="banner-hover-con">
                       <span><h4>Add a pop of yellow to black and gray </h4>
                         <hr>
                         <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                       </div> <!--/banner-hover-con -->
                     </div>
                   </div>
                   <div class="slider-sections sectn5 hidden-sm">
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images nopadding banner-img1-sectn5"> <img src="content/images/banner/banner-img5.jpg" class="img-responsive"> 
                      <div class="banner-hover-con">
                       <span><h4>Add a pop of yellow to black and gray </h4>
                         <hr>
                         <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                       </div> <!--/banner-hover-con -->
                     </div>
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images nopadding banner-img2-sectn5"> <img src="content/images/banner/banner-img7.jpg" class="img-responsive"> 
                      <div class="banner-hover-con">
                       <span><h4>Add a pop of yellow to black and gray </h4>
                         <hr>
                         <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                       </div> <!--/banner-hover-con -->
                     </div>
                   </div>
                 </div>
               </div> <!--/slide2 -->
               <div class="item">
                <div class="row">
                  <div class="slider-sections sectn1">
                   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images banner-img1-sectn1 nopadding"> <img src="content/images/banner/banner-img5.jpg" class="img-responsive">
                    <div class="banner-hover-con">
                     <span><h4>Add a pop of yellow to black and gray </h4>
                       <hr>
                       <div class="cat-zoom-icon"> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                     </div> <!--/banner-hover-con -->
                   </div>
                   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images banner-img2-sectn1 nopadding"> <img src="content/images/banner/banner-img7.jpg" class="img-responsive"> 
                    <div class="banner-hover-con">
                     <span><h4>Add a pop of yellow to black and gray </h4>
                       <hr>
                       <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                     </div> <!--/banner-hover-con -->
                   </div>
                 </div>
                 <div class="slider-sections sectn2">
                   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images banner-img1-sectn2 nopadding"> <img src="content/images/banner/banner-img4.jpg" class="img-responsive"> 
                    <div class="banner-hover-con">
                     <span><h4>Add a pop of yellow to black and gray </h4>
                       <hr>
                       <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                     </div> <!--/banner-hover-con -->
                   </div>
                 </div>
                 <div class="slider-sections sectn3">
                   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images banner-img1-sectn3 nopadding"> <img src="content/images/banner/banner-img5.jpg" class="img-responsive"> 
                    <div class="banner-hover-con">
                     <span><h4>Add a pop of yellow to black and gray </h4>
                       <hr>
                       <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                     </div> <!--/banner-hover-con -->
                   </div>
                   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images nopadding banner-img2-sectn3"> <img src="content/images/banner/banner-img7.jpg" class="img-responsive"> 
                    <div class="banner-hover-con">
                     <span><h4>Add a pop of yellow to black and gray </h4>
                       <hr>
                       <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                     </div> <!--/banner-hover-con -->
                   </div>
                 </div>
                 <div class="slider-sections sectn4">
                   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images nopadding banner-img1-sectn4"> <img src="content/images/banner/banner-img4.jpg" class="img-responsive"> 
                     <div class="banner-hover-con">
                       <span><h4>Add a pop of yellow to black and gray </h4>
                         <hr>
                         <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                       </div> <!--/banner-hover-con -->
                     </div>
                   </div>
                   <div class="slider-sections sectn5 hidden-sm">
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images nopadding banner-img1-sectn5"> <img src="content/images/banner/banner-img5.jpg" class="img-responsive"> 
                      <div class="banner-hover-con">
                       <span><h4>Add a pop of yellow to black and gray </h4>
                         <hr>
                         <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                       </div> <!--/banner-hover-con -->
                     </div>
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images nopadding banner-img2-sectn5"> <img src="content/images/banner/banner-img7.jpg" class="img-responsive"> 
                      <div class="banner-hover-con">
                       <span><h4>Add a pop of yellow to black and gray </h4>
                         <hr>
                         <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                       </div> <!--/banner-hover-con -->
                     </div>
                   </div>
                 </div>
               </div><!--/slide3 -->
             </div>



      <!--<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span>
        <span class="sr-only">Next</span>
      </a> -->
    </div><!--/carousel -->


    <!-- mobile view slider -->
    <div id="mbCarousel" class="carousel main-slider slide" data-ride="carousel" style="display:none;">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#mbCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#mbCarousel" data-slide-to="1"></li>
        <li data-target="#mbCarousel" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner" role="listbox">
        <div class="item active">
          <div class="row">
            <div class="slider-sections sectn1">
             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images banner-img1-sectn1 nopadding"> <img src="content/images/banner/banner-img5.jpg" class="img-responsive">
              <div class="banner-hover-con">
               <span><h4>Add a pop of yellow to black and gray </h4>
                 <hr>
                 <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
               </div> <!--/banner-hover-con -->
             </div>
             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images banner-img2-sectn1 nopadding"> <img src="content/images/banner/banner-img7.jpg" class="img-responsive"> 
              <div class="banner-hover-con">
               <span><h4>Add a pop of yellow to black and gray </h4>
                 <hr>
                 <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
               </div> <!--/banner-hover-con -->
             </div>
           </div>
           <div class="slider-sections sectn2">
             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images banner-img1-sectn2 nopadding"> <img src="content/images/banner/banner-img4.jpg" class="img-responsive"> 
              <div class="banner-hover-con">
               <span><h4>Add a pop of yellow to black and gray </h4>
                 <hr>
                 <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
               </div> <!--/banner-hover-con -->
             </div>
           </div>
           <div class="slider-sections sectn3">
             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images banner-img1-sectn3 nopadding"> <img src="content/images/banner/banner-img5.jpg" class="img-responsive"> 
              <div class="banner-hover-con">
               <span><h4>Add a pop of yellow to black and gray </h4>
                 <hr>
                 <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
               </div> <!--/banner-hover-con -->
             </div>
             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images nopadding banner-img2-sectn3"> <img src="content/images/banner/banner-img7.jpg" class="img-responsive"> 
              <div class="banner-hover-con">
               <span><h4>Add a pop of yellow to black and gray </h4>
                 <hr>
                 <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
               </div> <!--/banner-hover-con -->
             </div>
           </div>
           <div class="slider-sections sectn4">
             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images nopadding banner-img1-sectn4"> <img src="content/images/banner/banner-img4.jpg" class="img-responsive"> 
               <div class="banner-hover-con">
                 <span><h4>Add a pop of yellow to black and gray </h4>
                   <hr>
                   <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                 </div> <!--/banner-hover-con -->
               </div>
             </div>
             <div class="slider-sections sectn5">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images nopadding banner-img1-sectn5"> <img src="content/images/banner/banner-img5.jpg" class="img-responsive"> 
                <div class="banner-hover-con">
                 <span><h4>Add a pop of yellow to black and gray </h4>
                   <hr>
                   <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                 </div> <!--/banner-hover-con -->
               </div>
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images nopadding banner-img2-sectn5"> <img src="content/images/banner/banner-img7.jpg" class="img-responsive"> 
                <div class="banner-hover-con">
                 <span><h4>Add a pop of yellow to black and gray </h4>
                   <hr>
                   <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                 </div> <!--/banner-hover-con -->
               </div>
             </div>
           </div>
         </div> <!--/slide1 -->
         <div class="item">
           <div class="row">
            <div class="slider-sections sectn1">
             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images banner-img1-sectn1 nopadding"> <img src="content/images/banner/banner-img5.jpg" class="img-responsive">
              <div class="banner-hover-con">
               <span><h4>Add a pop of yellow to black and gray </h4>
                 <hr>
                 <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
               </div> <!--/banner-hover-con -->
             </div>
             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images banner-img2-sectn1 nopadding"> <img src="content/images/banner/banner-img7.jpg" class="img-responsive"> 
              <div class="banner-hover-con">
               <span><h4>Add a pop of yellow to black and gray </h4>
                 <hr>
                 <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
               </div> <!--/banner-hover-con -->
             </div>
           </div>
           <div class="slider-sections sectn2">
             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images banner-img1-sectn2 nopadding"> <img src="content/images/banner/banner-img4.jpg" class="img-responsive"> 
              <div class="banner-hover-con">
               <span><h4>Add a pop of yellow to black and gray </h4>
                 <hr>
                 <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
               </div> <!--/banner-hover-con -->
             </div>
           </div>
           <div class="slider-sections sectn3">
             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images banner-img1-sectn3 nopadding"> <img src="content/images/banner/banner-img5.jpg" class="img-responsive"> 
              <div class="banner-hover-con">
               <span><h4>Add a pop of yellow to black and gray </h4>
                 <hr>
                 <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
               </div> <!--/banner-hover-con -->
             </div>
             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images nopadding banner-img2-sectn3"> <img src="content/images/banner/banner-img7.jpg" class="img-responsive"> 
              <div class="banner-hover-con">
               <span><h4>Add a pop of yellow to black and gray </h4>
                 <hr>
                 <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
               </div> <!--/banner-hover-con -->
             </div>
           </div>
           <div class="slider-sections sectn4">
             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images nopadding banner-img1-sectn4"> <img src="content/images/banner/banner-img4.jpg" class="img-responsive"> 
               <div class="banner-hover-con">
                 <span><h4>Add a pop of yellow to black and gray </h4>
                   <hr>
                   <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                 </div> <!--/banner-hover-con -->
               </div>
             </div>
             <div class="slider-sections sectn5">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images nopadding banner-img1-sectn5"> <img src="content/images/banner/banner-img5.jpg" class="img-responsive"> 
                <div class="banner-hover-con">
                 <span><h4>Add a pop of yellow to black and gray </h4>
                   <hr>
                   <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                 </div> <!--/banner-hover-con -->
               </div>
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images nopadding banner-img2-sectn5"> <img src="content/images/banner/banner-img7.jpg" class="img-responsive"> 
                <div class="banner-hover-con">
                 <span><h4>Add a pop of yellow to black and gray </h4>
                   <hr>
                   <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                 </div> <!--/banner-hover-con -->
               </div>
             </div>
           </div>
         </div> <!--/slide2 -->
         
       </div>

     </div><!--/carousel -->
   </div> 


   <!--slider for mobile -->
   <div class="main-slider-container hidden-lg hidden-md hidden-sm">  
    
    <!-- mobile view slider -->
    <div id="mbCarousel" class="carousel main-slider slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#mbCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#mbCarousel" data-slide-to="1"></li>
        <li data-target="#mbCarousel" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner" role="listbox">
        <div class="item active">
          <div class="row">
            <div class="col-sm-12 col-xs-12"> 
              <div class="col-sm-6 col-xs-6 banner-images banner-img1-sectn1 nopadding"> <img src="content/images/banner/banner-img5.jpg" class="img-responsive">
                <div class="banner-hover-con">
                 <span><h4>Add a pop of yellow to black and gray </h4>
                   <hr>
                   <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                 </div> <!--/banner-hover-con -->
               </div>
               <div class="col-sm-6 col-xs-6 banner-images banner-img1-sectn1 nopadding"> <img src="content/images/banner/banner-img5.jpg" class="img-responsive">
                <div class="banner-hover-con">
                 <span><h4>Add a pop of yellow to black and gray </h4>
                   <hr>
                   <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                 </div> <!--/banner-hover-con -->
               </div>
               <div class="col-sm-6 col-xs-6 banner-images banner-img1-sectn1 nopadding"> <img src="content/images/banner/banner-img5.jpg" class="img-responsive">
                <div class="banner-hover-con">
                 <span><h4>Add a pop of yellow to black and gray </h4>
                   <hr>
                   <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                 </div> <!--/banner-hover-con -->
               </div>
               <div class="col-sm-6 col-xs-6 banner-images banner-img1-sectn1 nopadding"> <img src="content/images/banner/banner-img5.jpg" class="img-responsive">
                <div class="banner-hover-con">
                 <span><h4>Add a pop of yellow to black and gray </h4>
                   <hr>
                   <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                 </div> <!--/banner-hover-con -->
               </div>
               <div class="col-sm-6 col-xs-6 banner-images banner-img1-sectn1 nopadding"> <img src="content/images/banner/banner-img5.jpg" class="img-responsive">
                <div class="banner-hover-con">
                 <span><h4>Add a pop of yellow to black and gray </h4>
                   <hr>
                   <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                 </div> <!--/banner-hover-con -->
               </div>
               <div class="col-sm-6 col-xs-6 banner-images banner-img1-sectn1 nopadding"> <img src="content/images/banner/banner-img5.jpg" class="img-responsive">
                <div class="banner-hover-con">
                 <span><h4>Add a pop of yellow to black and gray </h4>
                   <hr>
                   <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                 </div> <!--/banner-hover-con -->
               </div> <!--/ all slide 1 container ends -->
             </div>
           </div>
         </div> <!--/slide1 -->
         <div class="item">
          <div class="row">
            <div class="col-sm-12 col-xs-12"> 
              <div class="col-sm-6 col-xs-6 banner-images banner-img1-sectn1 nopadding"> <img src="content/images/banner/banner-img5.jpg" class="img-responsive">
                <div class="banner-hover-con">
                 <span><h4>Add a pop of yellow to black and gray </h4>
                   <hr>
                   <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                 </div> <!--/banner-hover-con -->
               </div>
               <div class="col-sm-6 col-xs-6 banner-images banner-img1-sectn1 nopadding"> <img src="content/images/banner/banner-img5.jpg" class="img-responsive">
                <div class="banner-hover-con">
                 <span><h4>Add a pop of yellow to black and gray </h4>
                   <hr>
                   <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                 </div> <!--/banner-hover-con -->
               </div>
               <div class="col-sm-6 col-xs-6 banner-images banner-img1-sectn1 nopadding"> <img src="content/images/banner/banner-img5.jpg" class="img-responsive">
                <div class="banner-hover-con">
                 <span><h4>Add a pop of yellow to black and gray </h4>
                   <hr>
                   <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                 </div> <!--/banner-hover-con -->
               </div>
               <div class="col-sm-6 col-xs-6 banner-images banner-img1-sectn1 nopadding"> <img src="content/images/banner/banner-img5.jpg" class="img-responsive">
                <div class="banner-hover-con">
                 <span><h4>Add a pop of yellow to black and gray </h4>
                   <hr>
                   <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                 </div> <!--/banner-hover-con -->
               </div>
               <div class="col-sm-6 col-xs-6 banner-images banner-img1-sectn1 nopadding"> <img src="content/images/banner/banner-img5.jpg" class="img-responsive">
                <div class="banner-hover-con">
                 <span><h4>Add a pop of yellow to black and gray </h4>
                   <hr>
                   <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                 </div> <!--/banner-hover-con -->
               </div>
               <div class="col-sm-6 col-xs-6 banner-images banner-img1-sectn1 nopadding"> <img src="content/images/banner/banner-img5.jpg" class="img-responsive">
                <div class="banner-hover-con">
                 <span><h4>Add a pop of yellow to black and gray </h4>
                   <hr>
                   <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                 </div> <!--/banner-hover-con -->
               </div> <!--/ all slide 2 container ends -->
             </div>
           </div>
         </div> <!--/slide2 -->
         <div class="item">
          <div class="row">
            <div class="col-sm-12 col-xs-12"> 
              <div class="col-sm-6 col-xs-6 banner-images banner-img1-sectn1 nopadding"> <img src="content/images/banner/banner-img5.jpg" class="img-responsive">
                <div class="banner-hover-con">
                 <span><h4>Add a pop of yellow to black and gray </h4>
                   <hr>
                   <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                 </div> <!--/banner-hover-con -->
               </div>
               <div class="col-sm-6 col-xs-6 banner-images banner-img1-sectn1 nopadding"> <img src="content/images/banner/banner-img5.jpg" class="img-responsive">
                <div class="banner-hover-con">
                 <span><h4>Add a pop of yellow to black and gray </h4>
                   <hr>
                   <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                 </div> <!--/banner-hover-con -->
               </div>
               <div class="col-sm-6 col-xs-6 banner-images banner-img1-sectn1 nopadding"> <img src="content/images/banner/banner-img5.jpg" class="img-responsive">
                <div class="banner-hover-con">
                 <span><h4>Add a pop of yellow to black and gray </h4>
                   <hr>
                   <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                 </div> <!--/banner-hover-con -->
               </div>
               <div class="col-sm-6 col-xs-6 banner-images banner-img1-sectn1 nopadding"> <img src="content/images/banner/banner-img5.jpg" class="img-responsive">
                <div class="banner-hover-con">
                 <span><h4>Add a pop of yellow to black and gray </h4>
                   <hr>
                   <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                 </div> <!--/banner-hover-con -->
               </div>
               <div class="col-sm-6 col-xs-6 banner-images banner-img1-sectn1 nopadding"> <img src="content/images/banner/banner-img5.jpg" class="img-responsive">
                <div class="banner-hover-con">
                 <span><h4>Add a pop of yellow to black and gray </h4>
                   <hr>
                   <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                 </div> <!--/banner-hover-con -->
               </div>
               <div class="col-sm-6 col-xs-6 banner-images banner-img1-sectn1 nopadding"> <img src="content/images/banner/banner-img5.jpg" class="img-responsive">
                <div class="banner-hover-con">
                 <span><h4>Add a pop of yellow to black and gray </h4>
                   <hr>
                   <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                 </div> <!--/banner-hover-con -->
               </div> <!--/ all slide 3 container ends -->
             </div>
           </div>
         </div> <!--/slide3 -->
         
       </div>

     </div><!--/carousel -->
   </div> 

   <!--slider for mobile ends -->
 </div>      
</div>

<div class="main_content">
 
  <div class="main_content">


    
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
         <div class="col-lg-6 col-md-6 col-sm-8 col-xs-12 content_headline">
          <h2 ng-model="heading"><?php print_r($heading); ?></h2>
          <hr>
        </div> 
      </div> <!--/content tagline-->
      <div class="col-lg-10 col-md-10 col-sm-11 col-xs-12 wall whats_new_content" id="abc">
        

      </div>   <!--/whats_new_content --> 
      
      <!--/column-->

      <!-- mywork place ends here--> 
    </div> <!--/Whats_new_content-->
  </div> <!--/row-->
</div> <!--/container-fluid-->
</div> <!--/main_content--> <!--/main_content-->
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <div id="myjs" style="display: nonel"></div>
    <div id="total_product_count"><?php print_r($all_products_count); ?></div>
    <div class="container" style="text-align: center"><button class="btn" id="load_more" data-val = "0">Load more..<img style="display: none" id="loader" src="<?php echo str_replace('index.php','',base_url()) ?>asset/loader.GIF"> </button></div>
    <div class="grid" id="abcme">
    <!--<script type="text/javascript" src="https://code.jquery.com/jquery-1.2.6.pack.js"></script>-->
    <script>
 //var page1=[];

 $(document).ready(function(e){
      //alert("hi");
      //e.preventDefault();
      var page = $("#load_more").data('val');
      var total_product_count="<?php print_r($all_products_count);?>";
      var per_page=3;
            //alert(page);
            
            var total_pages=Math.ceil(parseInt(total_product_count)/parseInt(per_page));
            //alert(total_pages);
            if(page<=total_pages){
              //alert('onload');
              getcountry(page);
            }
            
          });

 $(window).scroll(function(e){

  if ($(window).scrollTop() == $(document).height() - $(window).height()){
        //alert("hi");
        e.preventDefault();
          // $("#load_more").data('val');
          page = $("#load_more").data('val');
          var total_product_count="<?php print_r($all_products_count);?>";
          var per_page=4;
           $('#load_more').data('val', ($('#load_more').data('val')+1));
           // alert(page);
           var total_pages=Math.ceil(parseInt(total_product_count)/parseInt(per_page));
            //alert(total_pages);
            if(page<total_pages){
              getcountry(page);
            }
            
            
            
          }
        }); 
 
 var getcountry = function(page){
   $("#loader").show();
   $('#load_more').data('val', ($('#load_more').data('val')+1));
   $.ajax({
    url:"<?php echo base_url() ?>myscroll/getCountry",
    type:'GET',
    data: {page:page}
  }).done(function(response){
    $("#abcme").append(response).show();   
    document.querySelector('.grid').gridify({srcNode: 'img', margin: '20px', width: '250px', resizable: true});  
$(".img-responsive").show();       
   // $('.wall').jaliswall({item:'.wall-item'});
/*
    $('.wall').masonry({
  columnWidth: 200,
  itemSelector: '.wall-item'
});
*/
   
    $("#loader").hide();
    
         //scroll();
    });
  
};
 /*
    var scroll  = function(){
        $('html, body').animate({
            scrollTop: $('#load_more').offset().top
        }, 6000);
    };
    */function myfunction(id,me){
      alert("hi");
      var id=id;
      alert(this.parent());
        var userid=$("#myid").html();
                //var source=$(this).parent().attr('id');

                //alert(source);
                if(userid==''){
                  alert("Please login first");
                }
                else{
          //alert(id);

          if(id){
            var data={
              "productid":id,
              "userid":userid
            }
            $.ajax({
             type: "POST",
             url: "http://localhost/finalrigalio/ajax/",
                 //dataType: 'json',
                 data: data,
                 success: function(html){
                  alert(html);
                  $("#"+id).next().css( "background", "yellow" );
                    //alert(html);
                    
                  }
                  });  //ajax ends here 
          } 
              } //else ends here
     }
    




/**
 * Created by khanhnh on 13/09/2014.
 */

'use strict';
Element.prototype.imagesLoaded = function (cb){
    var images = this.querySelectorAll('img');
    var count = images.length;
    if (count == 0) cb();
    for (var i= 0, length = images.length; i < length; i++)
    {
        var image = new Image();
        image.onload = function(e){
            count --;
            if (count == 0) cb()
        }
        image.onerror = function(e){
            count --;
            if (count == 0) cb()
        }
        image.src = images[i].getAttribute('src');
    }
}
Element.prototype.gridify = function (options)
{
    var self = this,
        options = options || {},
        indexOfSmallest = function (a) {
            var lowest = 0;
            for (var i = 1, length = a.length; i < length; i++) {
                if (a[i] < a[lowest]) lowest = i;
            }
            return lowest;
        },
        attachEvent = function(node, event, cb)
        {
            if (node.attachEvent)
                node.attachEvent('on'+event, cb);
            else if (node.addEventListener)
                node.addEventListener(event, cb);
        },
        detachEvent = function(node, event, cb)
        {
            if(node.detachEvent) {
                node.detachEvent('on'+event, cb);
            }
            else if(node.removeEventListener) {
                node.removeEventListener(event, render);
            }
        },
        render = function()
        {
            self.style.position = 'relative';
            var items = self.querySelectorAll(options.srcNode),
                transition = (options.transition || 'all 0.5s ease') + ', height 0, width 0',
                width = self.clientWidth,
                item_margin = parseInt(options.margin || 0),
                item_width = parseInt(options.max_width || options.width || 220),
                column_count = Math.max(Math.floor(width/(item_width + item_margin)),1),
                left = column_count == 1 ? item_margin/2 : (width % (item_width + item_margin)) / 2,
                columns = [];
            if (options.max_width)
            {
                column_count = Math.ceil(width/(item_width + item_margin));
                item_width = (width - column_count * item_margin - item_margin)/column_count;
                left = item_margin/2;
            }
            for (var i = 0; i < column_count; i++)
            {
                columns.push(0);
            }
            for (var i= 0, length = items.length; i < length; i++)
            {
                var idx = indexOfSmallest(columns);
                items[i].setAttribute('style', 'width: ' + item_width + 'px; ' +
                    'position: absolute; ' +
                    'margin: ' + item_margin/2 + 'px; ' +
                    'top: ' + (columns[idx] + item_margin/2) +'px; ' +
                    'left: ' + ((item_width + item_margin) * idx + left) + 'px; ' +
                    'transition: ' + transition);

                columns[idx] += items[i].clientHeight + item_margin;
            }
        };
    this.imagesLoaded(render);
    if (options.resizable)
    {
        attachEvent(window, 'resize', render);
        attachEvent(self, 'DOMNodeRemoved', function(){
            detachEvent(window, 'resize', render);
        })
    }
}





  </script>
</body>

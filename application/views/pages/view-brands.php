<!--view brands con -->
<div class="view-brand-pg col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
  <div class="container-fluid">
    <div class="row">

      <div class="view-brand-con col-lg-10 col-md-10 col-sm-11 col-xs-11 nopadding">
          <div class="view-brand-tag col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <a href="<?php echo base_url(); ?>timeline"> <button class="goback-btn pull-left browse-btn"><span class="icomoon icon-slider-left-arrow"></span>back</button></a><h3 class="
             mg-top">Brands </h3>
          </div> <!--/search-tagline -->
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 wall whats_new_content">
          <div class="grid" id="masonry-grid">
		  <?php foreach($userbrand as $brand)
		  {?>
              <div class="wall-column grid-item col-sm-6 col-xs-12">
                <div class="wall-item">
                  <div class="category">
                                    <a href="<?php echo base_url(); ?>brand/<?php echo str_replace(" ", "-", strtolower($brand['brand_Name'])); ?>/<?php echo $brand['brandId']; ?>/legacy">
                                        <div class="brand-result-img"><img
                                                src="<?php echo base_url(); ?><?php echo $brand['brand_image']; ?>"
                                                class="img-responsive"></div>
                                        <div class="brand-result-con"><span><?php echo $brand['brand_Name']; ?> </span>
                                    </a>
                                    <div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 myan brand-follow"
                                             id="<?php echo $brand['brandId']; ?>">
                                            <button class="follow-brand-btn following" data-text="Follow brand"><i
                                                    class="fa fa-check"></i></button>
                    </div>

                                    </div>
                                </div>

                            </div> <!--/brand-result content -->

                </div> <!--/wall-item -->
              </div> <!--/wall-column -->
              <?php } ?>
            
          </div>    
          </div><!--/whats_new_content -->
      </div> <!--/view brands con -->
     </div>
  </div>  
</div>
<!--view brands con-pg-con ends -->

  </body>
<script src="<?php echo base_url(); ?>content/js/hover.js"></script>

<script type="text/javascript">
    $('#masonry-grid').masonry({
        // options
        itemSelector: '.grid-item',
        percentPosition: true
        //columnWidth: 200
    });
</script>

  
</html>

<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
 <script src="<?php echo base_url(); ?>content/js/flatpickr.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>content/js/flatpickr.min.css"/>
<!--sign up-pg con -->
<!-- (Optional) Latest compiled and minified JavaScript translation files -->
<script type="text/javascript">
		$(function() {
    $( "#skills" ).autocomplete({
        source: '<?php echo base_url(); ?>main/getcity',
		minLength: 2
    });
});
		</script>
       
<div class="signup-pg col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
  <div class="container-fluid">
    <div class="row">

      <div class="signup-pg-con col-lg-10 col-md-10 col-sm-11 col-xs-11 nopadding">
        <div class="tab">
          <div class="tab-cell">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 questions-con nopadding">
           
             <div class="col-lg-7 col-md-7 col-sm-10 col-xs-12 profile-rightbar">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding basic-frm" id="firstform">
                  <div class="col-lg-10 col-md-10 col-sm-11 col-xs-12 content_headline">
                   <h2>Hello <?php echo $reqinvitedata[0]['name']; ?></h2>
                   <h3>Kindly help us to know you little more </h3>
                   
                 
			<!-- Dropdown List Option -->
		
                   <p id="usererror"></p>
                   <div class="upload-img">
                   <?php 
				   if($reqinvitedata[0]['profilepic'] == '')
				   {?>
					   <span class="pf-img" id="output" style="background:url(<?php echo base_url(); ?>content/images/profile/profile_pcture.jpg);">
                      
                       <div class="change-picture" data-toggle="modal" data-target="#change-profile-popup"> <div class="tab"><div class="tab-cell"><span>change profile picture <i class="fa fa-camera"> </i></span></div></div></div> 
                       </span>
				   <?php }else{ ?>
                    <span class="pf-img" id="output" style="background:url(<?php echo base_url().$reqinvitedata[0]['profilepic']; ?>);">
                  
                    <div class="change-picture" data-toggle="modal" data-target="#change-profile-popup"> <div class="tab"><div class="tab-cell"><span>change profile picture <i class="fa fa-camera"> </i></span></div></div></div> 
                    </span>
				  <?php  }?>
                    
                   </div>
                  
                  </div>
                  
                    <ul class="textboxes">
                      <li><input type="text" name="userfname" id="userfname" value="<?php echo $reqinvitedata[0]['name']." ".$reqinvitedata[0]['lname']; ?>" class="signin-textbox" placeholder="Full Name *" required></li>
                     <!-- <li><input type="text" name="userlname" id="userlname" value="<?php //echo $reqinvitedata[0]['lname']; ?>" class="signin-textbox" placeholder="Last Name" required></li>-->
                      <li>
                       
                      <input type="text" name="username123"  id="username123" onBlur="existuser()" value="" class="signin-textbox" placeholder="New Username *" required></li>
                      <li class="passwrdshow dob2"><input type="password" name="password123" id="password123" class="signin-textbox" placeholder="New Password *" required><span class="text">Show</span><input id="test2" type="checkbox" class="hiden-chck"></li>
                      <li><input type="text" name="emailnew" id="emailnew" required value="<?php echo $reqinvitedata[0]['emailid']; ?>" class="signin-textbox" placeholder="Email Address *"></li>
                      <li><input type="text" name="dob"  id="dob"value="<?php echo $reqinvitedata[0]['birthday']; ?>" class="signin-textbox" id="datepicker" placeholder="Date of Birth" class="flatpickr-input active signin-textbox" required></li>
                      <li class="gen-select">
                        <ul class="options-con">
                            <li><input id="radio4" type="radio" name="subtitle" value="male" <?php echo ($reqinvitedata[0]['gender']=='male')?"checked":"" ;?> class="radio1"><label for="radio4">male</label></li>
                            <li><input id="radio5" type="radio" name="subtitle" value="female" <?php echo ($reqinvitedata[0]['gender']=='female')?"checked":"" ;?> class="radio1"><label for="radio5">female</label></li>
                          </ul>
                      </li>
                      <li class="dob">
                       <!--  <select id="country" name="country" style="width:300px;" class="signin-textbox">
                         
                         </select>-->
                         
                         <div class="ui-widget">
    <label for="skills"> </label>
    <input id="skills" value="" class="signin-textbox" placeholder="Select City" required>
</div>
                       
                      </li>
                      <li><input type="text" name="phoneno" id="phoneno" value="" class="signin-textbox" placeholder="contact no."></li>
                      <li>   <select class="signin-textbox" id="Occupation"
                                                                name="Occupation">
                                                            <option value="select occupation">Please select Occupation
                                                            </option>
                                                            <option value="Remunerated"> Remunerated</option>
                                                            <option value="Entrepreneurial"> Entrepreneurial</option>
                                                            <option value="Government/Public Sector ">Government/Public
                                                                Sector
                                                            </option>
                                                            <option value="Multinational Corporations ">Multinational
                                                                Corporations
                                                            </option>
                                                            <option value="Public Limited Corporation  ">Public Limited
                                                                Corporation
                                                            </option>
                                                            <option value="Private Limited Corporation">Private Limited
                                                                Corporation
                                                            </option>
                                                            <option value="Partnership">Partnership</option>
                                                            <option value="Proprietorship">Proprietorship</option>
                                                            <option value="Chartered Accountant">Chartered Accountant
                                                            </option>
                                                            <option value="Engineer">Engineer</option>
                                                            <option value="Architect">Architect</option>
                                                            <option value="Lawyer">Lawyer</option>
                                                            <option value="Doctor">Doctor</option>
                                                            <option value="Trader">Trader</option>
                                                            <option value="Consultant">Consultant</option>
                                                            <option value="Media Houses">Media Houses</option>
                                                        </select></li>
                      <li><input type="text" name="organisation" id="organisation" value="<?php echo $reqinvitedata[0]["company"]; ?>" class="signin-textbox" placeholder="Organisation"></li>
                      <li><input type="text" name="designation" id="designation" value="<?php echo $reqinvitedata[0]["designation"]; ?>" class="signin-textbox" placeholder="Designation"></li>
                    </ul>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding" id="newnext1">
                    <button class="start-btn" id="conbtn-cat">Continue<span><i class="fa fa-angle-right"></i> <i class="fa fa-angle-right"></i> </span></button></div>
                   
             
                </div> <!--/profile first form  -->
                 
                
                 <section id="newques1" class="sectect-cat-frm" style="display:none;">
                  <div class="section-inner">
                        <h3>Kindly select your luxury preferences</h3>
<h4>This will help us offer you with our bespoke services.</h4>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding options-con smart-follow-con"> 
                          <ul class="category-check">
                          <?php
						$sno = 1;
						foreach ($all_cat as $key => $allcategory) {
							//print_r   ($key); 
							if ($key != 6) {
								?>
                          <li class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                               <label for="check<?php echo $sno; ?>">
                                <img src="<?php echo base_url(); ?><?php echo $allcategory['signupcatimage']; ?>" class="img-responsive">
                               <input id="check<?php echo $sno; ?>" type="checkbox" name="cat[]" value="<?php
                                                                    echo $allcategory['categoryId']; ?>" class="check1">
                               <div class="cat-overlay-bg <?php echo $allcategory['category_color']; ?>"><span class='ddd'><?php echo $allcategory['category_Name']; ?></span><span class="activated"><i class="fa fa-check"></i></span></div> </label>
                            </li>
                            
                            <?php } $sno++;  } ?>
                         
                        </ul>
                         	
                      </div> <!--/options-con -->
                      <div class="col-lg-12 nopadding privacy-com-con">
                      <ul>
                        <li> <input type="checkbox" name="" id="signup_newsletter" value="signup"> Subscribe to our newsletter.</li>
                        <li><input type="checkbox" name="policy" id="signup_policy" value="policy"> Policy </li>
                      </ul>
                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
                         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 nopadding" id="back-frm"><button class="start-btn"><span><i class="fa fa-angle-left"></i> <i class="fa fa-angle-left"></i> </span>Back</button></div>
                         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 nopadding" id="finalsubmit"><button class="done-btn2">Done</button></div>
                         </div>
                        

                      </div>
                                            
                  </div>  
                  
                 
                 </section
                 
             ></div> <!--/profile-rightbar -->
           </div>
          </div> <!--/tab cell -->
        </div> <!--/tab -->
      </div> <!--/privacy-pg-con -->
      

    </div>
  </div>  
</div><!--privacy-pg-con ends -->
<div id="change-profile-popup" class="modal fade" role="dialog">
    <div class="tab">
        <div class="tab-cell">
            <div class="modal-dialog">
                <button data-dismiss="modal" class="close custom-close" type="button">×</button>
                <div class="modal-content img-upload-pop">
                    <div class="modal-body">
                      <div class="tab">
                        <div class="tab-cell">
                           <a href="#" class="upload-img-btn"><i class="fa fa-plus"></i>UPLOAD PHOTO<input name="userfile1" id="userfile1"  type="file"  value="UPLOAD PHOTO" onchange="previewImage1();"/></a>
                        </div>
                      </div>
                       
                    </div>
                </div> <!--/modal-content -->
                <div class="modal-content img-uploaded signup-uploaded" style="display: none;">
                    <div class="modal-body nopadding">
                        <div class="caption-con nopadding">
                            <div class="uploaded-prev col-lg-11 col-md-11 col-sm-11 col-xs-11 nopadding">
                                <img src="" id="user_filessignup" class="img-responsive" ></div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 btn-section nopadding">
                                <div class="col-lg-7 col-md-7 col-sm-8 col-xs-6 pull-right nopadding">
                                    <div class="tab"><div class="tab-in">
                                            <img src='<?php echo base_url(); ?>content/images/loader.gif' id='loader_img' style="display:none;" />
                                            <button class="cancel-btn pull-left">Cancel</button>
                                    <span class="post-btn" id="change_image">Ok</span>
                                        </div></div>
                                    
                                    <!-- <button class="post-btn" id="updateprofile_pic">Update </button>-->
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div> <!--/modal-dialog -->
        </div>
    </div>
</div>

<div style="display:none;" id="mychangeimg"></div>
<div id="fbimg" style="display:none;"><?php echo $reqinvitedata[0]['profilepic']; ?></div>
  </body>

<script>
flatpickr("#dob");

 //check user name exist or not
    function existuser() {
        var username = $("#username123").val();
        // var username = document.getElementById("username123");
        var data = {
            "username": username
        };
        $.ajax({
            type: "POST",
            url: "" + base_url + "main/userexist",
            data: data,
            //crossDomain:true,
            success: function (html) {
                //alert(html);
                var inviteuser = html;
                if (inviteuser == 1) {
                    //alert(html);
                    //document.getElementById('usererror').innerHTML = "User Exist !";
                   // $("#usererror").html("user exist");


$("#alert-msg").text("User Exist.");
        $(".alert-sectn").fadeIn();
        setTimeout(function () {
            $('.alert-sectn').fadeOut();
        }, 3000);

                }
                else {
                    $("#usererror").html("");
                }
            }
        });
    }
	
$(function() {
  $( "#datepicker" ).datepicker();
});   
</script>
<script>
$('.smart-follow-banner .cat-overlay-bg').click(function() {
    $('.current').removeClass('current').hide()
        .next().show().addClass('current');
});

//
// var loadFile = function (event) {
//        var output = document.getElementById('output');
//        output.src = URL.createObjectURL(event.target.files[0]);
//    };
//	
$.toggleShowPassword({
    field: '#password123',
    control: '#test2'
});

function previewImage1() {
	
    $("#user_filessignup").show();
   // document.getElementById("user_files").style.display = "block";
    var oFReader = new FileReader();
    oFReader.readAsDataURL(document.getElementById("userfile1").files[0]);
// modal-content img-upload-pop
$(".img-upload-pop").hide();
    $(".img-uploaded").show();

oFReader.onload = function(oFREvent) {
// alert( oFREvent.target.result);
 $("#mychangeimg").html(oFREvent.target.result);

    document.getElementById("user_filessignup").src = oFREvent.target.result;
//  $("#meimg").html(oFREvent.target.result);
};
};


$(document).on('click','#change_image',function(){
	
	var iurl=$("#mychangeimg").html();
	//alert(iurl);
	$('#output').css('background-image','url(' + iurl + ')');
	$("#fbimg").html("");
   // $(".img-upload-pop").show();
    //$("#change-profile-popup").hide();
	 $('#change-profile-popup').modal('hide');

  //  $('#img-upload-pop').modal('hide');

	
	});

$("#back-frm").click(function(e){
    e.preventDefault();
    $(".basic-frm").show();
    $(".sectect-cat-frm").hide();
  });
</script>

 




<!--view people followers con -->
<div class="view-ppl-pg col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
  <div class="container-fluid">
    <div class="row">

      <div class="view-ppl-con col-lg-10 col-md-10 col-sm-11 col-xs-11 nopadding">
          <div class="view-ppl-tag col-lg-12 col-md-12 col-sm-12 col-xs-12">
             <button class="goback-btn pull-left browse-btn"><span class="icomoon icon-slider-left-arrow"></span>back</button><h3 class="
             mg-top">People </h3>
          </div> <!--/search-tagline -->
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding vault-part">
              <div class="vault-blocks col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div id="myTabContent3" class="tab-content">
                  <div class="ppl-vault-con tab-pane active in" id="ppl-vault">
                    <div class="ppl-inner-tab col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
                      <ul class="nav nav-pills col-lg-6 col-md-6 col-sm-8 col-xs-12">
                        <li class="active"><a href="#ppl-followers" data-toggle="tab">followers</a></li>
                        <li ><a href="#ppl-following" data-toggle="tab">following</a></li>
                        <li ><a href="#pending" data-toggle="tab">pending</a></li>
                      </ul>
                    </div> <!--/ppl-inner-tab -->
                    <div class="ppl-folow-blocks nopadding col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <div id="myTabContent4" class="tab-content">
                        <div class="ppl-followers-con tab-pane fade active in" id="ppl-followers">
                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding wall whats_new_content">
                           <?php foreach ($followers as $follower) {
                             # code...
                      ?>
                            <div class="wall-column col-sm-6 col-xs-12">
                              <div class="wall-item">
                                <div class="category">
                                  <div class="profile-result-img"> <img src="<?php echo $follower[0]['profile_picture']; ?>" class="img-responsive">
                                     <div class="brand-result-con"><span><?php echo $follower[0]['firstname']; ?> </span><h6>Founder at Rigalio </h6> <div><button class="follow-profile-btn">follow</button> </div> </div> 
                                  </div>

                                </div> <!--/brand-result content -->

                              </div> <!--/wall-item -->
                            </div> <!--/wall-column -->
                            <?php } ?>

                      
                         
                      
                          </div>   <!--/whats_new_content -->
                        </div> <!--/ppl-followers-con -->
                        <div class="ppl-following-con tab-pane" id="ppl-following">
                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 wall nopadding whats_new_content">
                             
<?php foreach($followings as $following) { ?>
                              <div class="wall-column col-sm-6 col-xs-12">
                                <div class="wall-item">
                                  <div class="category">
                                    <div class="profile-result-img"> <img src="<?php echo $following[0]['profile_picture']; ?>" class="img-responsive">
                                       <div class="brand-result-con"><span><?php echo $following[0]['firstname']; ?> </span><h6>Founder at Rigalio </h6> <div><button class="follow-profile-btn">following</button> </div> </div> 
                                    </div>

                                  </div> <!--/brand-result content -->

                                </div> <!--/wall-item -->
                              </div> <!--/wall-column -->

                              <?php  } ?>

                         
                             
                          </div>   <!--/whats_new_content -->
                        </div> <!--/ppl-following-con -->
                        <div class="pending-con tab-pane" id="pending">
                          <div class="followers-list">
                            <ul class="nopadding">
                              <li> <span class="follower-img"><img src="content/images/user-img.jpg"> </span> <span class="folow-msg">Veda Bashishtha wants to follow you </span> <span class="btn-sectn"><button class="ignore-btn">ignore</button><button class="accept-btn">accept</button> <button class="follow-back-btn" style="display: none;">follow back</button> </span> </li>
                              
                              <li> <span class="follower-img"><img src="content/images/user-img.jpg"> </span> <span class="folow-msg">Veda Bashishtha wants to follow you </span> <span class="btn-sectn"><button class="ignore-btn">ignore</button><button class="accept-btn">accept</button> <button class="follow-back-btn" style="display: none;">follow back</button> </span> </li>
                              
                              <li> <span class="follower-img"><img src="content/images/user-img.jpg"> </span> <span class="folow-msg">Veda Bashishtha wants to follow you </span> <span class="btn-sectn"><button class="ignore-btn">ignore</button><button class="accept-btn active">accept</button> <button class="follow-back-btn" style="display: none;">follow back</button> </span> </li>
                              <li> <span class="follower-img"><img src="content/images/user-img.jpg"> </span> <span class="folow-msg">Veda Bashishtha wants to follow you </span> <span class="btn-sectn"><button class="ignore-btn"  style="display: none;">ignore</button><button class="accept-btn active"  style="display: none;">accept</button> <button class="follow-back-btn active">follow back</button> </span> </li>
                            </ul>
                          </div> <!--/followers-list -->
                        </div> <!--/pending request -->
                      </div>
                    </div> <!--/ppl-folow-blocks -->
                    
                  </div>  <!--/ppl-vault-con -->
                  <div class="brand-vault-con tab-pane fade " id="brand-vault">
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 wall nopadding whats_new_content">
                        <div class="wall-column col-sm-6 col-xs-12">
                          <div class="wall-item">
                            <div class="category">
                              <div class="brand-result-img"> <img src="content/images/search-pg/louis-logo.png" class="img-responsive">
                                 <div class="brand-result-con"><span>Louis Vuitton </span> <div><button class="follow-profile-btn">follow</button> </div> </div> 
                              </div>

                            </div> <!--/brand-result content -->

                          </div> <!--/wall-item -->
                        </div> <!--/wall-column -->
                        <div class="wall-column col-sm-6 col-xs-12">
                          <div class="wall-item">
                            <div class="category">
                              <div class="brand-result-img"> <img src="content/images/search-pg/louis-logo.png" class="img-responsive">
                                 <div class="brand-result-con"><span>Louis Vuitton </span> <div><button class="follow-profile-btn">follow</button> </div> </div> 
                              </div>

                            </div> <!--/brand-result content -->

                          </div> <!--/wall-item -->
                        </div> <!--/wall-column -->
                        <div class="wall-column col-sm-6 col-xs-12">
                          <div class="wall-item">
                            <div class="category">
                              <div class="brand-result-img"> <img src="content/images/search-pg/louis-logo.png" class="img-responsive">
                                 <div class="brand-result-con"><span>Louis Vuitton </span> <div><button class="follow-profile-btn">follow</button> </div> </div> 
                              </div>

                            </div> <!--/brand-result content -->

                          </div> <!--/wall-item -->
                        </div> <!--/wall-column -->
                        <div class="wall-column col-sm-6 col-xs-12">
                          <div class="wall-item">
                            <div class="category">
                              <div class="brand-result-img"> <img src="content/images/search-pg/louis-logo.png" class="img-responsive">
                                 <div class="brand-result-con"><span>Louis Vuitton </span> <div><button class="follow-profile-btn">follow</button> </div> </div> 
                              </div>

                            </div> <!--/brand-result content -->

                          </div> <!--/wall-item -->
                        </div> <!--/wall-column -->
                     </div>   <!--/whats_new_content --> 
                  </div> <!-- /brand-vault-con-->
                  <div class="cat-vault-con tab-pane" id="cat-vault">
                     bkjhkhk
                  </div> <!--/cat-vault-con -->
                </div>
              </div> <!--/vault-blocks -->
            </div> <!--/vault-part -->
      </div> <!--/view brands con -->
     

       
          

    </div>
  </div>  
</div>
<!--view brands con-pg-con ends -->

  </body>

<script>
    // very simple to use!
    $(document).ready(function() {
      $('.js-activated').dropdownHover().dropdown();
    });
</script>

  <script src="content/js/bootstrap.min.js"></script>
  <script src="content/js/bootstrap-hover-dropdown.js"></script>
</html>

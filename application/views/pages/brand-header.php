 <div class="upbrand-bg col-lg-12 nopadding">
            <div class="brand-tab-panel col-lg-10 col-md-10 col-sm-11 col-xs-12">
                <ul class="nav nav-pills">
                    <?php if (($management_details) != '' && ($brand_details[0]['legacy']) != '') { ?>
                        <li class="<?php if($this->uri->segment(4, 0)=="legacy"){  echo "active"; } ?>"><a href="<?php echo base_url()."brand/".str_replace(" ", "-", strtolower($brand_details[0]['brand_Name']))."/".$brand_details[0]['brandId']; ?>/legacy">Legacy</a></li>
                    <?php } ?>
                    <?php if(count($products)!='0') { ?>
                    <li class="<?php if($this->uri->segment(4, 0)=="Products"){ echo "active";  } ?>"><a href="<?php echo base_url()."brand/".str_replace(" ", "-", strtolower($brand_details[0]['brand_Name']))."/".$brand_details[0]['brandId']; ?>/Products"><?php if ($this->uri->segment(3, 0) == "43") {
                                echo "Services";
                            } else {
                                echo "Products";
                            } ?></a></li>
                            <?php } ?>
                    <?php if ($brand_details[0]['upcoming'] != '') { ?>
                        <li><a href="#brand-upcoming" data-toggle="tab">Upcoming</a></li>
                    <?php } ?>
                    <?php if ($brand_details[0]['fb_userid'] != '' || $brand_details[0]['twitter_userid'] != '') { ?>
                    <li class="<?php if($this->uri->segment(4, 0)=="social-feed"){ echo "active"; } ?>"><a href="<?php echo base_url()."brand/".str_replace(" ", "-", strtolower($brand_details[0]['brand_Name']))."/".$brand_details[0]['brandId']; ?>/social-feed">Social Feed</a></li>
   					<?php } ?>
  <?php if(count($brand_news) != '0' ){ ?>
                    <li class="<?php if($this->uri->segment(4, 0)=="latest-news"){ echo "active"; } ?>"><a href="<?php echo base_url()."brand/".str_replace(" ", "-", strtolower($brand_details[0]['brand_Name']))."/".$brand_details[0]['brandId']; ?>/latest-news">Latest News</a></li>
 <?php } ?>
                    <?php if (count($brand_stores) != "0") { 
				
					?>
                        <li style="display:none;"><a href="#store-loc" data-toggle="tab">Store Locator</a></li>
 <?php /*?><li><a href="#" data-toggle="tab">Store Locator</a></li><?php */?>
                    <?php } ?>
                </ul>
</div>
            </div> <!--/category-tab-panel -->

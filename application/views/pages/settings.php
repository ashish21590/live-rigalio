<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width" />
    

    <title>Settings</title>
    <script src="content/js/jquery-1.9.1.min.js"></script>
    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
    <link href="content/css/bootstrap.min.css" rel="stylesheet">
    <link href="content/css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="content/css/header.css" />
    <link href="content/css/category.css" rel="stylesheet">
  
    <!-- Custom styles for this template -->
    <link href="content/css/carousel.css" rel="stylesheet">
    <link href="content/css/master.css" rel="stylesheet">
    <script src="content/js/bootstrap.min.js"></script>
    <script src="content/js/mycustom.js"></script>
    <script src="content/js/classie.js"></script>
  </head>
  

  
<!-- NAVBAR
================================================== -->
  <body>
<nav class="navbar navbar-inverse navbar-fixed-top" id="header_nav">
    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 header-collapse-btn">
      <a href="javascript:void(0)" class="popup-btn"> <img src="content/images/icons/header-collapse-icon.png" class="img-responsive"> </a>
    </div>
    <div class="main-logo col-lg-6 col-md-6 col-sm-6 col-xs-6">
        <a href="javascript:void(0)"> <img src="content/images/main-logo.png" class="img-responsive"> </a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 drop-search-wrap navbar-right">
      <a href="javascript:void(0)" class="search-icon"> <img src="content/images/icons/search-icon.png" class="img-responsive"></a>
    </div>
    <div class="search-hidden col-lg-12 col-md-12 col-sm-12 col-xs-12 dropdown-search " style="display:none;">
            <form id="searchform" role="search" method="get" action="http://themes.birdwp.com/zefir-new/">
              <input type="text" name="s" id="s" class="search-field form-control" placeholder="I am Looking for">
            </form>
    </div>
</nav>
<div class="popup" id="popup">
    <div class="popup-inner">
       <a href="javascript:void(0)" class="close">CLOSE</a>
       <div class="main-menu">
          <ul class="main">
             <li> 
                <div class="">
                  <a href="#" data-delay="1000" data-hover="dropdown" data-toggle="dropdown" class="btn dropdown-toggle" aria-expanded="false">Auto</a>
                  <ul class="dropdown-menu submenu">
                        <li class="nav1-cat1"> <a href="javascript:void(0)"> </a> </li>
                        <li class="nav1-cat2"><a href="javascript:void(0)"> </a> </li>
                        <li class="nav1-cat3"><a href="javascript:void(0)"> </a> </li>
                        <li class="nav1-cat4"><a href="javascript:void(0)"> </a> </li>
                  </ul>
                </div>
             </li>
             <li> 
                <div class="">
                  <a href="#" data-delay="1000" data-hover="dropdown" data-toggle="dropdown" class="btn dropdown-toggle" aria-expanded="false">Fashion and Lifestyle</a>
                  <ul class="dropdown-menu submenu">
                        <li class="nav1-cat1"> <a href="javascript:void(0)"> </a> </li>
                        <li class="nav1-cat2"><a href="javascript:void(0)"> </a> </li>
                        <li class="nav1-cat3"><a href="javascript:void(0)"> </a> </li>
                        <li class="nav1-cat4"><a href="javascript:void(0)"> </a> </li>
                  </ul>
                </div>
             </li>
             <li> 
                <div class="">
                  <a href="#" data-delay="1000" data-hover="dropdown" data-toggle="dropdown" class="btn dropdown-toggle" aria-expanded="false">Real Estate </a>
                  <ul class="dropdown-menu submenu">
                        <li class="nav1-cat1"> <a href="javascript:void(0)"> </a> </li>
                        <li class="nav1-cat2"><a href="javascript:void(0)"> </a> </li>
                        <li class="nav1-cat3"><a href="javascript:void(0)"> </a> </li>
                        <li class="nav1-cat4"><a href="javascript:void(0)"> </a> </li>
                  </ul>
                </div>
             </li>
             <li> 
                <div class="">
                  <a href="#" data-delay="1000" data-hover="dropdown" data-toggle="dropdown" class="btn dropdown-toggle" aria-expanded="false">Tarvel and Hotels</a>
                  <ul class="dropdown-menu submenu">
                        <li class="nav1-cat1"> <a href="javascript:void(0)"> </a> </li>
                        <li class="nav1-cat2"><a href="javascript:void(0)"> </a> </li>
                        <li class="nav1-cat3"><a href="javascript:void(0)"> </a> </li>
                        <li class="nav1-cat4"><a href="javascript:void(0)"> </a> </li>
                  </ul>
                </div>
             </li>
             <li> 
                <div class="">
                  <a href="#" data-delay="1000" data-hover="dropdown" data-toggle="dropdown" class="btn dropdown-toggle" aria-expanded="false">Events</a>
                  <ul class="dropdown-menu submenu">
                        <li class="nav1-cat1"> <a href="javascript:void(0)"> </a> </li>
                        <li class="nav1-cat2"><a href="javascript:void(0)"> </a> </li>
                        <li class="nav1-cat3"><a href="javascript:void(0)"> </a> </li>
                        <li class="nav1-cat4"><a href="javascript:void(0)"> </a> </li>
                  </ul>
                </div>
             </li>
          </ul>
            
            <div class="main-social-sectn">
                <ul>
                   <li> <a href="javascript:void(0)"> <img src="content/images/social-icons/facebook.png" class="img-responsive"> </a> </li>
                   <li> <a href="javascript:void(0)"> <img src="content/images/social-icons/facebook.png" class="img-responsive"> </a> </li>
                   <li> <a href="javascript:void(0)"> <img src="content/images/social-icons/facebook.png" class="img-responsive"> </a> </li>
                   <li> <a href="javascript:void(0)"> <img src="content/images/social-icons/facebook.png" class="img-responsive"> </a> </li>
                </ul>
            </div> <!--/main-social-sectn" -->
        </div> <!-- /main-menu -->
    </div><!-- popup -inner-->
</div>  <!--/header popup -->     

<div class="settings-tagline">
    <div class="col-lg-10 col-md-10 col-sm-11 col-xs-11 nopadding settings-info"> <img src="content/images/profile/profile_pcture.jpg" class="img-responsive"> <span class="user-name">Devendra Batra </span> </div>
</div>  <!--/settings-tagline -->
<!--settings page con -->
<div class="settings-pg col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding nomargin">
  <div class="container-fluid">
    <div class="row">
      <div class="settings-pg-con col-lg-6 col-md-6 col-sm-10 col-xs-12 nopadding">
        <div class="settings-tab-panel">
          <ul class="nav nav-pills">
            <li class="active"> <a href="#details" data-toggle="tab">details </a> </li>
            <li> <a href="#communication" data-toggle="tab"> communication</a> </li>
            <li> <a href="#privacy" data-toggle="tab">privacy </a> </li>
          </ul>
        </div> <!--/settings-tab-panel -->
        <div class="settings-blocks col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
         <div id="myTabContent2" class="tab-content">    
            <div class="details-tab-con tab-pane fade active in" id="details">
              <ul class="nopadding">
                <li> <label>Password</label> <button class="edit">edit </button> 
                   <div class="password-info">
                     <ul class="nopadding">
                       <li><input type="text" name="" value="" placeholder="CURRENT PASSWORD" class="textboxd"></li>
                      
                       <li class="passwrdshow"><input type="password" name="" placeholder="NEW PASSWORD" id="test1" value="" class="textboxd " /><span class="text">Show</span><input id="test2" type="checkbox" class="hiden-chck" />  </li>
                     </ul>
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding"><button class="save-btn">save</button></div>
                   </div> <!--/Password-info -->
                </li>
                <li> <label>Email address</label> <button class="edit">edit </button> 
                  <div class="email-settings">
                  	<h5>We already have your following email addresses </h5>
                  	<ul class="nopadding">
                  	  <li><label>abhishek@outlook.com </label> <button class="primary">primary</button> <button class="remove-btn">Remove</button> </li>
                  	  <li><label>abhishek@outlook.com </label> <button class="make-primary"> make primary</button> <button class="remove-btn">Remove</button> </li>
                  	</ul>
                  	<h4 class="add-email-click"> Add new email address</h4>
                  	<div class="add-email">
                  	  <input type="text" name="" value="" placeholder="NEW EMAIL ADDRESS" class="textboxd"><button class="verify-btn">verify</button>
                  	  <p class="verify-msg" style="display: none;">Check your email. New email address is updated </p>
                  	  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding"><button class="save-btn">save</button></div>
                  	</div>

                  </div> <!--/email-settings -->
                </li>
                <li> <label>date of birth</label> <button class="edit">edit </button> 
                   <div class="dob-info">
                     <ul class="nopadding">
                       <li><input type="text" name="" value="" placeholder="14/03/1992" class="textboxd"></li>
                       <li><input type="text" name="" value="" placeholder="ENTER NEW DATE" id="datepicker2" class="textboxd"></li>
                     </ul>
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding"><button class="save-btn">save</button></div>
                   </div> <!--/dob-info -->
                </li>
                <li> <label>Phone number</label> <button class="edit">edit </button>
                  <div class="phone-info">
                     <h5>We already have your following phone numbers </h5>
                  	<ul class="nopadding">
                  	  <li><label>+91 9999999999 </label> <button class="primary">primary</button> <button class="remove-btn">Remove</button> </li>
                  	  <li><label>+91 9999999999 </label> <button class="make-primary"> make primary</button> <button class="remove-btn">Remove</button> </li>
                  	</ul>
                  	<h4 class="add-ph-click"> Add new Phone no.</h4>
                  	<div class="add-ph">
                  	  <input type="text" name="" value="" placeholder="NEW PHONE NO." class="textboxd"><button class="verify-btn">verify</button>
                  	  <p class="verify-ph" style="display: none;">Check your phone. New phone no. is updated </p>
                  	  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding"><button class="save-btn">save</button></div>
                  </div> <!--/phone-info --> 
                </li>
              </ul>
            </div> <!--/details-tab-con -->
            <div class="comm-tab-con tab-pane" id="communication">
              <ul>
                <li> <label>Frequency of information</label><button class="edit">edit </button> 
                  <div class="freq-info">
                    <ul class="nopadding">
		                <li><input id="radio1" type="radio" name="radio1" value="Item 1" class="radio1"><label for="radio1">Weekly</label></li>
		                <li><input id="radio2" type="radio" name="radio2" value="Item 2" class="radio1"><label for="radio2">Fortnightly</label></li>
		                <li><input id="radio3" type="radio" name="radio3" value="Item 3" class="radio1"><label for="radio1">Monthly</label></li>
		            </ul>
		            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding"><button class="save-btn">save</button> <span class="alert-msg">You can choose any one of the option </span> </div>
                  </div> <!--/freq-info-inner -->
                </li>
                <li> <label>mode of communication </label> <button class="edit">edit </button> 
                   <div class="mode-comm">
                    <ul class="nopadding">
		                <li><input id="check8" type="checkbox" name="check8" value="Item 1" class="check1"><label for="check8">Send me information via text messages </label></li>
                 	    <li><input id="check9" type="checkbox" name="check9" value="Item 2" class="check1"><label for="check9">Call me to update </label></li>
                 	    <li><input id="check10" type="checkbox" name="check10" value="Item 3" class="check1"><label for="check10">E-mail me the information </label></li>
		            </ul>
		            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding"><button class="save-btn">save</button> <span class="alert-msg">You can choose any one of the option </span> </div>
                  </div> <!--/mode-comm-inner -->
                </li>
              </ul>
            </div> <!--/communication-tab-con -->
            <div class="privacy-tab-con tab-pane" id="privacy">
              <ul>
                <li> <label>profile visibility</label> <button class="edit">edit </button> 
                  <div class="profile-visibile">
                     <h5>Who can see your profile on Rigalio </h5>
                     <ul class="nopadding">
		                <li><input id="radio1" type="radio" name="radio1" value="Item 1" class="radio1"><label for="radio1">private</label></li>
		                <li><input id="radio2" type="radio" name="radio2" value="Item 2" class="radio1"><label for="radio2">members</label></li>
		             </ul>
                   </div> <!--/profile-visibile -->
                </li>
                <li> <label>crowns</label> <button class="edit">edit </button>
                   <div class="crown-info">
                     <h5>Who can see your crowns on Rigalio </h5>
                     <ul class="nopadding">
		                <li><input id="radio1" type="radio" name="radio1" value="Item 1" class="radio1"><label for="radio1">Only me</label></li>
		                <li><input id="radio2" type="radio" name="radio2" value="Item 2" class="radio1"><label for="radio2">Followers</label></li>
		                <li><input id="radio3" type="radio" name="radio3" value="Item 3" class="radio1"><label for="radio1">Members</label></li>
		             </ul>
                   </div>
                </li>
                <li> <label>comments</label> <button class="edit">edit </button> 
                   <div class="comments-setting">
                     <h5>Who can see your comments on Rigalio </h5>
                     <ul class="nopadding">
		                <li><input id="radio1" type="radio" name="radio1" value="Item 1" class="radio1"><label for="radio1">Only me</label></li>
		                <li><input id="radio2" type="radio" name="radio2" value="Item 2" class="radio1"><label for="radio2">Followers</label></li>
		                <li><input id="radio3" type="radio" name="radio3" value="Item 3" class="radio1"><label for="radio1">Members</label></li>
		             </ul>
                   </div> <!-- /comments-setting-->
                </li>
                <li> <label>share</label> <button class="edit">edit </button>
                  <div class="share-setting">
                     <h5>Who can see your share on Rigalio </h5>
                     <ul class="nopadding">
		                <li><input id="radio1" type="radio" name="radio1" value="Item 1" class="radio1"><label for="radio1">Only me</label></li>
		                <li><input id="radio2" type="radio" name="radio2" value="Item 2" class="radio1"><label for="radio2">Followers</label></li>
		                <li><input id="radio3" type="radio" name="radio3" value="Item 3" class="radio1"><label for="radio1">Members</label></li>
		             </ul>
                   </div> <!-- /share-setting--> 
                </li>
                <li> <label>vault</label> <button class="edit">edit </button>
                   <div class="vault-setting">
                     <h5>Who can see your list of followers and following </h5>
                     <ul class="nopadding follower-setting">
		                <li><input id="radio1" type="radio" name="radio1" value="Item 1" class="radio1"><label for="radio1">Only me</label></li>
		                <li><input id="radio2" type="radio" name="radio2" value="Item 2" class="radio1"><label for="radio2">Followers</label></li>
		                <li><input id="radio3" type="radio" name="radio3" value="Item 3" class="radio1"><label for="radio1">Members</label></li>
		             </ul>
		             <h5>Who can see your list of brands you follow </h5>
                     <ul class="nopadding brand-setting">
		                <li><input id="radio1" type="radio" name="radio1" value="Item 1" class="radio1"><label for="radio1">Only me</label></li>
		                <li><input id="radio2" type="radio" name="radio2" value="Item 2" class="radio1"><label for="radio2">Followers</label></li>
		                <li><input id="radio3" type="radio" name="radio3" value="Item 3" class="radio1"><label for="radio1">Members</label></li>
		             </ul>
		             <h5>Who can see your list of categories you follow </h5>
                     <ul class="nopadding cat-setting">
		                <li><input id="radio1" type="radio" name="radio1" value="Item 1" class="radio1"><label for="radio1">Only me</label></li>
		                <li><input id="radio2" type="radio" name="radio2" value="Item 2" class="radio1"><label for="radio2">Followers</label></li>
		                <li><input id="radio3" type="radio" name="radio3" value="Item 3" class="radio1"><label for="radio1">Members</label></li>
		             </ul>
                   </div> <!--/vault-setting -->
                </li>
                <li> <label>status</label> <button class="edit">edit </button>
                  <div class="status-setting">
                     <h5>Who can see your status on Rigalio </h5>
                     <ul class="nopadding">
		                <li><input id="radio1" type="radio" name="radio1" value="Item 1" class="radio1"><label for="radio1">Only me</label></li>
		                <li><input id="radio2" type="radio" name="radio2" value="Item 2" class="radio1"><label for="radio2">Followers</label></li>
		                <li><input id="radio3" type="radio" name="radio3" value="Item 3" class="radio1"><label for="radio1">Members</label></li>
		             </ul>
                   </div> <!-- /status-setting-->  
                </li>
                <li> <label>photos</label><button class="edit">edit </button> 
                  <div class="photos-setting">
                     <h5>Who can see your photos on Rigalio </h5>
                     <ul class="nopadding">
		                <li><input id="radio1" type="radio" name="radio1" value="Item 1" class="radio1"><label for="radio1">Only me</label></li>
		                <li><input id="radio2" type="radio" name="radio2" value="Item 2" class="radio1"><label for="radio2">Followers</label></li>
		                <li><input id="radio3" type="radio" name="radio3" value="Item 3" class="radio1"><label for="radio1">Members</label></li>
		             </ul>
                   </div> <!-- /photos-setting-->
                </li>
              </ul>
            </div> <!--/privacy-tab-con -->
         </div>   
        </div> <!--/settings-blocks -->
          
      </div> <!--/settings-pg-con -->
    </div>
  </div>  
</div>
<!--settings-pg-con ends -->

  </body>
<script>
$(function() {
  $( "#datepicker2" ).datepicker();
});   
</script>
<script>
$.toggleShowPassword({
    field: '#test1',
    control: '#test2'
});
</script>
<link rel="stylesheet" href="content/css/jquery-ui.css">
<script src="content/js/jquery-ui.js"></script>
  <script src="content/js/bootstrap.min.js"></script>
<script>
// script for the settings page
$(function() {
    $('.settings-blocks .edit').click(function() {
      $(this).parent().children("div").slideToggle("slow");
    });
});    
// script to show password on sign up page
//Place this plugin snippet into another file in your applicationb
(function ($) {
    $.toggleShowPassword = function (options) {
        var settings = $.extend({
            field: "#password",
            control: "#toggle_show_password",
        }, options);

        var control = $(settings.control);
        var field = $(settings.field)

        control.bind('click', function () {
            if (control.is(':checked')) {
                field.attr('type', 'text');
            } else {
                field.attr('type', 'password');
            }
        })
    };
}(jQuery));
// script for the adding new email address on settings page
$(function() {
    $('.add-email-click, .add-ph-click').click(function() {
      $('.add-email, .add-ph').slideToggle("slow");
    });
});	
</script>

</html>

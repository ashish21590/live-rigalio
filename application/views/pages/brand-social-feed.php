<div class="brand-pg-banner col-lg-12 nopadding">
  <span class="brand-bg-banner" style="background:url(<?php echo base_url().$brand_details[0]['cover_image']; ?>);"> </span>
  
  <div class="col-lg-10 col-sm-11 col-xs-12 brand-divison nopadding">
    <div class="brand-img">
     <span style="background:url(<?php echo base_url() . $brand_details[0]['brand_image']; ?>) no-repeat top center;"> </span>
    </div>
  </div>
  
</div>  


<div class="brand_pg_tagline">
  <div class="col-lg-10 col-md-10 col-sm-11 col-xs-12 brand-logo-sec">
     <div class="col-lg-6 col-sm-12 nopadding">
       <span class="brand-intro"><?php echo $brand_details[0]['brand_Name']; ?></span>
     </div>
     <div class="col-lg-6 col-sm-12 nopadding inner-up">
       <span class="followers"><img src="<?php echo base_url(); ?>content/images/icons/followers-icon.png"> 
<i id="getcount"><?php foreach($this->getdata->count_brandfollower($brand_details[0]['brandId']) as $count_no){
					if($count_no['no'] != ''){
					 echo $count_no['no']; 
					}
					else
					{
						echo '0';
					}
					 }?> </i></span>
       <span class="brand-follow up-brand" id="<?php echo $brand_details[0]['brandId'];?>">
                    <?php
															$me = [];
															foreach($user_follow as $user_foll){
																
																	$me[] = $user_foll['brand_id'];
															}
															$flag=1;
															for($i=0;$i<count($me);$i++){
															if($brand_details[0]['brandId'] ==$me[$i]){
																$flag=0;
																break;
															}
															}
															if($flag==0)
															{?>
                                                            <button class="follow-brand-btn following" data-text = "Follow brand"><i class="fa fa-check"></i></button>
                                                            <?php }
															else { ?>
                    <button class="follow-brand-btn follow" data-text="Follow brand"><i class="fa fa-check"></i></button>
     				<?php } ?>
      </span>
     </div>
     
  </div>      
</div> <!--/brand_pg_tagline -->

<!--category-pg-con -->
<div class="brand-pg-con col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
    <div class="container-fluid nomargin nopadding">
        <div class="row">
		 <?php include('brand-header.php'); ?>
           

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 social-feed-con">
          
                <div class="col-lg-10 col-md-10 col-sm-11 col-xs-12 wall whats_new_content12" id="feed">
                    <div class="grid" id="masonry-grid1">


                    </div>
                </div>
                <!--/whats_new_content -->
            </div><!--/social-feed-con tab content ends -->

</div>
</div>
        </div> <!--/tab-content -->
        <div class="product-share-sectn brand-pg col-lg-7 col-md-7 col-sm-11 col-xs-12">
            <h5>Share This </h5>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
                <div class="product-inner-social col-lg-5 col-md-5 col-sm-12 col-xs-12 nopadding">
                    <ul class="follow-icons">
                        <li><a target="_blank" class="facebook-follow"
                               href="https://www.facebook.com/sharer/sharer.php?u=<?php echo base_url(); ?>brand/<?php echo str_replace(" ", "-", strtolower($brand_details[0]['brand_Name'])); ?>/<?php echo $brand_details[0]['brandId']; ?>"></a>
                        </li>
                        <li><a target="_blank" class="twitter-follow"
                               href="https://twitter.com/home?status=<?php echo base_url(); ?>brand/<?php echo str_replace(" ", "-", strtolower($brand_details[0]['brand_Name'])); ?>/<?php echo $brand_details[0]['brandId']; ?>"></a>
                        </li>
                        <?php /*?> <li><a class="instagram-follow" href="" target="_blank"></a></li><?php */ ?>
                        <li><a target="_blank" class="linkedin-follow"
                               href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo base_url(); ?>brand/<?php echo str_replace(" ", "_", strtolower($brand_details[0]['brand_Name'])); ?>/<?php echo $brand_details[0]['brandId']; ?>&title=Rigalio&summary=Rigalio%20content&source=<?php echo base_url(); ?>brand/<?php echo str_replace(" ", "_", strtolower($brand_details[0]['brand_Name'])); ?>/<?php echo $brand_details[0]['brandId']; ?>"></a>
                        </li>
                        <li><a target="_blank" class="rss-follow"
                               href="https://plus.google.com/share?url=<?php echo base_url(); ?>brand/<?php echo str_replace(" ", "-", strtolower($brand_details[0]['brand_Name'])); ?>/<?php echo $brand_details[0]['brandId']; ?>"></a>
                        </li>
                        <li><a target="_blank" class="pin-follow"
                                   href="https://pinterest.com/pin/create/button/?url=Testing&media=<?php echo base_url(); ?>brand/<?php echo str_replace(" ", "-", strtolower($brand_details[0]['brand_Name'])); ?>/<?php echo $brand_details[0]['brandId']; ?>"></a>
                            </li>
                    </ul>
                </div>
                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 nopadding btn-sectn"><a target="_blank"
                        href="<?php echo $brand_details[0]['official_link']; ?>">
                        <button class="goto-web-btn">Go to official website</button>
                    </a>
                    <button class="query-btn" id="btn-sendform">Send a query</button>
                </div>

            </div>
        </div> <!-- /product-share-sectn -->
        <div class="sendquery-form" style="display: none;">
            <form class="col-lg-6 col-md-7 col-sm-10 col-xs-12" name="sendaquery" method="post">
                <label>Send a query</label>
                <p id="msgbrand"></p>
                <div id="myhide1" class="myhide1">
                 <?php
				  $regif = $this->session->userdata('registrationid');
				  if ($regif == '') {
					  ?>
                    <input type="text" name="name" id="namebrand" value="" placeholder="NAME" class="textbox textbox2">
                    <input type="text" name="contactno" id="contactnobrand" value="" placeholder="CONTACT NO."
                           class="textbox textbox2">
                    <input type="text" name="emailid" id="emailidbrand" value="" placeholder="EMAIL ID" class="textbox">
                    <textarea class="textbox" id="querybrand" name="query" placeholder="QUERY" rows="5"> </textarea>
                    <input type="text" name="formname" id="formnamebrand" value="Send A Query" style="display:none;">
                    <?php } else{?>  
                    <input type="text" name="name" id="namebrand" value="<?php echo $userdata[0]['firstname'] ?> <?php echo $userdata[0]['lastname'] ?>" placeholder="NAME" class="textbox textbox2">
                    <?php foreach($this->getdata->user_detaildata($regif) as $useralldata){ 
					 }
					 if($useralldata['phone'] == ''){?>
                    <input type="text" name="contactno" id="contactnobrand" value="" placeholder="CONTACT NO."
                           class="textbox textbox2">
                           <?php } else{ ?>
                           <input type="text" name="contactno" id="contactnobrand" value="<?php echo $useralldata['phone'] ?>" placeholder="CONTACT NO."
                           class="textbox textbox2">
                           <?php } ?>
                    <input type="text" name="emailid" id="emailidbrand" value="<?php echo $userdata[0]['email'] ?>" placeholder="EMAIL ID" class="textbox">
                    <textarea class="textbox" id="querybrand" name="query" placeholder="QUERY" rows="5"> </textarea>
                    <input type="text" name="formname" id="formnamebrand" value="Send A Query" style="display:none;">
                   <?php  }?>
                    <button name="submit" class="try-now-submit1" type="button">SUBMIT</button>
                </div>
            </form>
        <div class="close-btn"> <img src="<?php echo base_url(); ?>content/images/icons/close-btn-gold.png" class="img-responsive"> </div>
        </div> <!--/try-now-con -->

    </div>
</div>
</div>
<!--barnd pg-con ends -->

<footer>
    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 footer-inner">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <p>All rights reserved. All content belongs to respective owners.</p>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <ul>
                <li><a href="<?php echo base_url(); ?>main/aboutus">About us </a></li>
                <li><a href="<?php echo base_url(); ?>main/contactus">Contact us</a></li>
                <li><a href="<?php echo base_url(); ?>main/privacy">Privacy </a></li>
                 <li><a href="<?php echo base_url(); ?>main/faq">FAQ </a></li>
            </ul>
        </div>
    </div>
</footer>
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?php echo base_url(); ?>content/js/jaliswall.js"></script>

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>googlemaps/stylegoogle.css"/>
<script type="text/javascript"src="http://maps.googleapis.com/maps/api/js?sensor=false&key=AIzaSyD0X4v7eqMFcWCR-VZAJwEMfb47id9IZao">
</script>

<script type="text/javascript">






    $(window).load(function () {

  onsload();

  $("#feed").append("<img src=\"<?php echo base_url(); ?>loader.gif\"/>");
  $( "#feed" ).addClass( "abvcu" );

     


        var data = {
            "count": 25,
            "fb_userid": '<?php echo $brand_details[0]['fb_userid']; ?>',
            "twitter_userid": '<?php echo $brand_details[0]['twitter_userid']; ?>'
        };

        $.ajax({
            type: "GET",
            url: "http://www.rigalio.com/x/",

            //dataType: 'json',
            data: data,
            success: function (html) {

              $(".whats_new_content12").html(html);
$('.whats_new_content12').jaliswall({item: '.wall-item'});
$( "#feed" ).removeClass( "abvcu" );
                //correct();

                


                // alert(html);
                // $("#masonry-grid1").html(html);
                // alert(html);
                //$("#"+id).next().css( "background", "yellow" );
                //alert(html);
            }

        });  //ajax ends here


    });




function myurl(url){
//alert(url);

    window.location.href=url;

}


</script>

<style>
.content_headline2{
display:none;

}
</style>

<script src="<?php echo base_url(); ?>content/js/hover.js"></script>

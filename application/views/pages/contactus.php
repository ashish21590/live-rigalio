<!--contact us pg con -->
<div class="contact-pg col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
  <div class="container-fluid">
    <div class="row">

      <div class="contact-pg-in col-lg-9 col-md-10 col-sm-11 col-xs-11 nopadding">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
           <div class="col-lg-10 col-md-10 col-sm-11 col-xs-12 content_headline">
             <h4>Welcome to the <br> Customer Connect Service</h4>
             <hr class="grey">
           </div>
           <div class="contact-form col-lg-6 col-sm-12 nopadding">
             <div class="contact-us-con col-lg-10 col-md-10 col-sm-11 col-xs-11">
               <form class="col-lg-12" name="contactform" method="post">
                 <h3>Drop us a mail</h3>
		  <p id="msgctact"> </p>
		  <div id="myhide2" class="myhide2"> 
                 <input type="text" name="name" id="namectact" value="<?php 
                  if($this->session->userdata('registrationid')) { echo $userdata[0]['firstname']." ".$userdata[0]['lastname']; } ?>" placeholder="NAME" class="textbox textbox2">
                 <input type="text" name="contactno" id="contactnoctact" value="<?php 

 $regif = $this->session->userdata('registrationid');
                        foreach($this->getdata->user_detaildata($regif) as $useralldata){ 
                     

                          if($useralldata['phone']){

                            echo $useralldata['phone'];
                          }
                     } ?>" placeholder="CONTACT NO." class="textbox textbox2">
                 <input type="text" name="emailid" id="emailidctact" value="<?php if($this->session->userdata('registrationid')) { echo $userdata[0]['email']; } ?>" placeholder="EMAIL ID" class="textbox">
                 <textarea name="Query" rows="5" class="textbox" id="queryctact" placeholder="Query..."></textarea>
 <input type="text" name="formname" id="formnamectact" value="Contact Us" style="display:none;">
                 <button name="submit" class="contact-submit" type="button">SUBMIT</button>
               </div>
               </form>
               
             </div>
           </div> <!--/contact-form -->
           <div class="contact-pg-con col-lg-6 col-sm-12 nopadding">
              <div class="col-lg-10 col-md-10 col-sm-11 col-xs-12 assistance-con">
                   <div class="col-lg-12">
                      <div class="assistance-img"> </div>
                      <h3> Frequently asked questions</h3>
                      <p>For instant answers to frequent enquiries about Rigalio.com and your online luxury experience, please browse our <b> FAQ's</b> section. </p>
                   </div>
                   <div class="col-lg-12">
                      <div class="assistance-img"> </div>
                      <h3> SERVICE ASSISTANCE</h3>
                      <p>Please note that we are only able to provide assistance for issues related to the online product display and are not able to answer questions concerning retailers or any other service offered by Rigalio. </p>
                   </div>
                   <div class="col-lg-12">
                      <div class="assistance-img"></div>
                      <h3>PRODUCT ASSISTANCE</h3>
                      <P>For assistance related to watches please contact the nearest Service Centre.  </P>
                   </div>
                 
              </div>
            </div> <!--/contact-pg-con -->
        </div>
         
      </div> <!--/contact-pg-in -->
      
    </div>
  </div>  
</div><!--privacy-pg-con ends -->
<footer>
  <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 footer-inner">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
      <p>All rights reserved. All content belongs to respective owners. </p>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
       <ul>
         <li><a href="<?php echo base_url();?>main/aboutus">About us </a> </li>
         <li><a href="<?php echo base_url();?>main/contactus">Contact us</a> </li>
         <li><a href="<?php echo base_url();?>main/privacy">Privacy </a> </li>
          <li><a href="<?php echo base_url(); ?>main/faq">FAQ </a></li>
       </ul>
    </div>
  </div>
</footer>

<script>

 $(document).on('keydown', "#queryctact", function (e) {
        // $(".textbox-sign").keypress(function(e){
        if (e.which == 13) {//Enter key pressed
            $('.contact-submit').click();
            // alert("hi");
        }
    });
</script>

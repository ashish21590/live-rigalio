<!--privacy-pg con -->
<div class="privacy-pg col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
  <div class="container-fluid">
    <div class="row">

      <div class="privacy-pg-con col-lg-10 col-md-10 col-sm-11 col-xs-11 nopadding">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
           <div class="col-lg-10 col-md-10 col-sm-11 col-xs-12 content_headline">
             <h2>Terms and Conditions </h2> <hr>
           </div>
           
           <div class="privacy-text">
             <p>Welcome to the <a href="<?php echo base_url(); ?>"><b>Rigalio.com </b> </a> website or mobile properties, including related applications (collectively, this "Website"). </p>
           </div> <!-- /privacy-text -->
           <div class="privacy-text">
             <p>The website is a luxury portal that catalogues luxury products from diverse categories that have been stated on the website. The website offers an overview of the various listed products, their specifications as well as connects users to brands and their respective showrooms. Rigalio is not an e-commerce website. Apart from giving users uninterrupted access to luxury, Rigalio acts as a facilitator between users and luxury brands, and facilitates enquiries between parties. Being and ‘Invite-Only’ website it also maintains to preserve exclusivity. Members can register by filling their Invitation requests and once granted access, sign in using their dedicated usernames and passwords. They can browse products, comment, share and crown the items they are interested with. Further, they can also connect with fellow members on the website and follow them. Rigalio also offers luxury news and traces other happenings in the world of luxury. </p>
           </div> <!-- /privacy-text -->
           <div class="privacy-text">
             <p>This website is published by or on behalf of ­­­­­­<b>Rigalio E-Luxury Private Limited, N1/003, The Close (North), Unitech Nirvana Country, South City-2, Gurgaon-122018</b> </p> 
           </div>
           <div class="privacy-text">
             <p>The term <b>"you" </b> refers to the customer visiting the Website and/or contributing content on this Website.</p>
             <p>This Website is offered to you conditioned upon your acceptance without modification of any/all the terms, conditions, and notices set forth below <b> (collectively, the "Agreement") </b>. By accessing or using this Website in any manner, you agree to be bound by the Agreement and represent that you have read and understood its terms. Please read the Agreement carefully as it contains information concerning your legal rights and limitations on these rights, as well as a section regarding applicable law and jurisdiction of disputes. If you do not accept all of these terms and conditions you are not authorized to use this Website.</p>
             <p>We may change or otherwise modify the Agreement or similar terms of use in the future in accordance with the Terms and Conditions herein, and you understand and agree that your continued access or use of this Website after such change signifies your acceptance of the updated or modified Agreement. We will note the date that revisions were last made to the Agreement at the bottom of this page, and any revisions will take effect upon posting. We will notify our members of material changes to these terms and conditions by either sending a notice to the email address provided to us at registration or by placing a notice on our Website. Be sure to return to this page periodically to review the most current version of the Agreement. </p>
            
           </div> <!-- /privacy-text -->
           <div class="col-lg-10 col-md-10 col-sm-11 col-xs-12 content_headline">
              <h4>Use of the Website: </h4> <hr>
           </div>
           <div class="privacy-text">
              <p>As a condition of your use of this Website, you warrant that (i) all information supplied by you on this Website is true, accurate, current and complete, (ii) if you have a Rigalio account, you will safeguard your account information and will supervise and be completely responsible for any use of your account by anyone other than you, (iii) you are 18 years of age or older in order to register for an account and contribute to our Website and (iv) you possess the legal authority to enter into this Agreement under the applicable laws and to use this Website in accordance with all terms and conditions herein. </p>
              <p>Rigalio does not knowingly collect the information of anyone under the age of 18 and anyone under the age of 18 is prohibited from using the Website. We retain the right at our sole discretion to deny access to anyone to this Website and the services we offer, at any time and for any reason, including, but not limited to, for violation of this Agreement. </p>
              <p>Copying, transmission, reproduction, replication, posting or redistribution of the Website Content or any portion thereof is strictly prohibited without the prior written permission of Rigalio. To request permission, you may contact Rigalio as follows: </p>
              <p> <b>Chief Executive Officer </b> <br> <b>Rigalio E-Luxury Private Limited.  </b> <br>
               <b>N1/003, The Close (North), Unitech Nirvana Country, </b> <br> <b>South City-2, Gurgaon-122018 </b> </p>
               <p> Users of the Website will not incur any charges for using the Website in accordance with these Terms and Conditions. However, the Website contains links to third-party websites which are operated and owned by independent service providers or retailers. Rigalio in no way guarantees that the information provided by third party websites is correct nor is it in anyway responsible for content or services provided on such third party websites.</p>
           </div>
           <div class="col-lg-10 col-md-10 col-sm-11 col-xs-12 content_headline">
               <h4>Prohibited Activities: </h4> <hr>
           </div>
           <div class="privacy-text">
              <p>The content and information on this Website (including, but not limited to, messages, data, information, text, music, sound, photos, graphics, video, maps, icons, software, code or other material), as well as the infrastructure used to provide such content and information, is proprietary to us. You agree not to otherwise modify, copy, distribute, transmit, display, perform, reproduce, publish, license, create derivative works from, transfer, or sell or re-sell any information, software, products, or services obtained from or through this Website. Additionally, you agree not to: </p>
              <ol>
                <li>Use this Website or its contents for any commercial purpose. </li>
                <li>Access, monitor or copy any content or information of this Website using any robot, spider, scraper or other automated means or any manual process for any purpose without our express written permission. </li>
                <li>Violate the restrictions in any robot exclusion headers on this Website or bypass or circumvent other measures employed to prevent or limit access to this Website.</li>
                <li>Take any action that imposes, or may impose, in our discretion, an unreasonable or disproportionately large load on our infrastructure. </li>
                <li>Deep-link to any portion of this Website for any purpose without our express written permission. </li>
                <li> "Frame", "mirror" or otherwise incorporate any part of this Website into any other website without our prior written authorization. or</li>
                <li>Attempt to modify, translate, adapt, edit, decompile, disassemble, or reverse engineer any software programs used by Rigalio in connection with the Website or the services. </li>
              </ol>
           </div>
           <div class="col-lg-10 col-md-10 col-sm-11 col-xs-12 content_headline">
               <h4>FEATURES AND CONDITIONS OF THE PERSONAL DATA PROCESSING  </h4> <hr>
           </div>
           <div class="privacy-text">
             <p>Personal data collected by Rigalio are processed mainly with electronic, information and telematic means, according to logics and procedures strictly related to the purpose of processing above reported. Data may also be processed without electronic means through manual means; in any case data are kept only for the time necessary to achieve the specific processing purpose from time to time sought and according to applicable legislation on data protection, including security and confidentiality principles that inspire our activity. We will process data according to applicable security requirements to minimize the risks of data destruction and loss, even accidental, of unauthorized access, unlawful processing or processing that does not comply with the purposes of data collection and of illicit or not correct use of data. Moreover, information system and programs are configured so as to minimize the use of personal and identification data so that said data are used only when necessary to the specific processing purpose from time to time sought.  </p>
           </div>
           <div class="col-lg-10 col-md-10 col-sm-11 col-xs-12 content_headline">
               <h4>Privacy Policy and Disclosures:</h4> <hr>
           </div>
           <div class="privacy-text">
             <p>Rigalio believes in protecting your privacy. Any personal information you post on the Website will be used in accordance with our Privacy Policy which can be found below. </p>
           </div>
           <div class="col-lg-10 col-md-10 col-sm-11 col-xs-12 content_headline">
               <h4>Reviews, Comments and use of other Interactive Areas:</h4> <hr>
           </div>
           <div class="privacy-text">
             <p>We appreciate hearing from you. Please be aware that by submitting content to this Website by email, postings on this Website or otherwise, including any reviews, photos, video, questions, comments, suggestions, ideas or the like contained in any submissions (collectively, "Submissions"), you grant Rigalio and its affiliates a nonexclusive, royalty-free, perpetual, transferable, irrevocable and fully sub-licensable right to (a) use, reproduce, modify, adapt, translate, distribute, publish, create derivative works from and publicly display and perform such Submissions throughout the world in any media, now known or hereafter devised, for any purpose; and (b) use the name that you submit in connection with such Submission. You acknowledge that Rigalio may choose to provide attribution of your comments or reviews at our discretion. You further grant Rigalio the right to pursue at law any person or entity that violates your or Rigalio's rights in the Submissions by a breach of this Agreement. You acknowledge and agree that Submissions are non-confidential and non-proprietary. </p>
             <p>Rigalio does not edit or control or create the User Messages posted to or distributed on this Website including through any message exchanged, bulletin boards or other communications forums, and will not be in any way responsible or liable for such User Messages. Rigalio nevertheless reserves the right for any reason in its sole discretion to remove without notice any User Messages and/or Site Content.</p>
             <p> This Website may contain ratings, discussion forums, bulletin boards, review services or other forums in which you or third parties may post reviews of luxury experiences or other content, messages, materials or other items on this Website ("Interactive Areas"). If Rigalio provides such Interactive Areas, you are solely responsible for your use of such Interactive Areas and use them at your own risk. By using any Interactive Areas, you expressly agree not to post, upload to, transmit, distribute, store, create or otherwise publish through this Website (and Rigalio shall have the unilateral right to remove) any of the following:</p>
             <p> Any message, data, information, text, music, sound, photos, graphics, code or any other material ("Content") that is false, unlawful, misleading, libelous, defamatory, obscene, pornographic, indecent, lewd, suggestive, harassing, or advocates harassment of another person, threatening, invasive of privacy or publicity rights, abusive, inflammatory, fraudulent or otherwise objectionable. </p>
             <p>Content that is offensive to the online community, such as content that promotes racism, bigotry, hatred or physical harm of any kind against any group or individual. <br>Content that would constitute, encourage, promote or provide instructions for a conduct of an illegal activity, criminal offense, give rise to civil liability, violate the rights of any party in any country of the world, or that would otherwise create liability or violate any local, state, national or international law. </p>
             <p>Content that may infringe any patent, trademark, trade secret, copyright or other intellectual or proprietary right of any party. In particular, content that promotes an illegal or unauthorized copy of another person's copyrighted work, such as providing pirated computer programs or links to them, providing information to circumvent manufacture-installed copy-protect devices, or providing pirated music or links to pirated music files. <br>Content that impersonates any person or entity or otherwise misrepresents your affiliation with a person or entity, including Rigalio. </p>
             <p>Unsolicited promotions, mass mailings or "spamming", transmission of "junk mail", "chain letters", political campaigning, advertising, contests, raffles, or solicitations. <br>Private information of any third party, including, without limitation, surname (family name) addresses, phone numbers, email addresses, Social Security numbers and credit card numbers. <br>Contains restricted or password only access pages, or hidden pages or images (those not linked to or from another accessible page) </p>
             <p>Viruses, corrupted data or other harmful, disruptive or destructive files. <br>Content that is unrelated to the topic of the Interactive Area(s) in which such Content is posted. <br> or </p>
             <p>Content or links to content that, in the sole judgment of Rigalio, (a) violates the previous subsections herein, (b) is objectionable, (c) which restricts or inhibits any other person from using or enjoying the Interactive Areas or this Website, or (d) which may expose Rigalio or its affiliates or its users to any harm or liability of any type. </p>
             <p>Rigalio takes no responsibility and assumes no liability for any Content posted, stored or uploaded by you or any third party, or for any loss or damage thereto, nor is Rigalio liable for any mistakes, defamation, slander, libel, omissions, falsehoods, obscenity, pornography or profanity you may encounter. As a provider of interactive services, Rigalio is not the author or creator of the third party user generated content, as it is merely an intermediary and is not liable for any statements, representations or Content provided by its users in any public forum, personal home page or other Interactive Area. Although Rigalio has no obligation to screen, edit or monitor any of the Content posted to or distributed through any Interactive Area, Rigalio reserves the right, and has absolute discretion, to remove, screen, translate or edit without notice any Content posted or stored on this Website at any time and for any reason, or to have such actions performed by third parties on its behalf, and you are solely responsible for creating backup copies of and replacing any Content you post or store on this Website at your sole cost and expense. </p>
             <p>If it is determined that you retain moral rights (including rights of attribution or integrity) in the Content, you hereby declare that (a) you do not require that any personally identifying information be used in connection with the Content, or any derivative works of or upgrades or updates thereto; (b) you have no objection to the publication, use, modification, deletion and exploitation of the Content by Rigalio or its licensees, successors and assigns; (c) you forever waive and agree not to claim or assert any entitlement to any and all moral rights of an author in any of the Content; and (d) you forever release Rigalio, and its licensees, successors and assigns, from any claims that you could otherwise assert against Rigalio by virtue of any such moral rights. </p>
             <p>Any use of the Interactive Areas or other portions of this Website in violation of the foregoing violates the terms of this Agreement and may result in, among other things, termination or suspension of your rights to use the Interactive Areas and/or this Website.</p>
           </div>
           <div class="col-lg-10 col-md-10 col-sm-11 col-xs-12 content_headline">
               <h4>Liability Disclaimer:</h4> <hr>
           </div>
           <div class="privacy-text">
             <p>PLEASE READ THIS SECTION CAREFULLY. THIS SECTION LIMITS RIGALIO'S LIABILITY TO YOU FOR ISSUES THAT MAY ARISE IN CONNECTION WITH YOUR USE OF THIS WEBSITE. IF YOU DO NOT UNDERSTAND THE TERMS IN THIS SECTION OR ELSEWHERE IN THE AGREEMENT, PLEASE CONSULT A LAWYER FOR CLARIFICATION BEFORE ACCESSING OR USING THIS WEBSITE. </p>
             <p>THE INFORMATION, SOFTWARE, PRODUCTS, AND SERVICES PUBLISHED ON THIS WEBSITE MAY INCLUDE INACCURACIES OR ERRORS, RIGALIO, ITS SUBSIDIARIES AND CORPORATE AFFILIATES (COLLECTIVELY, THE "RIGALIO GROUP COMPANIES") DO NOT GUARANTEE THE ACCURACY OF, AND DISCLAIM ALL LIABILITY FOR, ANY ERRORS OR OTHER INACCURACIES RELATING TO THE INFORMATION APPEARING ON THIS WEBSITE (INCLUDING, WITHOUT LIMITATION, THE PRODUCT AVAILABILITY, PHOTOGRAPHS, LIST OF PRODUCTS, REVIEWS AND RATINGS, ETC. </p>
             <p>RIGALIO ALSO EXPRESSLY DISCLAIMS ANY WARRANTY, REPRESENTATION, OR OTHER TERM OF ANY KIND AS TO THE ACCURACY OR PROPRIETARY CHARACTER OF THE SITE CONTENT. </p>
             <p>PLEASE READ THIS SECTION CAREFULLY. THIS SECTION LIMITS RIGALIO'S LIABILITY TO YOU FOR ISSUES THAT MAY ARISE IN CONNECTION WITH YOUR USE OF THIS WEBSITE. IF YOU DO NOT UNDERSTAND THE TERMS IN THIS SECTION OR ELSEWHERE IN THE AGREEMENT, PLEASE CONSULT A LAWYER FOR CLARIFICATION BEFORE ACCESSING OR USING THIS WEBSITE. </p>
             <p>NOTHING IN THIS AGREEMENT SHALL EXCLUDE OR LIMIT RIGALIO'S LIABILITY FOR (I) DEATH OR PERSONAL INJURY CAUSED BY NEGLIGENCE; (II) FRAUD; (III) FRAUDULENT MISTATEMENT (IV) DELIBERATE BREACH OR GROSS NEGLIGENCE (V) ANY OTHER LIABILITY WHICH CANNOT BE EXCLUDED UNDER APPLICABLE LAW. </p>
             <p>SUBJECT TO THE FOREGOING, YOU USE THIS WEBSITE AT YOUR OWN RISK AND IN NO EVENT SHALL THE RIGALIO AND GROUP COMPANIES (OR THEIR OFFICERS, DIRECTORS AND AFFILIATES) BE LIABLE FOR ANY DIRECT, INDIRECT, PUNITIVE,INCIDENTAL, SPECIAL, OR CONSEQUENTIAL LOSSSES OR DAMAGES OR ANY LOSS OF INCOME, PROFITS, GOODWILL, DATA, CONTRACTS, USE OF MONEY, OR LOSS OR DAMAGES ARISING FROM OR CONNTECTED IN ANY WAY TO BUSINESS INTERRUPTION OF ANY TYPE ARISING OUT OF, OR IN ANY WAY CONNECTED WITH, YOUR ACCESS TO, DISPLAY OF OR USE OF THIS WEBSITE OR WITH THE DELAY OR INABILITY TO ACCESS, DISPLAY OR USE THIS WEBSITE (INCLUDING, BUT NOT LIMITED TO, YOUR RELIANCE UPON REVIEWS AND OPINIONS APPEARING ON THIS WEBSITE; ANY COMPUTER VIRUSES, INFORMATION, SOFTWARE, LINKED SITES, PRODUCTS, AND SERVICES OBTAINED THROUGH THIS WEBSITE; OR OTHERWISE ARISING OUT OF THE ACCESS TO, DISPLAY OF OR USE OF THIS WEBSITE) WHETHER BASED ON A THEORY OF NEGLIGENCE, CONTRACT, TORT, STRICT LIABILITY, OR OTHERWISE, AND EVEN IF RIGALIO HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. </p>
             <p>THESE TERMS AND CONDITIONS AND FOREGOING LIABLITY DISCLAIMER DO NOT AFFECT MANDATORY LEGAL RIGHTS THAT CANNOT BE EXCLUDED UNDER APPLICABLE LAW. </p>
             <p>The limitation of liability reflects the allocation of risk between the parties. The limitations specified in this section will survive and apply even if any limited remedy specified in these terms is found to have failed of its essential purpose. The limitations of liability provided in these terms inure to the benefit of the Rigalio and Group Companies. </p>
           </div>
           <div class="col-lg-10 col-md-10 col-sm-11 col-xs-12 content_headline">
               <h4>Indemnification: </h4> <hr>
           </div>
           <div class="privacy-text">
             <p>You agree to defend and indemnify Rigalio and its affiliates and any of their officers, directors, employees and agents from and against any claims, causes of action, demands, recoveries, losses, damages, fines, penalties or other costs or expenses of any kind or nature including but not limited to reasonable legal and accounting fees, brought by third parties as a result of: </p>
             <ol>
               <li>Your breach of this Agreement or the documents referenced herein. </li>
               <li>Your violation of any law or the rights of a third party. or </li>
               <li>Your use of this Website.</li>
             </ol>
           </div>
           <div class="col-lg-10 col-md-10 col-sm-11 col-xs-12 content_headline">
               <h4>Links to Third-Party Sites:  </h4> <hr>
           </div>
           <div class="privacy-text">
             <p>This Website may contain hyperlinks to websites operated by parties other than Rigalio. Such hyperlinks are provided for your reference only. We do not control such websites and are not responsible for their contents or the privacy or other practices of such websites. Further, it is up to you to take precautions to ensure that whatever links you select or software you download (whether from this Website or other websites) is free of such items as viruses, worms, trojan horses, defects and other items of a destructive nature. Our inclusion of hyperlinks to such websites does not imply any endorsement of the material on such websites or any association with their operators. In some cases you may be asked by a third party site to link your profile on Rigalio to a profile on another third party site. Choosing to do so is purely optional, and the decision to allow this information to be linked can be disabled (with the third party site) at any time.</p>
           </div>
           <div class="col-lg-10 col-md-10 col-sm-11 col-xs-12 content_headline">
               <h4>Software Available on this Website:  </h4> <hr>
           </div>
           <div class="privacy-text">
             <p>Any software that is made available to download from the Rigalio websites ("Software") is the copyrighted work of Rigalio, or Rigalio affiliates, or other third party software as identified. Your use of such Software is governed by the terms of the end user license agreement, if any, which accompanies, or is included with, the Software ("License Agreement"). You may not install or use any Software that is accompanied by or includes a License Agreement unless you first agree to the License Agreement terms. For any Software made available for download on this Website not accompanied by a License Agreement, we hereby grant to you, the user, a limited, personal, nontransferable license to use the Software for viewing and otherwise using this Website in accordance with these terms and conditions and for no other purpose.</p>
             <p>Please note that all Software, including, without limitation, all HTML, XML, Java code and Active X controls contained on this Website, is owned by Rigalio and/or its affiliates, and is protected by copyright laws and international treaty provisions. Any reproduction or redistribution of the Software is expressly prohibited, and may result in severe civil and criminal penalties. Violators will be prosecuted to the maximum extent possible.</p>
             <p>WITHOUT LIMITING THE FOREGOING, COPYING OR REPRODUCTION OF THE SOFTWARE TO ANY OTHER SERVER OR LOCATION FOR FURTHER REPRODUCTION OR REDISTRIBUTION IS EXPRESSLY PROHIBITED. THE SOFTWARE IS WARRANTED, IF AT ALL, ONLY ACCORDING TO THE TERMS OF THE LICENSE AGREEMENT.</p>
           </div>
           <div class="col-lg-10 col-md-10 col-sm-11 col-xs-12 content_headline">
               <h4>Copyright and Trademark Notices:   </h4> <hr>
           </div>
           <div class="privacy-text">
             <p>All contents of this Website are: © 2016 Rigalio. All rights reserved. Rigalio is not responsible for content on websites operated by parties other than Rigalio. RIGALIO, service names or slogans displayed on this Website are registered and/or common law trademarks of Rigalio and/or its suppliers or licensors, and may not be copied, imitated or used, in whole or in part, without the prior written permission of Rigalio or the applicable trademark holder. In addition, the look and feel of this Website, including all page headers, custom graphics, button icons and scripts, is the service mark, trademark and/or trade dress of Rigalio and may not be copied, imitated or used, in whole or in part, without the prior written permission of Rigalio. All other trademarks, registered trademarks, product names and company names or logos mentioned in this Website are the property of their respective owners. </p>
           </div>
           <div class="col-lg-10 col-md-10 col-sm-11 col-xs-12 content_headline">
               <h4>Modifications   </h4> <hr>
           </div>
           <div class="privacy-text">
             <p>Rigalio may change, add or delete these Terms and Conditions or any portion thereof from time to time in its sole discretion where it deems it necessary for legal, general regulatory and technical purposes, or due to changes in the services provided or nature or layout of the Website. Thereafter, you expressly agree to be bound by any such amended Terms and Conditions.</p>
             <p> Rigalio may change, suspend or discontinue any aspect of the Rigalio service at any time, including availability of any feature, database or content. Rigalio may also impose limits on certain features and services or restrict your access to all or parts of the Website or any other Rigalio web site without notice or liability for technical or security reasons, to prevent against unauthorised access, loss of, or destruction of data or where we consider in our sole discretion that you are in breach of any provision of these Terms and Conditions or of any law or regulation and where it decides to discontinue providing a service.</p>
             <p> YOUR CONTINUED USE OF RIGALIO NOW, OR FOLLOWING THE POSTING OF ANY SUCH NOTICE OF ANY CHANGES, WILL INDICATE ACCEPTANCE BY YOU OF SUCH MODIFICATIONS.</p>
           </div>
           <div class="col-lg-10 col-md-10 col-sm-11 col-xs-12 content_headline">
               <h4>Jurisdiction and Governing Law </h4> <hr>
           </div>
           <div class="privacy-text">
             <p>This Website is operated by Rigalio E-Luxury Private Limited and this Agreement is governed by the laws of Haryana, India. You hereby consent to the exclusive jurisdiction and venue of courts in Haryana, India and stipulate to the fairness and convenience of proceedings in such courts for all disputes arising out of or relating to the use of this Website. You agree that all claims you may have against Rigalio arising from or relating to this Website must be heard and resolved in a court of competent subject matter jurisdiction located in Haryana, India. Use of this Website is unauthorized in any jurisdiction that does not give effect to all provisions of these terms and conditions, including, without limitation, this paragraph. The foregoing shall not apply to the extent that applicable law in your country of residence requires application of another law and/or jurisdiction and this cannot be excluded by contract.</p>
           </div>
           <div class="col-lg-10 col-md-10 col-sm-11 col-xs-12 content_headline">
               <h4>Additional Mobile Licences: </h4> <hr>
           </div>
           <div class="privacy-text">
             <p>Portions of Rigalio mobile software may use copyrighted material, the use of which Rigalio acknowledges. In addition, there are specific terms that apply to use of certain Rigalio mobile applications. Please visit the Mobile Licenses page for notices specific to Rigalio mobile applications.</p>
           </div>
           <div class="col-lg-10 col-md-10 col-sm-11 col-xs-12 content_headline">
               <h4>General Provisions: </h4> <hr>
           </div>
           <div class="privacy-text">
             <p>You agree that no joint venture, agency, partnership, or employment relationship exists between you and the Rigalio and Group Companies and/or affiliates as a result of this Agreement or use of this Website.</p>
             <p>Our performance of this Agreement is subject to existing laws and legal process, and nothing contained in this Agreement limits our right to comply with law enforcement or other governmental or legal requests or requirements relating to your use of this Website or information provided to or gathered by us with respect to such use. To the extent allowed by applicable law, you agree that you will bring any claim or cause of action arising from or relating to your access or use of this Website within two (2) years from the date on which such claim or action arose or accrued or such claim or cause of action will be irrevocably waived.</p>
             <p>If any part of this Agreement is determined to be invalid or unenforceable pursuant to applicable law including, but not limited to, the warranty disclaimers and liability limitations set forth above, then the invalid or unenforceable provision will be deemed superseded by a valid, enforceable provision that most closely matches the intent of the original provision and the remaining provisions in the Agreement shall continue in effect.</p>
             <p>This Agreement (and any other terms and conditions referenced herein) constitutes the entire agreement between you and Rigalio with respect to this Website and it supersedes all prior or contemporaneous communications and proposals, whether electronic, oral, or written, between the customer and Rigalio with respect to this Website. A printed version of this Agreement and of any notice given in electronic form shall be admissible in judicial or administrative proceedings based upon or relating to this Agreement to the same extent and subject to the same conditions as other business documents and records originally generated and maintained in printed form.</p>
             <p>These Terms and Conditions are available in the language of the Website. The specific Terms and Conditions under to which you signify your agreement will not be individually stored by Rigalio.</p>
             <p>The Website may not always be updated on a periodic or regular basis and consequently is not required to register as editorial product under any relevant law.</p>
             <p>Fictitious names of companies, products, people, characters, and/or data mentioned on this Website are not intended to represent any real individual, company, product, or event.</p>
             <p>Any rights not expressly granted herein are reserved.</p>
           </div>
            <div class="col-lg-10 col-md-10 col-sm-11 col-xs-12 content_headline">
               <h4>Service Help:</h4> <hr>
           </div>
           <div class="privacy-text">
             <p>For answers to your questions or ways to contact us, visit our Help Center. Or, you can write to us at: media@rigalio.com</p>
             <p> <b>Rigalio E-Luxury Private Limited. </b> <br> <b>N1/003, The Close (North), Unitech Nirvana Country, </b> <br><b>South City-2, Gurgaon-122018  </b> <br> </p>
             
           </div>
        </div>
      </div> <!--/privacy-pg-con -->
      

    </div>
  </div>  
</div><!--privacy-pg-con ends -->
<footer>
  <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 footer-inner">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
      <p>All rights reserved. All content belongs to respective owners. </p>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
       <ul>
         <li><a href="<?php echo base_url(); ?>main/aboutus">About us </a> </li>
         <li><a href="<?php echo base_url(); ?>main/contactus">Contact us</a> </li>
         <li><a href="<?php echo base_url(); ?>main/privacy">Privacy </a> </li>
         <li><a href="<?php echo base_url(); ?>main/faq">FAQ </a></li>
       </ul>
    </div>
  </div>
</footer>
  </body>
<style type="text/css">
.privacy-text ol li {
    font-size: 12px;
    text-align: justify;
    font-family: 'opensans-reg', arial;
    line-height: 2;
    color: #4f4f4f;
}    
</style>
</html>


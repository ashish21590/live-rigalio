<div style="width: 100%; display:block; clear: both;" id="ss">
    <div style="width: 100%; display: block; clear: both;">
        <div class="main-slider-container hidden-xs screen-flip-slider">
            <div class=" main-slider loading-ico">
                <div class="ui grid invisible">
                    <div class="five column row ">
                        <div class="column">
                            <div class="ui people shape">
                                <div class="sides">
                                    <div class="active side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/slider/small/audemars_piguet_perpetual_calender.jpg" class='ui image' alt="slider_image"/>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>Audemars Piguet Royal Oak Perpetual Calendar: The new invention of time.</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/slider/small/audemars_piguet_royal_oak_offshore_chronograph.jpg" class='ui image' alt="slider_image"/>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>Audemars Piguet Royal Oak Offshore Chronograph: Dynamisma and heritage from the Swiss maestro.</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image" /> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                   <?php /*?> <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/slider/small/bmw-7-series.jpg" class='ui image' alt="slider_image"/>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>The epitome of fine dining </h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div><?php */?>
                                </div>
                            </div>
                            <div class="ui people shape">
                                <div class="sides">
                                    <div class="active side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/slider/small/bmw-7-series.jpg" class='ui image'>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>BMW 7 Series LI M Sport: Benchmarking luxury since 1916.</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/slider/small/burj-al-arab.jpg" class='ui image'/>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>The only 7 star hotel in the world.</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                   <?php /*?> <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/slider/small/gulfstream-g650-er.jpg" class='ui image' alt="slider_image"/>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>LVMH endorses sustainable luxury</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div><?php */?>
                                </div>
                            </div>
                        </div>
                        <div class=" column">
                            <div class="ui people shape">
                                <div class="sides">
                                    <div class="active side">
                                        <div class="content">
                                            <img src="<?php echo base_url(); ?>content/slider/large/hublot-watch.jpg" class='ui image' alt="slider_image"/>
                                            <div class="banner-hover-con">
                                                <div class="tab">
                                                    <div class="tab-in">
                             <span><h4>Hublot Classic Fusion Berluti All Black: Italian style and Swiss genius.</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                    </div>
                                                </div>
                                            </div> <!--/banner-hover-con -->
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <img src="<?php echo base_url(); ?>content/slider/large/indian-motorcycle-roadmaster.jpg" class='ui image' alt="slider_image"/>
                                            <div class="banner-hover-con">
                                                <div class="tab">
                                                    <div class="tab-in">
                             <span><h4>Indian Roadmaster: The open roads are a kingdom.</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                    </div>
                                                </div>
                                            </div> <!--/banner-hover-con -->
                                        </div>
                                    </div>
                                 <?php /*?>   <div class="side">
                                        <div class="content">
                                            <img src="<?php echo base_url(); ?>content/slider/large/macallan.jpg" class='ui image' alt="slider_image"/>
                                            <div class="banner-hover-con">
                                                <div class="tab">
                                                    <div class="tab-in">
                             <span><h4>A rare collection of exquisite tableware</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                    </div>
                                                </div>
                                            </div> <!--/banner-hover-con -->
                                        </div>
                                    </div><?php */?>
                                </div>
                            </div>
                        </div>

                        <div class="column">
                            <div class="ui people shape">
                                <div class="sides">
                                    <div class="active side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/slider/small/ducati_panigale.jpg" class='ui image' alt="slider_image"/>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>Track ready, start to finish.</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/slider/small/gulfstream-g650-er.jpg" class='ui image' alt="slider_image" />
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>Gulfstream 650 ER: The class leader in the skies.</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                   <?php /*?> <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/slider/small/samsung-88-inch.jpg" class='ui image'/>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>Escape monotony for a man made paradise</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div><?php */?>
                                </div>
                            </div>
                            <div class="ui people shape">
                                <div class="sides">
                                    <div class="active side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/slider/small/indian-motorcycle-chief-dark-horse.jpg" class='ui image' alt="slider_image"/>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>Indian Chief Dark Horse: a darker alter ego.</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/slider/small/quadski-xl.jpg" class='ui image' alt="slider_image"/>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>Gibbs Quadski XL: The spirit of luxury adventure.</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                 <?php /*?>   <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/slider/small/vertu-bentley.jpg" class='ui image' alt="slider_image"/>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>A luxurious gastronomic journey</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div><?php */?>
                                </div>
                            </div>
                        </div>

                        <div class=" column hidden-sm4">
                            <div class="ui people shape">
                                <div class="sides">
                                    <div class="active side">
                                        <div class="content">
                                            <img src="<?php echo base_url(); ?>content/slider/large/macallan.jpg" class='ui image' alt="slider_image"/>
                                            <div class="banner-hover-con">
                                                <div class="tab">
                                                    <div class="tab-in">
                             <span><h4>The Macallan Sherry Oak 30 Years: Highland Single Malt Scotch Whisky.</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                    </div>
                                                </div>
                                            </div> <!--/banner-hover-con -->
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <img src="<?php echo base_url(); ?>content/slider/large/rolls-royce-phantom.jpg" class='ui image' alt="slider_image"/>
                                            <div class="banner-hover-con">
                                                <div class="tab">
                                                    <div class="tab-in">
                             <span><h4>Rolls Royce Phantom: Birthed to be bespoke.</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                    </div>
                                                </div>
                                            </div> <!--/banner-hover-con -->
                                        </div>
                                    </div>
                                 <?php /*?>   <div class="side">
                                        <div class="content">
                                            <img src="<?php echo base_url(); ?>content/images/banner/large/Artboard1copy5.jpg" class='ui image' alt="slider_image"/>
                                            <div class="banner-hover-con">
                                                <div class="tab">
                                                    <div class="tab-in">
                             <span><h4>Experience Michelin Star restaurants in the city of love</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                    </div>
                                                </div>
                                            </div> <!--/banner-hover-con -->
                                        </div>
                                    </div><?php */?>
                                </div>
                            </div>
                        </div>

                        <div class="column hidden-sm5">
                            <div class="ui people shape">
                                <div class="sides">
                                    <div class="active side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/slider/small/samsung-88-inch.jpg" class='ui image' alt="slider_image"/>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>Samsung SUHD 4K Curved Smart TV: The brilliance of imagery in Ultra High Definition.</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/slider/small/subzero-pro-88.jpg" class='ui image' alt="slider_image"/>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>Sub-Zero Pro 48: A tribute to nutrition conservation.</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                   <?php /*?> <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/slider/small/Artboard1copy14.jpg" class='ui image' alt="slider_image"/>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>The finest wheels at Concorso d’Eleganza Villa d’Este 2016</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div><?php */?>
                                </div>
                            </div>
                            <div class="ui people shape">
                                <div class="sides">
                                    <div class="active side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/slider/small/taj-palace-mumbai.jpg" class='ui image' alt="slider_image"/>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>Taj Palace Mumbai: The most iconic Taj.</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/slider/small/vertu-bentley.jpg" class='ui image' alt="slider_image"/>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>Vertu Signature for Bentley: The stylish cell-phone crafted after Bentley.</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                   <?php /*?> <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/slider/small/Artboard1copy17.jpg" class='ui image' alt="slider_image"/>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>The Maharaja’s abode</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div><?php */?>
                                </div>
                            </div>
                        </div>

                    </div>

                    <ul class="progress-buttons">
                        <li class="button active"></li>
                        <li class="button"></li>
                        <li class="button"></li>
                    </ul>
                </div> <!--/ui grid flip slider -->


            </div>
        </div>


        <!--slider for mobile -->
        <div class="main-slider-container mobile-flip-slider hidden-lg hidden-md hidden-sm">
            <div class=" main-slider loading-ico">
                <div class="ui grid invisible">
                    <div class="two column row ">
                        <div class="column">
                            <div class="ui people shape">
                                <div class="sides">
                                    <div class="active side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/slider/small/audemars_piguet_perpetual_calender.jpg" class='ui image' alt="slider_image"/>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>Audemars Piguet Royal Oak Perpetual Calendar: The new invention of time.</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/slider/small/audemars_piguet_royal_oak_offshore_chronograph.jpg" class='ui image' alt="slider_image"/>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>Audemars Piguet Royal Oak Offshore Chronograph: Dynamisma and heritage from the Swiss maestro.</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                   <?php /*?> <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/slider/small/bmw-7-series.jpg" class='ui image' alt="slider_image"/>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>BMW 7 Series LI M Sport: Benchmarking luxury since 1916.</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->

                                            </div>
                                        </div>
                                    </div><?php */?>
                                </div>
                            </div>
                            <div class="ui people shape">
                                <div class="sides">
                                    <div class="active side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/slider/small/bmw-7-series.jpg" class='ui image' alt="slider_image"/>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>BMW 7 Series LI M Sport: Benchmarking luxury since 1916.</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/slider/small/burj-al-arab.jpg" class='ui image' alt="slider_image"/>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>The only 7 star hotel in the world.</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                <?php /*?>    <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/slider/small/gulfstream-g650-er.jpg" class='ui image' alt="slider_image"/>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>Gulfstream 650 ER: The class leader in the skies.</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div><?php */?>
                                </div>
                            </div>
                            <div class="ui people shape">
                                <div class="sides">
                                    <div class="active side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/slider/small/ducati_panigale.jpg" class='ui image' alt="slider_image"/>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>Track ready, start to finish.</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/slider/small/gulfstream-g650-er.jpg" class='ui image' alt="slider_image"/>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">

                             <span><h4>Gulfstream 650 ER: The class leader in the skies.</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                   <?php /*?> <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/slider/small/Artboard1copy8.jpg" class='ui image' alt="slider_image"/>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>Escape monotony for a man made paradise</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div><?php */?>
                                </div>
                            </div>
                        </div>
                        <div class=" column">
                            <div class="ui people shape">
                                <div class="sides">
                                    <div class="active side">
                                        <div class="content">
                                            <img src="<?php echo base_url(); ?>content/slider/small/indian-motorcycle-chief-dark-horse.jpg" class='ui image' alt="slider_image"/>
                                            <div class="banner-hover-con">
                                                <div class="tab">
                                                    <div class="tab-in">
                             <span><h4>Indian Chief Dark Horse: a darker alter ego.</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                    </div>
                                                </div>
                                            </div> <!--/banner-hover-con -->
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <img src="<?php echo base_url(); ?>content/slider/small/quadski-xl.jpg" class='ui image' alt="slider_image"/>
                                            <div class="banner-hover-con">
                                                <div class="tab">
                                                    <div class="tab-in">
                             <span><h4>Gibbs Quadski XL: The spirit of luxury adventure.</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                    </div>
                                                </div>
                                            </div> <!--/banner-hover-con -->
                                        </div>
                                    </div>
                                   <?php /*?> <div class="side">
                                        <div class="content">
                                            <img src="<?php echo base_url(); ?>content/slider/small/Artboard1copy11.jpg" class='ui image' alt="slider_image"/>
                                            <div class="banner-hover-con">
                                                <div class="tab">
                                                    <div class="tab-in">
                             <span><h4>A luxurious gastronomic journey</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                    </div>
                                                </div>
                                            </div> <!--/banner-hover-con -->
                                        </div>
                                    </div><?php */?>
                                </div>
                            </div>
                            <div class="ui people shape">
                                <div class="sides">
                                    <div class="active side">
                                        <div class="content">
                                            <img src="<?php echo base_url(); ?>content/slider/small/samsung-88-inch.jpg" class='ui image' alt="slider_image"/>


                                            <div class="banner-hover-con">
                                                <div class="tab">
                                                    <div class="tab-in">
                             <span><h4>Samsung SUHD 4K Curved Smart TV: The brilliance of imagery in Ultra High Definition.</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                    </div>
                                                </div>
                                            </div> <!--/banner-hover-con -->
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <img src="<?php echo base_url(); ?>content/slider/small/subzero-pro-88.jpg" class='ui image' alt="slider_image"/>
                                            <div class="banner-hover-con">
                                                <div class="tab">
                                                    <div class="tab-in">
                             <span><h4>Sub-Zero Pro 48: A tribute to nutrition conservation.</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                    </div>
                                                </div>
                                            </div> <!--/banner-hover-con -->
                                        </div>
                                    </div>
                                   <?php /*?> <div class="side">
                                        <div class="content">
                                            <img src="<?php echo base_url(); ?>content/slider/small/Artboard1copy14.jpg" class='ui image' alt="slider_image"/>
                                            <div class="banner-hover-con">
                                                <div class="tab">
                                                    <div class="tab-in">
                             <span><h4>The finest wheels at Concorso d’Eleganza Villa d’Este 2016</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                    </div>
                                                </div>
                                            </div> <!--/banner-hover-con -->
                                        </div>
                                    </div><?php */?>
                                </div>
                            </div>
                            <div class="ui people shape">
                                <div class="sides">
                                    <div class="active side">
                                        <div class="content">
                                            <img src="<?php echo base_url(); ?>content/slider/small/taj-palace-mumbai.jpg" class='ui image' alt="slider_image"/>
                                            <div class="banner-hover-con">
                                                <div class="tab">
                                                    <div class="tab-in">
                             <span><h4>Taj Palace Mumbai: The most iconic Taj.</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                    </div>
                                                </div>
                                            </div> <!--/banner-hover-con -->
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <img src="<?php echo base_url(); ?>content/slider/small/vertu-bentley.jpg" class='ui image' alt="slider_image"/>
                                            <div class="banner-hover-con">
                                                <div class="tab">
                                                    <div class="tab-in">
                             <span><h4>Vertu Signature for Bentley: The stylish cell-phone crafted after Bentley.</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                    </div>
                                                </div>
                                            </div> <!--/banner-hover-con -->
                                        </div>
                                    </div>
                                   <?php /*?> <div class="side">
                                        <div class="content">
                                            <img src="<?php echo base_url(); ?>content/slider/small/Artboard1copy17.jpg" class='ui image' alt="slider_image"/>
                                            <div class="banner-hover-con">
                                                <div class="tab">
                                                    <div class="tab-in">
                             <span><h4>The Maharaja’s abode</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                    </div>
                                                </div>
                                            </div> <!--/banner-hover-con -->
                                        </div>
                                    </div><?php */?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <ul class="progress-buttons">
                        <li class="button active"></li>
                        <li class="button"></li>
                        <li class="button"></li>
                    </ul>
                </div> <!--/ui grid flip slider -->


            </div>
        </div>

        <!--slider for mobile ends -->
    </div>
</div>

<link href="<?php echo base_url(); ?>content/css/carousel.css" rel="stylesheet">
<script type="text/javascript" src="<?php echo base_url(); ?>content/js/semantic.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>content/css/semantic.min.css">
<script type="text/javascript" src="<?php echo base_url(); ?>content/js/myflip.js"></script>


<script type="text/javascript">

    //Read the baseurl from the config.php file
    (function(d){
        var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement('script'); js.id = id; js.async = true;
        ref.parentNode.insertBefore(js, ref);
    }(document));
    window.fbAsyncInit = function() {
        //Initiallize the facebook using the facebook javascript sdk
        FB.init({
            appId:'<?php echo $this->config->item('appID'); ?>', // App ID
            cookie:true, // enable cookies to allow the server to access the session
            status:true, // check login status
            xfbml:true, // parse XFBML
            oauth : true //enable Oauth
        });
    };

    //Onclick for fb login
</script>

</body>

</html>


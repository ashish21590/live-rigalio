<!--log in page -->
<div class="login-pg col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
  <div class="container-fluid">
    <div class="row">
      <div class="login-pg-inner col-lg-10 col-md-10 col-sm-11 col-xs-11 nopadding">
          <div class="signin-popup">
            <div class="signin-popup-inner">
                <div class="tab">
                    <div class="tab-cell">
                        <div class="signinvia col-lg-8 col-md-8 col-sm-12 col-xs-12">
                          <div class="invite-request" style="display: block;">
                    <h3>Request an Invite </h3>
                    <hr>
                        <p id="msg2" class="msg2"></p>
                        <div class="aftersubmit">
                            <ul class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                  <?php /*?><li id="fbreqndinvitepage"> <img src="<?php echo base_url(); ?>content/images/social-icons/facebook-invite-button.jpg" class="img-responsive"><div class="register-fb<a class="register-fb fb_login" onlogin="checkLoginState();"></a></div> </li><?php */?>
                                  
                                  <li><div class="invite-btns"> <i class="fa fa-facebook" aria-hidden="true"></i><span> Request via Facebook</span> </div><div class="register-fb"><fb:login-button size="xlarge" max_rows="7" scope="email,user_friends,user_birthday" data-auto-logout-link="true" onlogin="checkLoginState();"> </fb:login-button><a class="register-fb fb_login" onlogin="checkLoginState();"></a></div> </li>
                                
                                  <li class="sep"><span class="or-brk"> OR</span></li>
                                  
                                 <li id="linkdinreqndinvite"><div class="invite-btns"> <i class="fa fa-linkedin" aria-hidden="true"></i><span> Request via Linkedin</span> </div><div class="linkedin-log"> <script type="in/Login"></script></div> <li>
                                    <div id="profiles"></div>
                                    
                                    
                            </li></ul>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
                            <a href="<?php echo base_url(); ?>main/loginpage" class="go-back-href">
                                <button class="continue-btn go-back-btn">go back</button>
                            </a>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
</div>
      </div> 
 <div class="loader-part" style="display:none;"><img src="<?php echo base_url(); ?>content/images/ring.gif" class="img-responsive"/></div>
<!--/login-pg-inner -->
    </div>
  </div>  
</div>
<!--log in page ends -->

  </body>

</html>

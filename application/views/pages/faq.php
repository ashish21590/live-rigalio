<!--faq-pg con -->
<div class="faq-pg col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
  <div class="container-fluid">
    <div class="row">

      <div class="faq-pg-con col-lg-10 col-md-10 col-sm-11 col-xs-11 nopadding">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
           <div class="col-lg-10 col-md-10 col-sm-11 col-xs-12 content_headline">
             <h2>FAQ</h2> <hr>
           </div>
           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 faq-section nopadding">
             <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 faq-questions">
                <span>What is Rigalio? </span>
                <div class="markup"><hr> </div>
                <div class="faq-start"><img src="<?php echo base_url(); ?>content/images/icons/faq-start-icon.png"></div>
             </div> <!--/left question con -->
             <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12 faq-ans">
               <p>Rigalio is the first platform in the realm of digital luxury to offer consumers the finest and most exclusive insights into the world of luxury and brands, curated by experts from the field. It is a luxury cataloguing digital platform that is accessible through an ‘Invite-Only’ basis whose intentions are to keep the privacy of its membership exclusive and secure. Rigalio provides a bespoke avenue for luxury brands to exhibit and showcase their fabulous merchandises on a digital domain that is tailor made to luxury</p>
             </div> <!--/right answers con -->
           </div> <!--/ faq-section -->
           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 faq-section nopadding">
             <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 faq-questions">
                <span>Is Rigalio an e-commerce website?</span>
                <div class="markup"><hr> </div>
             </div> <!--/left question con -->
             <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12 faq-ans">
               <p>No, Rigalio is not an e-commerce website. It is a luxury product cataloguing website that highlights and catalogues the finest in the world of luxury across the globe. If someone is interested in a particular product, Rigalio will forward the interest to the requisite brands, and the brands will personally get in touch with the consumer.</p>
             </div> <!--/right answers con -->
           </div> <!--/ faq-section -->
           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 faq-section nopadding">
             <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 faq-questions">
                <span>How do I request an Invite on Rigalio?</span>
                <div class="markup"><hr> </div>
             </div> <!--/left question con -->
             <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12 faq-ans">
               <p>In order to maintain exclusivity, Rigalio is an Invite-Only platform. Interested users can request an invite through the ‘Request an Invite’ form which is displayed on the homepage of the website-www.rigalio.com.</p>
             </div> <!--/right answers con -->
           </div> <!--/ faq-section -->
           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 faq-section nopadding">
             <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 faq-questions">
                <span>Do I need a membership to browse products on Rigalio?</span>
                <div class="markup"><hr> </div>
             </div> <!--/left question con -->
             <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12 faq-ans">
               <p>No, users do not need a membership to browse products on Rigalio. However, to pursue any other engagement like crowning an item, interacting with other members, or commenting on a product, users will require a membership account. Further, in order to get live updates along with the current luxury news and launces, users will require a membership.</p>
             </div> <!--/right answers con -->
           </div> <!--/ faq-section -->
           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 faq-section nopadding">
             <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 faq-questions">
                <span>How will I know if my Invite Request is accepted?</span>
                <div class="markup"><hr> </div>
             </div> <!--/left question con -->
             <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12 faq-ans">
               <p>To preserve the exclusivity of its members and the website, Rigalio pursues a strict evaluation method to ascertain the validity of your Invite request. The request is screened by experts and curators from the field of luxury. Once the invite is screened and accepted, the user will be notified through an e-mail along with a special username and their exclusive password.</p>
             </div> <!--/right answers con -->
           </div> <!--/ faq-section -->
           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 faq-section nopadding">
             <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 faq-questions">
                <span>Can I share the products and articles showcased on Rigalio in my social media profiles?</span>
                <div class="markup"><hr> </div>
             </div> <!--/left question con -->
             <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12 faq-ans">
               <p>Yes, one can share all engagements on Rigalio on social media platforms.</p>
             </div> <!--/right answers con -->
           </div> <!--/ faq-section -->
           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 faq-section nopadding">
             <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 faq-questions">
                <span>Will my confidential information be secure on Rigalio, a luxury digital platform?</span>
                <div class="markup"><hr> </div>
             </div> <!--/left question con -->
             <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12 faq-ans">
               <p>Yes, the foremost aim of Rigalio is to preserve the sanctity of the information received from users. The exclusivity is maintained through a strict membership program. Apart from the same a host of Privacy Controls gives member users total control on what they want to share and what they do not want to share on the website.</p>
             </div> <!--/right answers con -->
           </div> <!--/ faq-section -->
           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 faq-section nopadding">
             <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 faq-questions">
                <span>If I need any assistance with regard to any issue on the website who should I get in touch with?</span>
                <div class="markup"><hr> </div>
             </div> <!--/left question con -->
             <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12 faq-ans">
               <p>Rigalio will personally address each and every issue that member’s face on the website. The ‘Contact Us’ icon directs users and members to our e-mail addresses and the phone numbers which are manned by dedicated operators. Rigalio promises to get in touch with members and address their concerns within twelve hours of any issue being reported.</p>
             </div> <!--/right answers con -->
           </div> <!--/ faq-section -->
           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 faq-section nopadding">
             <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 faq-questions">
                <span>Will I receive special offers or concessions if I am a member on Rigalio?</span>
                <div class="markup"><hr> </div>
             </div> <!--/left question con -->
             <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12 faq-ans">
               <p>No, Rigalio is a digital platform for luxury. No offers or concessions will be ever distributed on Rigalio. However, members will also be able to keep up-to-date with luxury launches and special screenings which are exclusive events hosted by luxury brands.</p>
             </div> <!--/right answers con -->
           </div> <!--/ faq-section -->
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 faq-section nopadding">
             <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 faq-questions">
                <span>Does membership on Rigalio entail any membership fee?</span>
                <div class="markup"><hr> </div>
             </div> <!--/left question con -->
             <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12 faq-ans">
               <p>No, currently Rigalio does not charge any currency for membership. It is only the profile of the individual which is screened and validated as a deciding factor/vantage point to grant membership.</p>
             </div> <!--/right answers con -->
           </div> <!--/ faq-section -->
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 faq-section nopadding">
             <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 faq-questions">
                <span>Can I use the information on Rigalio on any other platform?</span>
                <div class="markup"><hr> </div>
             </div> <!--/left question con -->
             <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12 faq-ans">
               <p>No, all the information on Rigalio is strictly copyrighted material any third party usage of pictures or literature will immediately lead to copyright infringement and prosecution.</p>
             </div> <!--/right answers con -->
           </div> <!--/ faq-section -->
          <?php /*?> <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 faq-section nopadding">
             <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 faq-questions">
                <div class="load-more-faq"><a href="#">Load more</a> </div>
             </div> <!--/left question con -->
             <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12 faq-ans">
             </div> <!--/right answers con -->
           </div> <!--/ faq-section --><?php */?>
           
        </div>
      </div> <!--/faq-pg-con inner ends -->
      

    </div>
  </div>  
</div><!--faq-pg-con ends -->
<footer>
  <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 footer-inner">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
      <p>All rights reserved. All content belongs to respective owners. </p>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
       <ul>
         <li><a href="<?php echo base_url();?>main/aboutus">About us </a> </li>
         <li><a href="<?php echo base_url();?>main/contactus">Contact us</a> </li>
         <li><a href="<?php echo base_url();?>main/privacy">Privacy </a> </li>
         <li><a href="<?php echo base_url();?>main/faq">Faq </a> </li>
       </ul>
    </div>
  </div>
</footer>

<script src="<?php echo base_url(); ?>content/js/bootstrap.min.js"></script>

<style>

</style>

  


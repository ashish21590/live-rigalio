<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?php if($seo){ echo $seo[0]['title']; }?>">
    <meta name="author" content="">
    <meta name="keyword" content="<?php if($seo){ echo $seo[0]['keywords']; }?>">
    

    <title><?php if($seo){ echo $seo[0]['title']; }?></title>
    <script src="<?php echo base_url(); ?>content/js/jquery-1.9.1.min.js"></script>
    <link rel="stylesheet" href="<?php echo base_url(); ?>content/css/font-awesome.css">
    <link href="<?php echo base_url(); ?>content/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>content/css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url(); ?>content/css/header.css" />
    <link href="<?php echo base_url(); ?>content/css/category.css" rel="stylesheet">
  
    <!-- Custom styles for this template -->
    <link href="<?php echo base_url(); ?>content/css/carousel.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>content/css/master.css" rel="stylesheet">
    <script src="<?php echo base_url(); ?>content/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>content/js/mycustom.js"></script>
    <script src="<?php echo base_url(); ?>content/js/classie.js"></script>
    <link rel="stylesheet" href="<?php echo base_url(); ?>content/css/jquery-ui.css">
    <script src="<?php echo base_url(); ?>content/js/jquery-ui.js"></script>
  </head>
  

  
<!-- NAVBAR
================================================== -->
  <?php 
  $username=$username[0]['username'];
  ?>
  <body>
<nav class="navbar navbar-inverse navbar-fixed-top" id="header_nav">
    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 header-collapse-btn">
      
    </div>
    <div class="main-logo col-lg-6 col-md-6 col-sm-6 col-xs-6">
        <a href="javascript:void(0)"> <img src="<?php echo base_url(); ?>content/images/main-logo.png" class="img-responsive"> </a>
    </div>
   <?php /*?> <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 drop-search-wrap navbar-right">
      <a href="javascript:void(0)" class="search-icon"> <img src="<?php echo base_url(); ?>content/images/icons/search-icon.png" class="img-responsive"></a>
    </div>
    <div class="search-hidden col-lg-12 col-md-12 col-sm-12 col-xs-12 dropdown-search " style="display:none;">
            <form id="searchform" role="search" method="get" action="http://themes.birdwp.com/zefir-new/">
              <input type="text" name="s" id="s" class="search-field form-control" placeholder="I am Looking for">
            </form>
    </div><?php */?>
</nav>
  <!--/header popup -->     

<div class="reset-pass-con col-lg-12 nopadding">
    <div class="col-lg-6 col-md-6 col-sm-9 col-xs-12 nopadding reset-pass-inner"> 
      <div id="username123" style="display:none" value="<?php print_r($username); ?>"></div>
      <h3>Reset password </h3>
      <div class="password-info" style="display: block;">
         <ul class="nopadding">
           <li class="passwrdshow"><input type="password" class="textboxd " value="" id="test12" placeholder="NEW PASSWORD" name=""><span class="text">Show</span><input type="checkbox" class="hiden-chck" id="test21">  </li>
         </ul>
         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding"><button class="save-btn" id="savebtn1">save</button> <a href="<?php echo base_url();?>"><button class="save-btn">Go Back</button></a></div>
       </div>
    </div>
</div>  <!--/reset-pass-con -->

<input type="text"  value="<?php print_r($username); ?>" id="user123" style="display:none" >

  </body>
  
<script>
$.toggleShowPassword({
    field: '#test12',
    control: '#test21'
});
</script>

</html>

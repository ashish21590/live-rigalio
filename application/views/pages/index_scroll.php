

    <!-- Carousel
    ================================================== -->
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding mg-top">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
  <div class="main-slider-container hidden-xs">  
    <div id="myCarousel" class="carousel main-slider slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner" role="listbox">
        <div class="item active">
          <div class="row">
            <div class="slider-sections sectn1">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images banner-img1-sectn1 nopadding"> <img src="content/images/banner/banner-img5.jpg" class="img-responsive">
                  <div class="banner-hover-con">
                         <span><h4>Add a pop of yellow to black and gray </h4>
                         <hr>
                         <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                  </div> <!--/banner-hover-con -->
                </div>
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images banner-img2-sectn1 nopadding"> <img src="content/images/banner/banner-img7.jpg" class="img-responsive"> 
                  <div class="banner-hover-con">
                         <span><h4>Add a pop of yellow to black and gray </h4>
                         <hr>
                         <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                  </div> <!--/banner-hover-con -->
               </div>
            </div>
            <div class="slider-sections sectn2">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images banner-img1-sectn2 nopadding"> <img src="content/images/banner/banner-img4.jpg" class="img-responsive"> 
                  <div class="banner-hover-con">
                         <span><h4>Add a pop of yellow to black and gray </h4>
                         <hr>
                         <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                  </div> <!--/banner-hover-con -->
               </div>
            </div>
            <div class="slider-sections sectn3">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images banner-img1-sectn3 nopadding"> <img src="content/images/banner/banner-img5.jpg" class="img-responsive"> 
                  <div class="banner-hover-con">
                         <span><h4>Add a pop of yellow to black and gray </h4>
                         <hr>
                         <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                  </div> <!--/banner-hover-con -->
               </div>
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images nopadding banner-img2-sectn3"> <img src="content/images/banner/banner-img7.jpg" class="img-responsive"> 
                  <div class="banner-hover-con">
                         <span><h4>Add a pop of yellow to black and gray </h4>
                         <hr>
                         <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                  </div> <!--/banner-hover-con -->
               </div>
            </div>
            <div class="slider-sections sectn4">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images nopadding banner-img1-sectn4"> <img src="content/images/banner/banner-img4.jpg" class="img-responsive"> 
                   <div class="banner-hover-con">
                         <span><h4>Add a pop of yellow to black and gray </h4>
                         <hr>
                         <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                  </div> <!--/banner-hover-con -->
               </div>
            </div>
            <div class="slider-sections sectn5 hidden-sm">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images nopadding banner-img1-sectn5"> <img src="content/images/banner/banner-img5.jpg" class="img-responsive"> 
                  <div class="banner-hover-con">
                         <span><h4>Add a pop of yellow to black and gray </h4>
                         <hr>
                         <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                  </div> <!--/banner-hover-con -->
               </div>
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images nopadding banner-img2-sectn5"> <img src="content/images/banner/banner-img7.jpg" class="img-responsive"> 
                  <div class="banner-hover-con">
                         <span><h4>Add a pop of yellow to black and gray </h4>
                         <hr>
                         <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                  </div> <!--/banner-hover-con -->
               </div>
            </div>
          </div>
        </div> <!--/slide1 -->
        <div class="item">
         <div class="row">
            <div class="slider-sections sectn1">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images banner-img1-sectn1 nopadding"> <img src="content/images/banner/banner-img5.jpg" class="img-responsive">
                  <div class="banner-hover-con">
                         <span><h4>Add a pop of yellow to black and gray </h4>
                         <hr>
                         <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                  </div> <!--/banner-hover-con -->
                </div>
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images banner-img2-sectn1 nopadding"> <img src="content/images/banner/banner-img7.jpg" class="img-responsive"> 
                  <div class="banner-hover-con">
                         <span><h4>Add a pop of yellow to black and gray </h4>
                         <hr>
                         <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                  </div> <!--/banner-hover-con -->
               </div>
            </div>
            <div class="slider-sections sectn2">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images banner-img1-sectn2 nopadding"> <img src="content/images/banner/banner-img4.jpg" class="img-responsive"> 
                  <div class="banner-hover-con">
                         <span><h4>Add a pop of yellow to black and gray </h4>
                         <hr>
                         <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                  </div> <!--/banner-hover-con -->
               </div>
            </div>
            <div class="slider-sections sectn3">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images banner-img1-sectn3 nopadding"> <img src="content/images/banner/banner-img5.jpg" class="img-responsive"> 
                  <div class="banner-hover-con">
                         <span><h4>Add a pop of yellow to black and gray </h4>
                         <hr>
                         <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                  </div> <!--/banner-hover-con -->
               </div>
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images nopadding banner-img2-sectn3"> <img src="content/images/banner/banner-img7.jpg" class="img-responsive"> 
                  <div class="banner-hover-con">
                         <span><h4>Add a pop of yellow to black and gray </h4>
                         <hr>
                         <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                  </div> <!--/banner-hover-con -->
               </div>
            </div>
            <div class="slider-sections sectn4">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images nopadding banner-img1-sectn4"> <img src="content/images/banner/banner-img4.jpg" class="img-responsive"> 
                   <div class="banner-hover-con">
                         <span><h4>Add a pop of yellow to black and gray </h4>
                         <hr>
                         <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                  </div> <!--/banner-hover-con -->
               </div>
            </div>
            <div class="slider-sections sectn5 hidden-sm">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images nopadding banner-img1-sectn5"> <img src="content/images/banner/banner-img5.jpg" class="img-responsive"> 
                  <div class="banner-hover-con">
                         <span><h4>Add a pop of yellow to black and gray </h4>
                         <hr>
                         <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                  </div> <!--/banner-hover-con -->
               </div>
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images nopadding banner-img2-sectn5"> <img src="content/images/banner/banner-img7.jpg" class="img-responsive"> 
                  <div class="banner-hover-con">
                         <span><h4>Add a pop of yellow to black and gray </h4>
                         <hr>
                         <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                  </div> <!--/banner-hover-con -->
               </div>
            </div>
          </div>
        </div> <!--/slide2 -->
        <div class="item">
          <div class="row">
            <div class="slider-sections sectn1">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images banner-img1-sectn1 nopadding"> <img src="content/images/banner/banner-img5.jpg" class="img-responsive">
                  <div class="banner-hover-con">
                         <span><h4>Add a pop of yellow to black and gray </h4>
                         <hr>
                         <div class="cat-zoom-icon"> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                  </div> <!--/banner-hover-con -->
                </div>
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images banner-img2-sectn1 nopadding"> <img src="content/images/banner/banner-img7.jpg" class="img-responsive"> 
                  <div class="banner-hover-con">
                         <span><h4>Add a pop of yellow to black and gray </h4>
                         <hr>
                         <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                  </div> <!--/banner-hover-con -->
               </div>
            </div>
            <div class="slider-sections sectn2">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images banner-img1-sectn2 nopadding"> <img src="content/images/banner/banner-img4.jpg" class="img-responsive"> 
                  <div class="banner-hover-con">
                         <span><h4>Add a pop of yellow to black and gray </h4>
                         <hr>
                         <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                  </div> <!--/banner-hover-con -->
               </div>
            </div>
            <div class="slider-sections sectn3">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images banner-img1-sectn3 nopadding"> <img src="content/images/banner/banner-img5.jpg" class="img-responsive"> 
                  <div class="banner-hover-con">
                         <span><h4>Add a pop of yellow to black and gray </h4>
                         <hr>
                         <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                  </div> <!--/banner-hover-con -->
               </div>
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images nopadding banner-img2-sectn3"> <img src="content/images/banner/banner-img7.jpg" class="img-responsive"> 
                  <div class="banner-hover-con">
                         <span><h4>Add a pop of yellow to black and gray </h4>
                         <hr>
                         <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                  </div> <!--/banner-hover-con -->
               </div>
            </div>
            <div class="slider-sections sectn4">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images nopadding banner-img1-sectn4"> <img src="content/images/banner/banner-img4.jpg" class="img-responsive"> 
                   <div class="banner-hover-con">
                         <span><h4>Add a pop of yellow to black and gray </h4>
                         <hr>
                         <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                  </div> <!--/banner-hover-con -->
               </div>
            </div>
            <div class="slider-sections sectn5 hidden-sm">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images nopadding banner-img1-sectn5"> <img src="content/images/banner/banner-img5.jpg" class="img-responsive"> 
                  <div class="banner-hover-con">
                         <span><h4>Add a pop of yellow to black and gray </h4>
                         <hr>
                         <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                  </div> <!--/banner-hover-con -->
               </div>
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images nopadding banner-img2-sectn5"> <img src="content/images/banner/banner-img7.jpg" class="img-responsive"> 
                  <div class="banner-hover-con">
                         <span><h4>Add a pop of yellow to black and gray </h4>
                         <hr>
                         <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                  </div> <!--/banner-hover-con -->
               </div>
            </div>
          </div>
        </div><!--/slide3 -->
      </div>



      <!--<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span>
        <span class="sr-only">Next</span>
      </a> -->
    </div><!--/carousel -->


      <!-- mobile view slider -->
      <div id="mbCarousel" class="carousel main-slider slide" data-ride="carousel" style="display:none;">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#mbCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#mbCarousel" data-slide-to="1"></li>
        <li data-target="#mbCarousel" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner" role="listbox">
        <div class="item active">
          <div class="row">
            <div class="slider-sections sectn1">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images banner-img1-sectn1 nopadding"> <img src="content/images/banner/banner-img5.jpg" class="img-responsive">
                  <div class="banner-hover-con">
                         <span><h4>Add a pop of yellow to black and gray </h4>
                         <hr>
                         <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                  </div> <!--/banner-hover-con -->
                </div>
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images banner-img2-sectn1 nopadding"> <img src="content/images/banner/banner-img7.jpg" class="img-responsive"> 
                  <div class="banner-hover-con">
                         <span><h4>Add a pop of yellow to black and gray </h4>
                         <hr>
                         <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                  </div> <!--/banner-hover-con -->
               </div>
            </div>
            <div class="slider-sections sectn2">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images banner-img1-sectn2 nopadding"> <img src="content/images/banner/banner-img4.jpg" class="img-responsive"> 
                  <div class="banner-hover-con">
                         <span><h4>Add a pop of yellow to black and gray </h4>
                         <hr>
                         <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                  </div> <!--/banner-hover-con -->
               </div>
            </div>
            <div class="slider-sections sectn3">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images banner-img1-sectn3 nopadding"> <img src="content/images/banner/banner-img5.jpg" class="img-responsive"> 
                  <div class="banner-hover-con">
                         <span><h4>Add a pop of yellow to black and gray </h4>
                         <hr>
                         <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                  </div> <!--/banner-hover-con -->
               </div>
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images nopadding banner-img2-sectn3"> <img src="content/images/banner/banner-img7.jpg" class="img-responsive"> 
                  <div class="banner-hover-con">
                         <span><h4>Add a pop of yellow to black and gray </h4>
                         <hr>
                         <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                  </div> <!--/banner-hover-con -->
               </div>
            </div>
            <div class="slider-sections sectn4">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images nopadding banner-img1-sectn4"> <img src="content/images/banner/banner-img4.jpg" class="img-responsive"> 
                   <div class="banner-hover-con">
                         <span><h4>Add a pop of yellow to black and gray </h4>
                         <hr>
                         <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                  </div> <!--/banner-hover-con -->
               </div>
            </div>
            <div class="slider-sections sectn5">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images nopadding banner-img1-sectn5"> <img src="content/images/banner/banner-img5.jpg" class="img-responsive"> 
                  <div class="banner-hover-con">
                         <span><h4>Add a pop of yellow to black and gray </h4>
                         <hr>
                         <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                  </div> <!--/banner-hover-con -->
               </div>
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images nopadding banner-img2-sectn5"> <img src="content/images/banner/banner-img7.jpg" class="img-responsive"> 
                  <div class="banner-hover-con">
                         <span><h4>Add a pop of yellow to black and gray </h4>
                         <hr>
                         <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                  </div> <!--/banner-hover-con -->
               </div>
            </div>
          </div>
        </div> <!--/slide1 -->
        <div class="item">
         <div class="row">
            <div class="slider-sections sectn1">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images banner-img1-sectn1 nopadding"> <img src="content/images/banner/banner-img5.jpg" class="img-responsive">
                  <div class="banner-hover-con">
                         <span><h4>Add a pop of yellow to black and gray </h4>
                         <hr>
                         <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                  </div> <!--/banner-hover-con -->
                </div>
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images banner-img2-sectn1 nopadding"> <img src="content/images/banner/banner-img7.jpg" class="img-responsive"> 
                  <div class="banner-hover-con">
                         <span><h4>Add a pop of yellow to black and gray </h4>
                         <hr>
                         <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                  </div> <!--/banner-hover-con -->
               </div>
            </div>
            <div class="slider-sections sectn2">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images banner-img1-sectn2 nopadding"> <img src="content/images/banner/banner-img4.jpg" class="img-responsive"> 
                  <div class="banner-hover-con">
                         <span><h4>Add a pop of yellow to black and gray </h4>
                         <hr>
                         <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                  </div> <!--/banner-hover-con -->
               </div>
            </div>
            <div class="slider-sections sectn3">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images banner-img1-sectn3 nopadding"> <img src="content/images/banner/banner-img5.jpg" class="img-responsive"> 
                  <div class="banner-hover-con">
                         <span><h4>Add a pop of yellow to black and gray </h4>
                         <hr>
                         <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                  </div> <!--/banner-hover-con -->
               </div>
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images nopadding banner-img2-sectn3"> <img src="content/images/banner/banner-img7.jpg" class="img-responsive"> 
                  <div class="banner-hover-con">
                         <span><h4>Add a pop of yellow to black and gray </h4>
                         <hr>
                         <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                  </div> <!--/banner-hover-con -->
               </div>
            </div>
            <div class="slider-sections sectn4">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images nopadding banner-img1-sectn4"> <img src="content/images/banner/banner-img4.jpg" class="img-responsive"> 
                   <div class="banner-hover-con">
                         <span><h4>Add a pop of yellow to black and gray </h4>
                         <hr>
                         <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                  </div> <!--/banner-hover-con -->
               </div>
            </div>
            <div class="slider-sections sectn5">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images nopadding banner-img1-sectn5"> <img src="content/images/banner/banner-img5.jpg" class="img-responsive"> 
                  <div class="banner-hover-con">
                         <span><h4>Add a pop of yellow to black and gray </h4>
                         <hr>
                         <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                  </div> <!--/banner-hover-con -->
               </div>
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 banner-images nopadding banner-img2-sectn5"> <img src="content/images/banner/banner-img7.jpg" class="img-responsive"> 
                  <div class="banner-hover-con">
                         <span><h4>Add a pop of yellow to black and gray </h4>
                         <hr>
                         <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                  </div> <!--/banner-hover-con -->
               </div>
            </div>
          </div>
        </div> <!--/slide2 -->
        
      </div>

    </div><!--/carousel -->
  </div> 


  <!--slider for mobile -->
   <div class="main-slider-container hidden-lg hidden-md hidden-sm">  
  
      <!-- mobile view slider -->
      <div id="mbCarousel" class="carousel main-slider slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
          <li data-target="#mbCarousel" data-slide-to="0" class="active"></li>
          <li data-target="#mbCarousel" data-slide-to="1"></li>
          <li data-target="#mbCarousel" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
          <div class="item active">
            <div class="row">
              <div class="col-sm-12 col-xs-12"> 
                  <div class="col-sm-6 col-xs-6 banner-images banner-img1-sectn1 nopadding"> <img src="content/images/banner/banner-img5.jpg" class="img-responsive">
                      <div class="banner-hover-con">
                           <span><h4>Add a pop of yellow to black and gray </h4>
                           <hr>
                           <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                      </div> <!--/banner-hover-con -->
                  </div>
                  <div class="col-sm-6 col-xs-6 banner-images banner-img1-sectn1 nopadding"> <img src="content/images/banner/banner-img5.jpg" class="img-responsive">
                      <div class="banner-hover-con">
                           <span><h4>Add a pop of yellow to black and gray </h4>
                           <hr>
                           <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                      </div> <!--/banner-hover-con -->
                  </div>
                  <div class="col-sm-6 col-xs-6 banner-images banner-img1-sectn1 nopadding"> <img src="content/images/banner/banner-img5.jpg" class="img-responsive">
                      <div class="banner-hover-con">
                           <span><h4>Add a pop of yellow to black and gray </h4>
                           <hr>
                           <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                      </div> <!--/banner-hover-con -->
                  </div>
                  <div class="col-sm-6 col-xs-6 banner-images banner-img1-sectn1 nopadding"> <img src="content/images/banner/banner-img5.jpg" class="img-responsive">
                      <div class="banner-hover-con">
                           <span><h4>Add a pop of yellow to black and gray </h4>
                           <hr>
                           <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                      </div> <!--/banner-hover-con -->
                  </div>
                  <div class="col-sm-6 col-xs-6 banner-images banner-img1-sectn1 nopadding"> <img src="content/images/banner/banner-img5.jpg" class="img-responsive">
                      <div class="banner-hover-con">
                           <span><h4>Add a pop of yellow to black and gray </h4>
                           <hr>
                           <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                      </div> <!--/banner-hover-con -->
                  </div>
                  <div class="col-sm-6 col-xs-6 banner-images banner-img1-sectn1 nopadding"> <img src="content/images/banner/banner-img5.jpg" class="img-responsive">
                      <div class="banner-hover-con">
                           <span><h4>Add a pop of yellow to black and gray </h4>
                           <hr>
                           <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                      </div> <!--/banner-hover-con -->
                  </div> <!--/ all slide 1 container ends -->
              </div>
            </div>
          </div> <!--/slide1 -->
          <div class="item">
            <div class="row">
              <div class="col-sm-12 col-xs-12"> 
                  <div class="col-sm-6 col-xs-6 banner-images banner-img1-sectn1 nopadding"> <img src="content/images/banner/banner-img5.jpg" class="img-responsive">
                      <div class="banner-hover-con">
                           <span><h4>Add a pop of yellow to black and gray </h4>
                           <hr>
                           <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                      </div> <!--/banner-hover-con -->
                  </div>
                  <div class="col-sm-6 col-xs-6 banner-images banner-img1-sectn1 nopadding"> <img src="content/images/banner/banner-img5.jpg" class="img-responsive">
                      <div class="banner-hover-con">
                           <span><h4>Add a pop of yellow to black and gray </h4>
                           <hr>
                           <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                      </div> <!--/banner-hover-con -->
                  </div>
                  <div class="col-sm-6 col-xs-6 banner-images banner-img1-sectn1 nopadding"> <img src="content/images/banner/banner-img5.jpg" class="img-responsive">
                      <div class="banner-hover-con">
                           <span><h4>Add a pop of yellow to black and gray </h4>
                           <hr>
                           <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                      </div> <!--/banner-hover-con -->
                  </div>
                  <div class="col-sm-6 col-xs-6 banner-images banner-img1-sectn1 nopadding"> <img src="content/images/banner/banner-img5.jpg" class="img-responsive">
                      <div class="banner-hover-con">
                           <span><h4>Add a pop of yellow to black and gray </h4>
                           <hr>
                           <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                      </div> <!--/banner-hover-con -->
                  </div>
                  <div class="col-sm-6 col-xs-6 banner-images banner-img1-sectn1 nopadding"> <img src="content/images/banner/banner-img5.jpg" class="img-responsive">
                      <div class="banner-hover-con">
                           <span><h4>Add a pop of yellow to black and gray </h4>
                           <hr>
                           <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                      </div> <!--/banner-hover-con -->
                  </div>
                  <div class="col-sm-6 col-xs-6 banner-images banner-img1-sectn1 nopadding"> <img src="content/images/banner/banner-img5.jpg" class="img-responsive">
                      <div class="banner-hover-con">
                           <span><h4>Add a pop of yellow to black and gray </h4>
                           <hr>
                           <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                      </div> <!--/banner-hover-con -->
                  </div> <!--/ all slide 2 container ends -->
              </div>
            </div>
          </div> <!--/slide2 -->
          <div class="item">
            <div class="row">
              <div class="col-sm-12 col-xs-12"> 
                  <div class="col-sm-6 col-xs-6 banner-images banner-img1-sectn1 nopadding"> <img src="content/images/banner/banner-img5.jpg" class="img-responsive">
                      <div class="banner-hover-con">
                           <span><h4>Add a pop of yellow to black and gray </h4>
                           <hr>
                           <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                      </div> <!--/banner-hover-con -->
                  </div>
                  <div class="col-sm-6 col-xs-6 banner-images banner-img1-sectn1 nopadding"> <img src="content/images/banner/banner-img5.jpg" class="img-responsive">
                      <div class="banner-hover-con">
                           <span><h4>Add a pop of yellow to black and gray </h4>
                           <hr>
                           <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                      </div> <!--/banner-hover-con -->
                  </div>
                  <div class="col-sm-6 col-xs-6 banner-images banner-img1-sectn1 nopadding"> <img src="content/images/banner/banner-img5.jpg" class="img-responsive">
                      <div class="banner-hover-con">
                           <span><h4>Add a pop of yellow to black and gray </h4>
                           <hr>
                           <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                      </div> <!--/banner-hover-con -->
                  </div>
                  <div class="col-sm-6 col-xs-6 banner-images banner-img1-sectn1 nopadding"> <img src="content/images/banner/banner-img5.jpg" class="img-responsive">
                      <div class="banner-hover-con">
                           <span><h4>Add a pop of yellow to black and gray </h4>
                           <hr>
                           <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                      </div> <!--/banner-hover-con -->
                  </div>
                  <div class="col-sm-6 col-xs-6 banner-images banner-img1-sectn1 nopadding"> <img src="content/images/banner/banner-img5.jpg" class="img-responsive">
                      <div class="banner-hover-con">
                           <span><h4>Add a pop of yellow to black and gray </h4>
                           <hr>
                           <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                      </div> <!--/banner-hover-con -->
                  </div>
                  <div class="col-sm-6 col-xs-6 banner-images banner-img1-sectn1 nopadding"> <img src="content/images/banner/banner-img5.jpg" class="img-responsive">
                      <div class="banner-hover-con">
                           <span><h4>Add a pop of yellow to black and gray </h4>
                           <hr>
                           <div> <img src="content/images/icons/hovered-img-icon.png"> </div> </span>
                      </div> <!--/banner-hover-con -->
                  </div> <!--/ all slide 3 container ends -->
              </div>
            </div>
          </div> <!--/slide3 -->
         
        </div>

      </div><!--/carousel -->
  </div> 

  <!--slider for mobile ends -->
</div>      
</div>

    <div class="main_content">
    <div class="main_content">  
        <div class="container-fluid">
        <input class="pagenum" type="hidden" value="1">
<input class="total-page" type="hidden" value="46">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
         <div class="col-lg-6 col-md-6 col-sm-8 col-xs-12 content_headline">
            <h2 ng-model="heading"><?php print_r($heading); ?></h2>
            <hr>
         </div> 
      </div> <!--/content tagline-->
    
      <div class="col-lg-10 col-md-10 col-sm-11 col-xs-12 wall whats_new_content" id="abc">
       
</div>   <!--/whats_new_content --> 
     
        <!--/column-->

            <!-- mywork place ends here--> 
                </div> <!--/Whats_new_content-->
              </div> <!--/row-->
            </div> <!--/container-fluid-->
          </div> <!--/main_content--> <!--/main_content-->
    <!-- Bootstrap core JavaScript

    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
   <div class="container" style="text-align: center"><button class="btn" id="load_more" data-val = "0">Load more..<img style="display: none" id="loader" src="<?php echo str_replace('index.php','',base_url()) ?>asset/loader.GIF"> </button></div>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.2.6.pack.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script>
 var page1=[];

    $(document).ready(function(){
    
    page1.push("1");
          var page=page1.length;
          var page2=page1.length-1;
          //alert(page)
if(page==page2){

}
else{
            var page = $("#load_more").data('val');
            //alert(page);
            getcountry(page);
          }
 
        $("#load_more").click(function(e){
            e.preventDefault();
            var page = $(this).data('val');
            //alert(page);
            getcountry(page);
 
        });
 
    });

   $(window).scroll(function(e){

    if ($(window).scrollTop() == $(document).height() - $(window).height()){
        //alert("hi");
           //e.preventDefault();
           // $('#load_more').data('val', ($('#load_more').data('val')+1));
            var page = $("#load_more").data('val');
            //alert(page);
            getcountry(page);
          
 
      
    }
  }); 
 
    var getcountry = function(page){
      $('#load_more').data('val', ($('#load_more').data('val')+1));
        $("#loader").show();
        $.ajax({
            url:"<?php echo base_url() ?>myscroll/getCountry",
            type:'GET',
            async:true,
         
            data: {page:page}
        }).done(function(response){
            $("#abc").append(response);
            $("#loader").hide();
          
           //scroll();
        });
    };
 /*
    var scroll  = function(){
        $('html, body').animate({
            scrollTop: $('#load_more').offset().top
        }, 6000);
    };
   */ 
 
 
</script>


  </body>
  
   